package it.com.atlassian.confluence.extra.webdav;

import com.atlassian.confluence.plugin.functest.helper.IndexHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.client.methods.CopyMethod;
import org.apache.jackrabbit.webdav.client.methods.MkColMethod;
import org.apache.jackrabbit.webdav.client.methods.MoveMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ConfigurationWebDavTestCase extends AbstractWebDavTestCase {
    private void setDenyUserAgentRegex(String regex) {
        gotoPage("admin/plugins/webdav/config.action");
        setWorkingForm("webdavSettings");

        final String tokenAttribute = "atl_token";

        // get the XSRF token from the form and use it in the gotoPage below
        String token = getElementAttributByXPath("//input[@name='" + tokenAttribute + "']", "value");

        gotoPage("/admin/plugins/webdav/doconfig.action?denyRegexes=" + regex + "&" + tokenAttribute + "=" + token);
    }

    protected void setUp() throws Exception {
        super.setUp();

        try {
            gotoPageWithEscalatedPrivileges("/admin/console.action");
            setDenyUserAgentRegex("Microsoft.*");
        } finally {
            dropEscalatedPrivileges();
        }
    }

    public void testPutDeniedIfClientIsBlacklisted() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        PutMethod putMethod = new PutMethod(targetUrl);
        RequestEntity requestEntity = new StringRequestEntity("updated space details");
        putMethod.setRequestEntity(requestEntity);
        putMethod.setRequestHeader("User-Agent", "Microsoft Data Access Internet Publishing Provider DAV");

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(HttpServletResponse.SC_FORBIDDEN, statusCode);
        } finally {
            putMethod.releaseConnection();
        }
    }

    public void testCopyDeniedIfClientIsBlacklisted() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)";
        CopyMethod copyMethod = new CopyMethod(targetUrl, destinationUrl, false);
        copyMethod.setRequestHeader("User-Agent", "Microsoft Data Access Internet Publishing Provider DAV");

        try {
            int statusCode = httpClient.executeMethod(copyMethod);

            assertEquals(HttpServletResponse.SC_FORBIDDEN, statusCode);
        } finally {
            copyMethod.releaseConnection();
        }
    }

    public void testDeleteDeniedIfClientIsBlacklisted() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("newpage");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        DeleteMethod deleteMethod = new DeleteMethod(targetUrl);
        deleteMethod.setRequestHeader("User-Agent", "Microsoft Data Access Internet Publishing Provider DAV");

        try {
            int statusCode = httpClient.executeMethod(deleteMethod);
            assertEquals(HttpServletResponse.SC_FORBIDDEN, statusCode);
        } finally {
            deleteMethod.releaseConnection();
        }
    }

    public void testMkcolDeniedIfClientIsBlacklisted() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/ImaginaryPage";
        MkColMethod mkColMethod = new MkColMethod(targetUrl);
        mkColMethod.setRequestHeader("User-Agent", "Microsoft Data Access Internet Publishing Provider DAV");

        try {
            int statusCode = httpClient.executeMethod(mkColMethod);
            assertEquals(HttpServletResponse.SC_FORBIDDEN, statusCode);
        } finally {
            mkColMethod.releaseConnection();
        }
    }

    public void testMoveDeniedIfClientIsBlacklisted() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("newpage");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/pagenew";
        MoveMethod moveMethod = new MoveMethod(targetUrl, destinationUrl, false);
        moveMethod.setRequestHeader("User-Agent", "Microsoft Data Access Internet Publishing Provider DAV");

        try {
            int statusCode = httpClient.executeMethod(moveMethod);
            assertEquals(HttpServletResponse.SC_FORBIDDEN, statusCode);
        } finally {
            moveMethod.releaseConnection();
        }
    }

    // Foo


    public void testPutNotDeniedIfClientIsNotBlacklisted() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        PutMethod putMethod = new PutMethod(targetUrl);
        RequestEntity requestEntity = new StringRequestEntity("updated space details");
        putMethod.setRequestEntity(requestEntity);
        putMethod.setRequestHeader("User-Agent", "ImaginaryClient/1.0");

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);
        } finally {
            putMethod.releaseConnection();
        }
    }

    public void testCopyNotDeniedIfClientIsNotBlacklisted() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)";
        CopyMethod copyMethod = new CopyMethod(targetUrl, destinationUrl, false);
        copyMethod.setRequestHeader("User-Agent", "ImaginaryClient/1.0");

        try {
            int statusCode = httpClient.executeMethod(copyMethod);

            assertEquals(HttpServletResponse.SC_CREATED, statusCode);
        } finally {
            copyMethod.releaseConnection();
        }
    }

    public void testDeleteNotDeniedIfClientIsNotBlacklisted() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("newpage");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        DeleteMethod deleteMethod = new DeleteMethod(targetUrl);
        deleteMethod.setRequestHeader("User-Agent", "ImaginaryClient/1.0");

        try {
            int statusCode = httpClient.executeMethod(deleteMethod);
            assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);
        } finally {
            deleteMethod.releaseConnection();
        }
    }

    public void testMkcolNotDeniedIfClientIsNotBlacklisted() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/ImaginaryPage";
        MkColMethod mkColMethod = new MkColMethod(targetUrl);
        mkColMethod.setRequestHeader("User-Agent", "ImaginaryClient/1.0");

        try {
            int statusCode = httpClient.executeMethod(mkColMethod);
            assertEquals(HttpServletResponse.SC_CREATED, statusCode);
        } finally {
            mkColMethod.releaseConnection();
        }
    }

    public void testMoveNotDeniedIfClientIsNotBlacklisted() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("newpage");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/pagenew";
        MoveMethod moveMethod = new MoveMethod(targetUrl, destinationUrl, false);
        moveMethod.setRequestHeader("User-Agent", "ImaginaryClient/1.0");

        try {
            int statusCode = httpClient.executeMethod(moveMethod);
            assertEquals(HttpServletResponse.SC_CREATED, statusCode);
        } finally {
            moveMethod.releaseConnection();
        }
    }

    public void testCustomWebdavSettingsLoadedFromPluginClassLoaderByXstream() {
        try

        {
            gotoPageWithEscalatedPrivileges("/admin/console.action");
            setDenyUserAgentRegex("Ack");

            IndexHelper indexHelper = getIndexHelper();
            indexHelper.flush();

            //gotoPage("/admin/console.action");
            //clickLinkWithText("Cache Management");
            //clickLinkWithText("Flush all"); //This will remove the cached WebdavSettings and force the plugin to re-read its serialized config from Bandana

            gotoPage("/admin/console.action");
            clickLinkWithText("WebDAV Configuration");
        } finally {
            dropEscalatedPrivileges();
        }
    }
}
