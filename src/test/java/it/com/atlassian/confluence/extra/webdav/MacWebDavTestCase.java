package it.com.atlassian.confluence.extra.webdav;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.MultiStatus;
import org.apache.jackrabbit.webdav.MultiStatusResponse;
import org.apache.jackrabbit.webdav.client.methods.LockMethod;
import org.apache.jackrabbit.webdav.client.methods.PropFindMethod;
import org.apache.jackrabbit.webdav.client.methods.PutMethod;
import org.apache.jackrabbit.webdav.client.methods.UnLockMethod;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Automated Functional Test
 * <p>
 * This class will be testing operations based on MacOS Finder behaviors.
 * Test includes update page content, delete page, update space description and upload attachment
 *
 * @author weiching.cher
 */
public class MacWebDavTestCase extends AbstractWebDavTestCase {

    /**
     * Test update page content with MacOS Finder
     *
     * @throws IOException
     * @throws DavException
     */
//	public void testMacGlobalUpdatePageContentAsFolders() throws IOException, DavException
//    {
//    	PageHelper pageHelper = getPageHelper();
//
//    	pageHelper.setSpaceKey("ds");
//    	pageHelper.setTitle("macpage");
//    	String content = pageHelper.getContent();
//
//    	content += "added";
//    	pageHelper.setContent(content);
//
//    	assertTrue(pageHelper.create());
//
//    	// Search for new page
//    	String targetUrl = getWebdavServletUrl() + "/Global/ds";
//        PropFindMethod propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(6, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/",
//                            getWebdavServletUrl() + "/Global/ds/%40news/",
//                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
//                            getWebdavServletUrl() + "/Global/ds/Index/",
//                            getWebdavServletUrl() + "/Global/ds/macpage/",
//                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//     // Search for the page content text file
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/macpage.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(1, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/macpage/macpage.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // Delete the page content text file
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/macpage.txt";
//    	DeleteMethod deleteMethod = new DeleteMethod(targetUrl);
//    	deleteMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(deleteMethod);
//    		assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
//    	}
//    	finally
//    	{
//    		deleteMethod.releaseConnection();
//    	}
//
//    	// Search for the page content text file
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/macpage.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // Recreate new page content text file
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/macpage.txt";
//    	PutMethod putMethod = new PutMethod(targetUrl);
//    	putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//    	RequestEntity requestEntity = new StringRequestEntity("Upload attachment success");
//    	putMethod.setRequestEntity(requestEntity);
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(putMethod);
//
//    		assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
//    	}
//    	finally
//    	{
//    		putMethod.releaseConnection();
//    	}
//
//    	// Search for the page content text file
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // Recreate new page content text file
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//        putMethod = new PutMethod(targetUrl);
//    	putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//    	requestEntity = new StringRequestEntity("Upload attachment success");
//    	putMethod.setRequestEntity(requestEntity);
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(putMethod);
//
//    		assertEquals(HttpStatus.SC_CREATED, statusCode);
//    	}
//    	finally
//    	{
//    		putMethod.releaseConnection();
//    	}
//
//    	// Lock ._macpage.txt
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//
//    	String[] lockTokens = {};
//    	LockMethod lockMethod = new LockMethod(targetUrl, 0, lockTokens);
//    	lockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(lockMethod);
//
//    		assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
//    	}
//    	finally
//    	{
//    		lockMethod.releaseConnection();
//    	}
//
//    	// Get ._macpage.txt
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/macpage.txt";
//        GetMethod getMethod = new GetMethod(targetUrl);
//        getMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//        try
//        {
//            int statusCode = httpClient.executeMethod(getMethod);
//            assertEquals(HttpStatus.SC_OK, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            getMethod.releaseConnection();
//        }
//
//    	// Put ._macpage.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//    	putMethod = new PutMethod(targetUrl);
//    	putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//    	requestEntity = new StringRequestEntity("Upload attachment success");
//    	putMethod.setRequestEntity(requestEntity);
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(putMethod);
//
//    		assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
//    	}
//    	finally
//    	{
//    		putMethod.releaseConnection();
//    	}
//
//    	// Propfind ._macpage.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_MULTI_STATUS, statusCode);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(1, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//    	// Unlock ._macpage.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//
//    	UnLockMethod unLockMethod = new UnLockMethod(targetUrl, targetUrl);
//    	unLockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(unLockMethod);
//
//    		assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
//    	}
//    	finally
//    	{
//    		unLockMethod.releaseConnection();
//    	}
//
//    	// Lock ._macpage.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//
//    	lockMethod = new LockMethod(targetUrl, 0, lockTokens);
//    	lockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(lockMethod);
//
//    		assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
//    	}
//    	finally
//    	{
//    		lockMethod.releaseConnection();
//    	}
//
//    	// Get ._macpage.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//        getMethod = new GetMethod(targetUrl);
//        getMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//        try
//        {
//
//            int statusCode = httpClient.executeMethod(getMethod);
//            assertEquals(HttpStatus.SC_OK, statusCode);
//        }
//        finally
//        {
//            getMethod.releaseConnection();
//        }
//
//    	// Unlock ._macpage.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//
//    	unLockMethod = new UnLockMethod(targetUrl, targetUrl);
//    	unLockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(unLockMethod);
//
//    		assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
//    	}
//    	finally
//    	{
//    		unLockMethod.releaseConnection();
//    	}
//
//    	// Delete ._macpage.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//    	deleteMethod = new DeleteMethod(targetUrl);
//    	deleteMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(deleteMethod);
//    		assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
//    	}
//    	finally
//    	{
//    		deleteMethod.releaseConnection();
//    	}
//
//    	// Propfind .DS_Store
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/.DS_Store";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//    	// Put .DS_Store
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/.DS_Store";
//    	putMethod = new PutMethod(targetUrl);
//    	putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//    	requestEntity = new StringRequestEntity("Upload attachment success");
//    	putMethod.setRequestEntity(requestEntity);
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(putMethod);
//
//    		assertEquals(HttpStatus.SC_CREATED, statusCode);
//    	}
//    	finally
//    	{
//    		putMethod.releaseConnection();
//    	}
//
//    	// Propfind ._.DS_Store
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._.DS_Store";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//    	// Put .DS_Store
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/.DS_Store";
//    	putMethod = new PutMethod(targetUrl);
//    	putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//    	requestEntity = new StringRequestEntity("Upload attachment success");
//    	putMethod.setRequestEntity(requestEntity);
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(putMethod);
//
//    		assertEquals(HttpStatus.SC_CREATED, statusCode);
//    	}
//    	finally
//    	{
//    		putMethod.releaseConnection();
//    	}
//
//    	// Propfind .DS_Store
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/.DS_Store";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//    	// Lock macpage.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/macpage.txt";
//
//    	lockMethod = new LockMethod(targetUrl, 0, lockTokens);
//    	lockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(lockMethod);
//
//    		assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
//    	}
//    	finally
//    	{
//    		lockMethod.releaseConnection();
//    	}
//
//    	// Get macpage.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/macpage.txt";
//        getMethod = new GetMethod(targetUrl);
//        getMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//        try
//        {
//            int statusCode = httpClient.executeMethod(getMethod);
//            assertEquals(HttpStatus.SC_OK, statusCode);
//        }
//        finally
//        {
//            getMethod.releaseConnection();
//        }
//
//    	// Put macpage.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/macpage.txt";
//    	putMethod = new PutMethod(targetUrl);
//    	putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//    	requestEntity = new StringRequestEntity("Upload attachment success");
//    	putMethod.setRequestEntity(requestEntity);
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(putMethod);
//
//    		assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
//    	}
//    	finally
//    	{
//    		putMethod.releaseConnection();
//    	}
//
//    	// Propfind macpage.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/macpage.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_MULTI_STATUS, statusCode);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(1, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/macpage/macpage.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//    	// Unlock macpage.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/macpage.txt";
//
//    	unLockMethod = new UnLockMethod(targetUrl, targetUrl);
//    	unLockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(unLockMethod);
//
//    		assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
//    	}
//    	finally
//    	{
//    		unLockMethod.releaseConnection();
//    	}
//
//    	// Propfind ._macpage.txt x 5
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//    	// Propfind .DS_Store
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/.DS_Store";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//    	// Propfind ._macpage.txt x3
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macpage.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//    	// Propfind /macpage
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_MULTI_STATUS, statusCode);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(5, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                    		getWebdavServletUrl() + "/Global/ds/macpage/",
//                    		getWebdavServletUrl() + "/Global/ds/macpage/%40versions/",
//                    		getWebdavServletUrl() + "/Global/ds/macpage/%40exports/",
//                            getWebdavServletUrl() + "/Global/ds/macpage/macpage.txt",
//                            getWebdavServletUrl() + "/Global/ds/macpage/macpage.url",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//    }

    /**
     * Test delete page with MacOS Finder
     *
     * @throws IOException
     * @throws DavException
     */
//    public void testMacGlobalDeletePageAsFolders() throws IOException, DavException
//    {
//    	String targetUrl = getWebdavServletUrl() + "/Global/ds/macpage";
//    	MkColMethod mkColMethod = new MkColMethod(targetUrl);
//    	mkColMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(mkColMethod);
//    		assertEquals(statusCode, HttpStatus.SC_CREATED);
//    	}
//    	finally
//    	{
//    		/* Always always call releaseConnection() in finally. */
//    		mkColMethod.releaseConnection();
//    	}
//
//    	// Propfind macpage page
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/macpage";
//        PropFindMethod propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(5, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/macpage/",
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40exports/",
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40versions/",
//                            getWebdavServletUrl() + "/Global/ds/macpage/macpage.txt",
//                            getWebdavServletUrl() + "/Global/ds/macpage/macpage.url",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // Propfind versions
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/%40versions";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(2, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40versions/",
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40versions/Version%201.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // Propfind exports
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/%40exports";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(3, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40exports/",
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40exports/macpage.pdf",
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40exports/macpage.doc",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // Propfind macpage page x 2
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/%40exports";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(3, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40exports/",
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40exports/macpage.pdf",
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40exports/macpage.doc",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // Propfind exports
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/%40exports";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(3, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40exports/",
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40exports/macpage.doc",
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40exports/macpage.pdf",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // Delete macpage.doc
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/%40exports/macpage.doc";
//    	DeleteMethod deleteMethod = new DeleteMethod(targetUrl);
//    	deleteMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(deleteMethod);
//    		assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
//    	}
//    	finally
//    	{
//    		deleteMethod.releaseConnection();
//    	}
//
//        // Delete macpage.pdf
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/%40exports/macpage.pdf";
//    	deleteMethod = new DeleteMethod(targetUrl);
//    	deleteMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(deleteMethod);
//    		assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
//    	}
//    	finally
//    	{
//    		deleteMethod.releaseConnection();
//    	}
//
//    	// Propfind exports
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/%40exports";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(1, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40exports/",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // Delete exports
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/%40exports";
//    	deleteMethod = new DeleteMethod(targetUrl);
//    	deleteMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(deleteMethod);
//    		assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
//    	}
//    	finally
//    	{
//    		deleteMethod.releaseConnection();
//    	}
//
//        // Propfind versions
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/%40versions";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(2, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40versions/",
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40versions/Version%201.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // Delete Version 1.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/%40versions/Version%201.txt";
//    	deleteMethod = new DeleteMethod(targetUrl);
//    	deleteMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(deleteMethod);
//    		assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
//    	}
//    	finally
//    	{
//    		deleteMethod.releaseConnection();
//    	}
//
//        // Delete versions
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/%40versions";
//    	deleteMethod = new DeleteMethod(targetUrl);
//    	deleteMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(deleteMethod);
//    		assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
//    	}
//    	finally
//    	{
//    		deleteMethod.releaseConnection();
//    	}
//
//        // Propfind versions
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/%40versions";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(1, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/macpage/%40versions/",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // Delete versions
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/%40versions";
//    	deleteMethod = new DeleteMethod(targetUrl);
//    	deleteMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(deleteMethod);
//    		assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
//    	}
//    	finally
//    	{
//    		deleteMethod.releaseConnection();
//    	}
//
//        // Delete macpage.url
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/macpage.url";
//    	deleteMethod = new DeleteMethod(targetUrl);
//    	deleteMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(deleteMethod);
//    		assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
//    	}
//    	finally
//    	{
//    		deleteMethod.releaseConnection();
//    	}
//
//        // Delete macpage.txt
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/macpage.txt";
//    	deleteMethod = new DeleteMethod(targetUrl);
//    	deleteMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(deleteMethod);
//    		assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
//    	}
//    	finally
//    	{
//    		deleteMethod.releaseConnection();
//    	}
//
//        // Propfind macpage page
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(1, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/macpage/",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // Delete macpage page
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/macpage";
//    	deleteMethod = new DeleteMethod(targetUrl);
//    	deleteMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(deleteMethod);
//    		assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
//    	}
//    	finally
//    	{
//    		deleteMethod.releaseConnection();
//    	}
//
//        // Propfind ds x 2
//    	targetUrl = getWebdavServletUrl() + "/Global/ds";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(5, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                    		getWebdavServletUrl() + "/Global/ds/",
//                    		getWebdavServletUrl() + "/Global/ds/%40news/",
//                    		getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
//                    		getWebdavServletUrl() + "/Global/ds/Index/",
//                    		getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//
//    }

    /**
     * Test update space description with MacOS Finder
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMacGlobalUpdateSpaceDescriptionAsFolders() throws IOException, DavException {
        // Propfind demonstration space.txt
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(1, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        // Delete demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        DeleteMethod deleteMethod = new DeleteMethod(targetUrl);
        deleteMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try {
            int statusCode = httpClient.executeMethod(deleteMethod);
            assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
        } finally {
            deleteMethod.releaseConnection();
        }

        // Propfind Demonstration Space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
        try {
            int statusCode = httpClient.executeMethod(propFindMethod);
            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        // Put Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        PutMethod putMethod = new PutMethod(targetUrl);
        putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
        RequestEntity requestEntity = new StringRequestEntity("Upload attachment success");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(HttpStatus.SC_CREATED, statusCode);
        } finally {
            putMethod.releaseConnection();
        }

        // Propfind ._Demonstration Space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
        try {
            int statusCode = httpClient.executeMethod(propFindMethod);
            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        // Put ._Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";
        putMethod = new PutMethod(targetUrl);
        putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
        requestEntity = new StringRequestEntity("Upload attachment success");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(HttpStatus.SC_CREATED, statusCode);
        } finally {
            putMethod.releaseConnection();
        }

        // Lock ._Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";

        String[] lockTokens = {};
        LockMethod lockMethod = new LockMethod(targetUrl, 0, lockTokens);
        lockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try {
            int statusCode = httpClient.executeMethod(lockMethod);

            assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
        } finally {
            lockMethod.releaseConnection();
        }

        // Put ._Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";
        putMethod = new PutMethod(targetUrl);
        putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
        requestEntity = new StringRequestEntity("Upload attachment success");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
        } finally {
            putMethod.releaseConnection();
        }

        // Unlock ._Demonstration Space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";

        UnLockMethod unLockMethod = new UnLockMethod(targetUrl, targetUrl);
        unLockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try {
            int statusCode = httpClient.executeMethod(unLockMethod);

            assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
        } finally {
            unLockMethod.releaseConnection();
        }

        // Lock ._Demonstration Space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";

        lockMethod = new LockMethod(targetUrl, 0, lockTokens);
        lockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try {
            int statusCode = httpClient.executeMethod(lockMethod);

            assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
        } finally {
            lockMethod.releaseConnection();
        }

        // Unlock ._Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";

        unLockMethod = new UnLockMethod(targetUrl, targetUrl);
        unLockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try {
            int statusCode = httpClient.executeMethod(unLockMethod);

            assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
        } finally {
            unLockMethod.releaseConnection();
        }

        // Delete ._Demonstration Space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";
        deleteMethod = new DeleteMethod(targetUrl);
        deleteMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try {
            int statusCode = httpClient.executeMethod(deleteMethod);
            assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
        } finally {
            deleteMethod.releaseConnection();
        }

        // Get Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        GetMethod getMethod = new GetMethod(targetUrl);
        getMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try {
            int statusCode = httpClient.executeMethod(getMethod);
            assertEquals(HttpStatus.SC_OK, statusCode);
        } finally {
            getMethod.releaseConnection();
        }

        // Put Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        putMethod = new PutMethod(targetUrl);
        putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
        requestEntity = new StringRequestEntity("Upload attachment success");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
        } finally {
            putMethod.releaseConnection();
        }

        // Propfind Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(1, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        // Unlock Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";

        unLockMethod = new UnLockMethod(targetUrl, targetUrl);
        unLockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try {
            int statusCode = httpClient.executeMethod(unLockMethod);

            assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
        } finally {
            unLockMethod.releaseConnection();
        }

        // Propfind ds x 2
        targetUrl = getWebdavServletUrl() + "/Global/ds";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(5, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test upload attachment to page with MacOS Finder
     *
     * @throws IOException
     * @throws DavException
     */
//    public void testMacGlobalUploadAttachmentToPageAsFilesWithMacOS10_5_1() throws IOException, DavException
//    {
//    	PageHelper pageHelper = getPageHelper();
//
//    	pageHelper.setSpaceKey("ds");
//    	pageHelper.setTitle("vista");
//    	String content = pageHelper.getContent();
//
//    	content += "added";
//    	pageHelper.setContent(content);
//
//    	assertTrue(pageHelper.create());
//
//    	// Search for new page
//    	String targetUrl = getWebdavServletUrl() + "/Global/ds";
//        PropFindMethod propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(6, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/",
//                            getWebdavServletUrl() + "/Global/ds/%40news/",
//                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
//                            getWebdavServletUrl() + "/Global/ds/Index/",
//                            getWebdavServletUrl() + "/Global/ds/vista/",
//                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND macfile.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/macfile.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // PUT macfile.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/macfile.txt";
//    	PutMethod putMethod = new PutMethod(targetUrl);
//    	putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//    	RequestEntity requestEntity = new StringRequestEntity("Upload attachment success");
//    	putMethod.setRequestEntity(requestEntity);
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(putMethod);
//
//    		assertEquals(HttpStatus.SC_CREATED, statusCode);
//    	}
//    	finally
//    	{
//    		putMethod.releaseConnection();
//    	}
//
//        // PROPFIND ._macfile.txt
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/vista/._macfile.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // PUT ._macfile.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/._macfile.txt";
//    	putMethod = new PutMethod(targetUrl);
//    	putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//    	requestEntity = new StringRequestEntity("Upload attachment success");
//    	putMethod.setRequestEntity(requestEntity);
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(putMethod);
//
//    		assertEquals(HttpStatus.SC_CREATED, statusCode);
//    	}
//    	finally
//    	{
//    		putMethod.releaseConnection();
//    	}
//
//        // LOCK ._macfile.txt
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/vista/._macfile.txt";
//
//    	String[] lockTokens = {};
//    	LockMethod lockMethod = new LockMethod(targetUrl, 0, lockTokens);
//    	lockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(lockMethod);
//
//    		assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
//    	}
//    	finally
//    	{
//    		lockMethod.releaseConnection();
//    	}
//
//        // PUT ._macfile.txt
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/vista/._macfile.txt";
//    	putMethod = new PutMethod(targetUrl);
//    	putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//    	requestEntity = new StringRequestEntity("Upload attachment success");
//    	putMethod.setRequestEntity(requestEntity);
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(putMethod);
//
//    		assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
//    	}
//    	finally
//    	{
//    		putMethod.releaseConnection();
//    	}
//
//        // PROPFIND ._macfile.txt
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/vista/._macfile.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(1, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/vista/._macfile.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // UNLOCK ._macfile.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/._macfile.txt";
//
//    	UnLockMethod unLockMethod = new UnLockMethod(targetUrl, targetUrl);
//    	unLockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(unLockMethod);
//
//    		assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
//    	}
//    	finally
//    	{
//    		unLockMethod.releaseConnection();
//    	}
//
//        // PROPFIND .DS_Store
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/.DS_Store";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PUT .DS_Store
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/.DS_Store";
//    	putMethod = new PutMethod(targetUrl);
//    	putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//    	requestEntity = new StringRequestEntity("Upload attachment success");
//    	putMethod.setRequestEntity(requestEntity);
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(putMethod);
//
//    		assertEquals(HttpStatus.SC_CREATED, statusCode);
//    	}
//    	finally
//    	{
//    		putMethod.releaseConnection();
//    	}
//
//        // LOCK .DS_Store
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/vista/.DS_Store";
//
//    	lockMethod = new LockMethod(targetUrl, 0, lockTokens);
//    	lockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(lockMethod);
//
//    		assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
//    	}
//    	finally
//    	{
//    		lockMethod.releaseConnection();
//    	}
//
//        // PROPFIND ._vista
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/._vista";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PUT .DS_Store
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/.DS_Store";
//    	putMethod = new PutMethod(targetUrl);
//    	putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//    	requestEntity = new StringRequestEntity("Upload attachment success");
//    	putMethod.setRequestEntity(requestEntity);
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(putMethod);
//
//    		assertEquals(HttpStatus.SC_CREATED, statusCode);
//    	}
//    	finally
//    	{
//    		putMethod.releaseConnection();
//    	}
//
//        // PROPFIND ._vista
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/._vista";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND .DS_Store
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/.DS_Store";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND ._vista
//        targetUrl = getWebdavServletUrl() + "/Global/ds/._vista";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // UNLOCK .DS_Store
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/.DS_Store";
//
//    	unLockMethod = new UnLockMethod(targetUrl, targetUrl);
//    	unLockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(unLockMethod);
//
//    		assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
//    	}
//    	finally
//    	{
//    		unLockMethod.releaseConnection();
//    	}
//
//        // LOCK ._macfile.txt
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/vista/._macfile.txt";
//
//    	lockMethod = new LockMethod(targetUrl, 0, lockTokens);
//    	lockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(lockMethod);
//
//    		assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
//    	}
//    	finally
//    	{
//    		lockMethod.releaseConnection();
//    	}
//
//        // PROPFIND .DS_Store
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/vista/.DS_Store";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // GET ._macfile.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macfile.txt";
//        GetMethod getMethod = new GetMethod(targetUrl);
//        getMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//
//        try
//        {
//            int statusCode = httpClient.executeMethod(getMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            getMethod.releaseConnection();
//        }
//
//        // PROPFIND ._macfile.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/._macfile.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND ._vista
//        targetUrl = getWebdavServletUrl() + "/Global/ds/._vista";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PUT ._macfile.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/._macfile.txt";
//    	putMethod = new PutMethod(targetUrl);
//    	putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//    	requestEntity = new StringRequestEntity("Upload attachment success");
//    	putMethod.setRequestEntity(requestEntity);
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(putMethod);
//
//    		assertEquals(HttpStatus.SC_CREATED, statusCode);
//    	}
//    	finally
//    	{
//    		putMethod.releaseConnection();
//    	}
//
//        // LOCK ._macfile.txt
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/vista/._macfile.txt";
//
//    	lockMethod = new LockMethod(targetUrl, 0, lockTokens);
//    	lockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(lockMethod);
//
//    		assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
//    	}
//    	finally
//    	{
//    		lockMethod.releaseConnection();
//    	}
//
//        // PROPFIND /vista folder
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/vista";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(7, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/vista/",
//                            getWebdavServletUrl() + "/Global/ds/vista/%40versions/",
//                            getWebdavServletUrl() + "/Global/ds/vista/%40exports/",
//                            getWebdavServletUrl() + "/Global/ds/vista/vista.txt",
//                            getWebdavServletUrl() + "/Global/ds/vista/._macfile.txt",
//                            getWebdavServletUrl() + "/Global/ds/vista/vista.url",
//                            getWebdavServletUrl() + "/Global/ds/vista/macfile.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND macfile.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/macfile.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(1, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/vista/macfile.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND /vista folder
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(7, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/vista/",
//                            getWebdavServletUrl() + "/Global/ds/vista/%40versions/",
//                            getWebdavServletUrl() + "/Global/ds/vista/%40exports/",
//                            getWebdavServletUrl() + "/Global/ds/vista/vista.txt",
//                            getWebdavServletUrl() + "/Global/ds/vista/._macfile.txt",
//                            getWebdavServletUrl() + "/Global/ds/vista/vista.url",
//                            getWebdavServletUrl() + "/Global/ds/vista/macfile.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // LOCK macfile.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/macfile.txt";
//
//    	lockMethod = new LockMethod(targetUrl, 0, lockTokens);
//    	lockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(lockMethod);
//
//    		assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
//    	}
//    	finally
//    	{
//    		lockMethod.releaseConnection();
//    	}
//
//        // GET macfile.txt
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/macfile.txt";
//        getMethod = new GetMethod(targetUrl);
//        getMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//
//        try
//        {
//            int statusCode = httpClient.executeMethod(getMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            getMethod.releaseConnection();
//        }
//
//        // PUT macfile.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/macfile.txt";
//    	putMethod = new PutMethod(targetUrl);
//    	putMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//    	requestEntity = new StringRequestEntity("Upload attachment success");
//    	putMethod.setRequestEntity(requestEntity);
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(putMethod);
//
//    		assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
//    	}
//    	finally
//    	{
//    		putMethod.releaseConnection();
//    	}
//
//        // PROPFIND macfile.txt
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/vista/macfile.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(1, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/vista/macfile.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // UNLOCK macfile.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/macfile.txt";
//
//    	unLockMethod = new UnLockMethod(targetUrl, targetUrl);
//    	unLockMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(unLockMethod);
//
//    		assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
//    	}
//    	finally
//    	{
//    		unLockMethod.releaseConnection();
//    	}
//
//        // GET ._macfile.txt
//    	targetUrl = getWebdavServletUrl() + "/Global/ds/macpage/._macfile.txt";
//        getMethod = new GetMethod(targetUrl);
//        getMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//
//        try
//        {
//            int statusCode = httpClient.executeMethod(getMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            getMethod.releaseConnection();
//        }
//
//        // PROPFIND macfile.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/macfile.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(1, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/vista/macfile.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND /._vista folder
//        targetUrl = getWebdavServletUrl() + "/Global/ds/._vista";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND /ds folder x2
//        targetUrl = getWebdavServletUrl() + "/Global/ds";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(6, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/",
//                            getWebdavServletUrl() + "/Global/ds/%40news/",
//                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
//                            getWebdavServletUrl() + "/Global/ds/Index/",
//                            getWebdavServletUrl() + "/Global/ds/vista/",
//                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        targetUrl = getWebdavServletUrl() + "/Global/ds";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(6, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/",
//                            getWebdavServletUrl() + "/Global/ds/%40news/",
//                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
//                            getWebdavServletUrl() + "/Global/ds/Index/",
//                            getWebdavServletUrl() + "/Global/ds/vista/",
//                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND /._Global folder
//        targetUrl = getWebdavServletUrl() + "/Global/ds/._Global";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND /Global folder
//        targetUrl = getWebdavServletUrl() + "/Global";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(2, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                    		getWebdavServletUrl() + "/Global/",
//                            getWebdavServletUrl() + "/Global/ds/",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND /default folder
//        targetUrl = getWebdavServletUrl() + "/";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(3, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                    		getWebdavServletUrl() + "/",
//                            getWebdavServletUrl() + "/Global/",
//                            getWebdavServletUrl() + "/Personal/",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND /Global folder
//        targetUrl = getWebdavServletUrl() + "/Global";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(2, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                    		getWebdavServletUrl() + "/Global/",
//                            getWebdavServletUrl() + "/Global/ds/",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND /._. folder
//        targetUrl = getWebdavServletUrl() + "/Global/ds/._.";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND /ds folder
//        targetUrl = getWebdavServletUrl() + "/Global/ds";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(6, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/",
//                            getWebdavServletUrl() + "/Global/ds/%40news/",
//                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
//                            getWebdavServletUrl() + "/Global/ds/Index/",
//                            getWebdavServletUrl() + "/Global/ds/vista/",
//                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND /._vista folder
//        targetUrl = getWebdavServletUrl() + "/Global/ds/._vista";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND /vista folder x2
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(6, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/vista/",
//                            getWebdavServletUrl() + "/Global/ds/vista/%40versions/",
//                            getWebdavServletUrl() + "/Global/ds/vista/%40exports/",
//                            getWebdavServletUrl() + "/Global/ds/vista/vista.txt",
//                            getWebdavServletUrl() + "/Global/ds/vista/vista.url",
//                            getWebdavServletUrl() + "/Global/ds/vista/macfile.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(6, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/vista/",
//                            getWebdavServletUrl() + "/Global/ds/vista/%40versions/",
//                            getWebdavServletUrl() + "/Global/ds/vista/%40exports/",
//                            getWebdavServletUrl() + "/Global/ds/vista/vista.txt",
//                            getWebdavServletUrl() + "/Global/ds/vista/vista.url",
//                            getWebdavServletUrl() + "/Global/ds/vista/macfile.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND /macfile.txt
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista/macfile.txt";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(1, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/vista/macfile.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND /._vista folder
//        targetUrl = getWebdavServletUrl() + "/Global/ds/._.";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	int statusCode = httpClient.executeMethod(propFindMethod);
//            assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//        // PROPFIND /vista
//        targetUrl = getWebdavServletUrl() + "/Global/ds/vista";
//        propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        propFindMethod.setRequestHeader("User-Agent", "WebDAVFS/1.5 (01438000) Darwin/8.11.1 (i386)");
//        try
//        {
//        	MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(6, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/vista/",
//                            getWebdavServletUrl() + "/Global/ds/vista/%40versions/",
//                            getWebdavServletUrl() + "/Global/ds/vista/%40exports/",
//                            getWebdavServletUrl() + "/Global/ds/vista/vista.txt",
//                            getWebdavServletUrl() + "/Global/ds/vista/vista.url",
//                            getWebdavServletUrl() + "/Global/ds/vista/macfile.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            propFindMethod.releaseConnection();
//        }
//
//    }

}
