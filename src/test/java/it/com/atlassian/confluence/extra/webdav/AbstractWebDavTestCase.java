package it.com.atlassian.confluence.extra.webdav;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.SimpleHttpConnectionManager;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.jackrabbit.webdav.MultiStatusResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * This class will define all the necessary settings to be used when this class is extended
 *
 * @author weiching.cher
 */
public abstract class AbstractWebDavTestCase extends AbstractConfluencePluginWebTestCase {
    protected HttpClient httpClient;

    protected void setUp() throws Exception {
        super.setUp();
        setupHttpClient();
    }

    protected void setupHttpClient() {
        HttpConnectionManager httpConnectionManager = new SimpleHttpConnectionManager();
        ConfluenceWebTester confluenceWebTester = getConfluenceWebTester();

        httpClient = new HttpClient();
        httpClient.setHttpConnectionManager(httpConnectionManager);

        httpClient.getParams().setAuthenticationPreemptive(true);
        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM
                )
                , new UsernamePasswordCredentials(confluenceWebTester.getAdminUserName(), confluenceWebTester.getAdminPassword())
        );
    }

    protected String getWebdavServerHostName() {
        return System.getProperty("webdav.host");
    }

    protected int getWebdavServerPort() {
        return Integer.parseInt(System.getProperty("webdav.port"));
    }

    protected String getWebdavServerContextPath() {
        return System.getProperty("webdav.context");
    }

    protected String getWebdavServletPath() {
        return System.getProperty("webdav.resource.path.prefix");
    }

    protected String getWebdavServletUrl() {
        return "http://" + getWebdavServerHostName() + ':' + getWebdavServerPort() + '/' + getWebdavServerContextPath() + getWebdavServletPath();
    }

    protected List getMultiStatusHrefs(MultiStatusResponse[] multiStatusResponses) {
        List hrefs = new ArrayList();

        if (null != multiStatusResponses) {
            for (int i = 0; i < multiStatusResponses.length; ++i)
                hrefs.add(multiStatusResponses[i].getHref());
        }

        return hrefs;
    }

    protected void pause(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            // noop.
        }

    }

}
