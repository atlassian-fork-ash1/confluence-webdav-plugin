package it.com.atlassian.confluence.extra.webdav.xsrf;

import com.atlassian.confluence.plugin.functest.util.ConfluenceBuildUtil;
import it.com.atlassian.confluence.extra.webdav.AbstractWebDavTestCase;

public class WebDavConfigXsrfAcceptanceTest extends AbstractWebDavTestCase {
    public void testAdminWebDavConfigurationXsrfProtected() {
        if (!ConfluenceBuildUtil.containsSudoFeature())
            assertResourceXsrfProtected("/admin/plugins/webdav/doconfig.action?denyRegexsContainer=a");
    }
}
