package it.com.atlassian.confluence.extra.webdav;

import com.atlassian.confluence.plugin.functest.helper.BlogPostHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.MultiStatus;
import org.apache.jackrabbit.webdav.MultiStatusResponse;
import org.apache.jackrabbit.webdav.client.methods.MkColMethod;
import org.apache.jackrabbit.webdav.client.methods.MoveMethod;
import org.apache.jackrabbit.webdav.client.methods.PropFindMethod;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Automated Functional Test
 * <p>
 * This class will be testing operations based on file manipulation.
 * Such as fetch/update/delete page content.
 *
 * @author weiching.cher
 */
public class FileListWebDavTestCase extends AbstractWebDavTestCase {
    private static Logger log = Logger.getLogger(FileListWebDavTestCase.class);

    // ###### < READING  > ################################################################
    // ------ <  Global  > ----------------------------------------------------------------

    /**
     * Test GET command to fetch space description content and check the content
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetGlobalSpaceDetailsAsFile() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        GetMethod getMethod = new GetMethod(targetUrl);

        try {
            byte[] byteArray;
            String expectedBody;
            String actualBody;

            httpClient.executeMethod(getMethod);

            byteArray = getMethod.getResponseBody();

            assertNotNull(byteArray);

            expectedBody = "A space which demonstrates Confluence functionality.";

            actualBody = new String(byteArray, "UTF-8");

            assertEquals(expectedBody.length(), actualBody.length());
            assertTrue(expectedBody.contains(actualBody));
        } finally {
            /* Always always call releaseConnection() in finally. */
            getMethod.releaseConnection();
        }
    }

    /**
     * Test GET command to fetch page content and check the content
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetGlobalPageAsFile() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.txt";
        GetMethod getMethod = new GetMethod(targetUrl);

        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Confluence Overview");
        pageHelper.setVersion(61);
        pageHelper.setId(pageHelper.findBySpaceKeyAndTitle());

        assertTrue(pageHelper.read());

        String content = pageHelper.getContent();

        try {
            byte[] byteArray;
            String expectedBody;
            String actualBody;

            httpClient.executeMethod(getMethod);

            byteArray = getMethod.getResponseBody();

            assertNotNull(byteArray);

            expectedBody = content;

            assertNotNull(expectedBody);

            actualBody = new String(byteArray, "UTF-8");

            assertEquals(expectedBody.length(), actualBody.length());
            assertTrue(expectedBody.contains(actualBody));
        } finally {
            /* Always always call releaseConnection() in finally. */
            getMethod.releaseConnection();
        }
    }

    // Test disabled due to flakeyness in Bamboo build
    /**
     * Test GET command to fetch page version content and check the content
     *
     * @throws IOException
     * @throws DavException
     */
//    public void testGetGlobalVersionAsFile() throws IOException, DavException
//    {
//        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/Version%2061.txt";
//        GetMethod getMethod = new GetMethod(targetUrl);
//
//        PageHelper pageHelper = getPageHelper();
//        pageHelper.setSpaceKey("ds");
//        pageHelper.setTitle("Confluence Overview");
//        pageHelper.setVersion(61);
//        pageHelper.setId(pageHelper.findBySpaceKeyAndTitle());
//
//        assertTrue(pageHelper.read());
//
//        String content = pageHelper.getContent();
//
//        try
//        {
//            byte[] byteArray;
//            String expectedBody;
//            String actualBody;
//
//            httpClient.executeMethod(getMethod);
//
//            byteArray = getMethod.getResponseBody();
//
//            assertNotNull(byteArray);
//
//            expectedBody = content;
//
//            assertNotNull(expectedBody);
//
//            actualBody = new String(byteArray, "UTF-8");
//
//            assertEquals(expectedBody.length(), actualBody.length());
//            assertTrue(expectedBody.contains(actualBody));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            getMethod.releaseConnection();
//        }
//    }

    /**
     * Test GET command to fetch space news (blog) and check the content
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetGlobalBlogAsFile() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/%40news/2004/11/21/Octagon%20blog%20post.txt";
        GetMethod getMethod = new GetMethod(targetUrl);

        BlogPostHelper blogPostHelper = getBlogPostHelper();
        Calendar cal = Calendar.getInstance();
        cal.set(2004, 10, 21);

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setCreationDate(cal.getTime());
        blogPostHelper.setTitle("Octagon blog post");
        blogPostHelper.setId(blogPostHelper.findBySpaceKeyPublishedDateAndBlogPostTitle());

        assertTrue(blogPostHelper.read());

        String content = blogPostHelper.getContent();

        try {
            byte[] byteArray;
            String expectedBody;
            String actualBody;

            httpClient.executeMethod(getMethod);

            byteArray = getMethod.getResponseBody();

            assertNotNull(byteArray);

            expectedBody = content;

            assertNotNull(expectedBody);

            actualBody = new String(byteArray, "UTF-8");

            assertEquals(expectedBody.length(), actualBody.length());
            assertTrue(expectedBody.contains(actualBody));
        } finally {
            /* Always always call releaseConnection() in finally. */
            getMethod.releaseConnection();
        }
    }

    // ------ < Personal > ----------------------------------------------------------------

    /**
     * Test GET command to fetch personal space page content and check the content
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetPersonalPageAsFile() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/Home.txt";
        GetMethod getMethod = new GetMethod(targetUrl);

        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("~admin");
        pageHelper.setTitle("Home");
        pageHelper.setVersion(2);
        pageHelper.setId(pageHelper.findBySpaceKeyAndTitle());

        assertTrue(pageHelper.read());

        String content = pageHelper.getContent();
        try {
            byte[] byteArray;
            String expectedBody;
            String actualBody;

            httpClient.executeMethod(getMethod);

            byteArray = getMethod.getResponseBody();

            assertNotNull(byteArray);

            expectedBody = content;

            assertNotNull(expectedBody);

            actualBody = new String(byteArray, "UTF-8");

            assertEquals(expectedBody.length(), actualBody.length());
            assertTrue(expectedBody.contains(actualBody));
        } finally {
            /* Always always call releaseConnection() in finally. */
            getMethod.releaseConnection();
        }
    }

    /**
     * Test GET command to fetch personal space page version content and check the content
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetPersonalVersionAsFile() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/Version%203.txt";
        GetMethod getMethod = new GetMethod(targetUrl);

        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("~admin");
        pageHelper.setTitle("Home");
        pageHelper.setVersion(3);
        pageHelper.setId(pageHelper.findBySpaceKeyAndTitle());

        assertTrue(pageHelper.read());

        String content = pageHelper.getContent();

        try {
            byte[] byteArray;
            String expectedBody;
            String actualBody;

            httpClient.executeMethod(getMethod);

            byteArray = getMethod.getResponseBody();

            assertNotNull(byteArray);

            expectedBody = content;

            assertNotNull(expectedBody);

            actualBody = new String(byteArray, "UTF-8");

            assertEquals(expectedBody.length(), actualBody.length());
            assertTrue(expectedBody.contains(actualBody));
        } finally {
            /* Always always call releaseConnection() in finally. */
            getMethod.releaseConnection();
        }
    }

    /**
     * Test GET command to fetch personal space news (blog) and check the content
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetPersonalBlogAsFile() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/%40news/2008/05/15/admin%20blog%20post.txt";
        GetMethod getMethod = new GetMethod(targetUrl);

        BlogPostHelper blogPostHelper = getBlogPostHelper();
        Calendar cal = Calendar.getInstance();
        cal.set(2008, 4, 15);

        blogPostHelper.setSpaceKey("~admin");
        blogPostHelper.setCreationDate(cal.getTime());
        blogPostHelper.setTitle("admin blog post");
        blogPostHelper.setId(blogPostHelper.findBySpaceKeyPublishedDateAndBlogPostTitle());

        assertTrue(blogPostHelper.read());

        String content = blogPostHelper.getContent();

        try {
            byte[] byteArray;
            String expectedBody;
            String actualBody;

            httpClient.executeMethod(getMethod);

            byteArray = getMethod.getResponseBody();

            assertNotNull(byteArray);

            expectedBody = content;

            assertNotNull(expectedBody);

            actualBody = new String(byteArray, "UTF-8");

            assertEquals(expectedBody.length(), actualBody.length());
            assertTrue(expectedBody.contains(actualBody));
        } finally {
            /* Always always call releaseConnection() in finally. */
            getMethod.releaseConnection();
        }
    }

    // ###### < WRITING  > ################################################################
    // ------ <  Global  > ----------------------------------------------------------------

    /**
     * Test PUT command to update space description and check the content
     *
     * @throws IOException
     * @throws DavException
     */
    public void testPutGlobalUpdateSpaceDescriptionAsFile() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        PutMethod putMethod = new PutMethod(targetUrl);
        RequestEntity requestEntity = new StringRequestEntity("updated space details");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(statusCode, HttpStatus.SC_NO_CONTENT);
        } finally {
            putMethod.releaseConnection();
        }

        GetMethod getMethod = new GetMethod(targetUrl);

        try {
            byte[] byteArray;
            String expectedBody;
            String actualBody;

            httpClient.executeMethod(getMethod);

            byteArray = getMethod.getResponseBody();

            assertNotNull(byteArray);

            expectedBody = "updated space details";

            actualBody = new String(byteArray, "UTF-8");

            assertEquals(expectedBody.length(), actualBody.length());
            assertTrue(expectedBody.contains(actualBody));
        } finally {
            /* Always always call releaseConnection() in finally. */
            getMethod.releaseConnection();
        }
    }

    // Test disabled due to flakeyness in Bamboo build
    /**
     * Test PUT command to upload an attachment to a page
     *
     * @throws IOException
     * @throws DavException
     */
//    public void testPutGlobalUploadAttachmentAsFile() throws IOException, DavException
//    {
//    	String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/test.txt";
//    	PutMethod putMethod = new PutMethod(targetUrl);
//    	RequestEntity requestEntity = new StringRequestEntity("Upload attachment success");
//    	putMethod.setRequestEntity(requestEntity);
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(putMethod);
//
//    		assertEquals(statusCode, HttpStatus.SC_CREATED);
//    	}
//    	finally
//    	{
//    		putMethod.releaseConnection();
//    	}
//
//    	GetMethod getMethod = new GetMethod(targetUrl);
//
//        try
//        {
//            byte[] byteArray;
//            String expectedBody;
//            String actualBody;
//
//            httpClient.executeMethod(getMethod);
//
//            byteArray = getMethod.getResponseBody();
//
//            assertNotNull(byteArray);
//
//            expectedBody = "Upload attachment success";
//
//            actualBody = new String(byteArray, "UTF-8");
//
//            assertEquals(expectedBody.length(), actualBody.length());
//
//            // Debugging log to find out why sometimes it is failing on Bamboo. Remove when its not happening again.
//            log.info("Actual Body - [" + actualBody + "]");
//
//            assertTrue(expectedBody.contains(actualBody));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            getMethod.releaseConnection();
//        }
//
//        targetUrl = getWebdavServletUrl() + "/Global/ds/Index";
//        PropFindMethod propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(6, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                    		getWebdavServletUrl() + "/Global/ds/Index/",
//                    		getWebdavServletUrl() + "/Global/ds/Index/%40exports/",
//                            getWebdavServletUrl() + "/Global/ds/Index/%40versions/",
//                            getWebdavServletUrl() + "/Global/ds/Index/Index.txt",
//                            getWebdavServletUrl() + "/Global/ds/Index/Index.url",
//                            getWebdavServletUrl() + "/Global/ds/Index/test.txt",
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//    }

    /**
     * Test REMOVE command to delete attachment from a page
     *
     * @throws IOException
     * @throws DavException
     */
    public void testDelGlobalRemoveAttachmentAsFile() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/spacesummary.gif";
        DeleteMethod deleteMethod = new DeleteMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(deleteMethod);

            assertEquals(statusCode, HttpStatus.SC_NO_CONTENT);
        } finally {
            deleteMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(5, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/%40exports/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/%40versions/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/Using%20Spaces.txt",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/Using%20Spaces.url",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test PUT command to upload an existing attachment to a page
     *
     * @throws IOException
     * @throws DavException
     */
    public void testPutGlobalUploadSameNameAttachmentAsFile() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/worddocument.doc";
        PutMethod putMethod = new PutMethod(targetUrl);
        RequestEntity requestEntity = new StringRequestEntity("Upload attachment success");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(statusCode, HttpStatus.SC_NO_CONTENT);
        } finally {
            putMethod.releaseConnection();
        }
    }

    /**
     * Test PUT command to update page content to a page
     *
     * @throws IOException
     * @throws DavException
     */
    public void testPutGlobalUpdatePageContentAsFile() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("newpage");
        pageHelper.setContent("Nothing now");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/newpage/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/newpage/newpage.txt";
        PutMethod putMethod = new PutMethod(targetUrl);
        RequestEntity requestEntity = new StringRequestEntity("There is something now");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(statusCode, HttpStatus.SC_NO_CONTENT);
        } finally {
            putMethod.releaseConnection();
        }

        GetMethod getMethod = new GetMethod(targetUrl);

        try {
            byte[] byteArray;
            String expectedBody;
            String actualBody;

            httpClient.executeMethod(getMethod);

            byteArray = getMethod.getResponseBody();

            assertNotNull(byteArray);

            expectedBody = "There is something now";

            actualBody = new String(byteArray, "UTF-8");

            assertEquals(expectedBody.length(), actualBody.length());
            assertTrue(expectedBody.contains(actualBody));
        } finally {
            /* Always always call releaseConnection() in finally. */
            getMethod.releaseConnection();
        }
    }

    /**
     * Test PUT command to update page content to a page and check for page version
     *
     * @throws IOException
     * @throws DavException
     */
    public void testPutGlobalUpdatePageContentAndCheckPageVersionAsFile() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("newnewpage");
        pageHelper.setContent("Nothing now");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/newnewpage/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/newnewpage";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(5, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/newnewpage/",
                            getWebdavServletUrl() + "/Global/ds/newnewpage/%40exports/",
                            getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions/",
                            getWebdavServletUrl() + "/Global/ds/newnewpage/newnewpage.txt",
                            getWebdavServletUrl() + "/Global/ds/newnewpage/newnewpage.url",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(3, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions/",
                            getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions/Version%201.txt",
                            getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions/README.txt"
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/newnewpage/newnewpage.txt";
        PutMethod putMethod = new PutMethod(targetUrl);
        RequestEntity requestEntity = new StringRequestEntity("There is something now");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(statusCode, HttpStatus.SC_NO_CONTENT);
        } finally {
            putMethod.releaseConnection();
        }


        targetUrl = getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(4, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions/",
                            getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions/Version%201.txt",
                            getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions/Version%202.txt",
                            getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions/README.txt"
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

    }

    /**
     * Test PUT command by drag & drop folder with subfolder from desktop into WebDAV Client
     *
     * @throws IOException
     * @throws DavException
     */
    public void testPutGlobalUploadPageWithSubPageAndTextFiles() throws IOException, DavException {
        for (int i = 1; i < 5; ++i) {
            if (i == 1 || i == 3) {
                String targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
                if (i == 3)
                    targetUrl = getWebdavServletUrl() + "/Global/ds/newpage/subpage";
                MkColMethod mkColMethod = new MkColMethod(targetUrl);

                try {
                    int statusCode = httpClient.executeMethod(mkColMethod);
                    assertEquals(statusCode, HttpStatus.SC_CREATED);
                } finally {
                    /* Always always call releaseConnection() in finally. */
                    mkColMethod.releaseConnection();
                }
            } else if (i == 2 || i == 4) {
                String targetUrl = getWebdavServletUrl() + "/Global/ds/newpage/newpage.txt";
                if (i == 4)
                    targetUrl = getWebdavServletUrl() + "/Global/ds/newpage/subpage/subpage.txt";
                PutMethod putMethod = new PutMethod(targetUrl);
                RequestEntity requestEntity = new StringRequestEntity("This is newpage");
                if (i == 4)
                    requestEntity = new StringRequestEntity("This is subpage");
                putMethod.setRequestEntity(requestEntity);

                try {
                    int statusCode = httpClient.executeMethod(putMethod);

                    assertEquals(statusCode, HttpStatus.SC_NO_CONTENT);
                } finally {
                    putMethod.releaseConnection();
                }
            }
        }

        String targetUrl = getWebdavServletUrl() + "/Global/ds";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/newpage/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/newpage/",
                            getWebdavServletUrl() + "/Global/ds/newpage/%40exports/",
                            getWebdavServletUrl() + "/Global/ds/newpage/%40versions/",
                            getWebdavServletUrl() + "/Global/ds/newpage/subpage/",
                            getWebdavServletUrl() + "/Global/ds/newpage/newpage.txt",
                            getWebdavServletUrl() + "/Global/ds/newpage/newpage.url",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/newpage/subpage";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(5, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/newpage/subpage/",
                            getWebdavServletUrl() + "/Global/ds/newpage/subpage/%40exports/",
                            getWebdavServletUrl() + "/Global/ds/newpage/subpage/%40versions/",
                            getWebdavServletUrl() + "/Global/ds/newpage/subpage/subpage.txt",
                            getWebdavServletUrl() + "/Global/ds/newpage/subpage/subpage.url",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/newpage/newpage.txt";
        GetMethod getMethod = new GetMethod(targetUrl);

        try {
            byte[] byteArray;
            String expectedBody;
            String actualBody;

            httpClient.executeMethod(getMethod);

            byteArray = getMethod.getResponseBody();

            assertNotNull(byteArray);

            expectedBody = "This is newpage";

            actualBody = new String(byteArray, "UTF-8");

            assertEquals(expectedBody.length(), actualBody.length());
            assertTrue(expectedBody.contains(actualBody));
        } finally {
            /* Always always call releaseConnection() in finally. */
            getMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/newpage/subpage/subpage.txt";
        getMethod = new GetMethod(targetUrl);

        try {
            byte[] byteArray;
            String expectedBody;
            String actualBody;

            httpClient.executeMethod(getMethod);

            byteArray = getMethod.getResponseBody();

            assertNotNull(byteArray);

            expectedBody = "This is subpage";

            actualBody = new String(byteArray, "UTF-8");

            assertEquals(expectedBody.length(), actualBody.length());
            assertTrue(expectedBody.contains(actualBody));
        } finally {
            /* Always always call releaseConnection() in finally. */
            getMethod.releaseConnection();
        }
    }

    /**
     * Test PUT command by uploading doc file into page exports folder
     *
     * @throws IOException
     * @throws DavException
     */
    public void testPutGlobalUploadPageExportDocAsFiles() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/%40exports/Index.doc";
        PutMethod putMethod = new PutMethod(targetUrl);
        RequestEntity requestEntity = new StringRequestEntity("Upload attachment success");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
        } finally {
            putMethod.releaseConnection();
        }
    }

    /**
     * Test PUT command by uploading pdf file into page exports folder
     *
     * @throws IOException
     * @throws DavException
     */
    public void testPutGlobalUploadPageExportPdfAsFiles() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/%40exports/Index.pdf";
        PutMethod putMethod = new PutMethod(targetUrl);
        RequestEntity requestEntity = new StringRequestEntity("Upload attachment success");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(statusCode, HttpStatus.SC_NO_CONTENT);
        } finally {
            putMethod.releaseConnection();
        }
    }

    /**
     * Test DELETE command by removing doc file from page exports folder
     *
     * @throws IOException
     * @throws DavException
     */
    public void testDelGlobalRemovePageExportDocAsFiles() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/%40exports/Index.doc";
        DeleteMethod deleteMethod = new DeleteMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(deleteMethod);

            assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
        } finally {
            deleteMethod.releaseConnection();
        }
    }

    /**
     * Test DELETE command by removing pdf file from page exports folder
     *
     * @throws IOException
     * @throws DavException
     */
    public void testDelGlobalRemovePageExportPdfAsFiles() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/%40exports/Index.pdf";
        DeleteMethod deleteMethod = new DeleteMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(deleteMethod);

            assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
        } finally {
            deleteMethod.releaseConnection();
        }
    }

    /**
     * Test MOVE command by renaming doc file from page exports folder
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMoveGlobalRenamePageDocExportsAsFiles() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/Confluence%20Overview.doc";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/Confluence%20OverviewS.doc";
        MoveMethod moveMethod = new MoveMethod(targetUrl, destinationUrl, false);

        try {
            int statusCode = httpClient.executeMethod(moveMethod);
            assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
        } finally {
            moveMethod.releaseConnection();
        }
    }

    /**
     * Test MOVE command by renaming doc file from page exports folder
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMoveGlobalRenamePagePdfExportsAsFiles() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/Confluence%20Overview.pdf";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/Confluence%20OverviewS.pdf";
        MoveMethod moveMethod = new MoveMethod(targetUrl, destinationUrl, false);

        try {
            int statusCode = httpClient.executeMethod(moveMethod);
            assertEquals(HttpStatus.SC_PRECONDITION_FAILED, statusCode);
        } finally {
            moveMethod.releaseConnection();
        }
    }

    /**
     * Test PUT command by uploading page url file into page folder
     *
     * @throws IOException
     * @throws DavException
     */
    public void testPutGlobalUploadPageUrlAsFiles() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/Index.url";
        PutMethod putMethod = new PutMethod(targetUrl);
        RequestEntity requestEntity = new StringRequestEntity("Upload attachment success");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(HttpStatus.SC_NO_CONTENT, statusCode); /* Creation of URL resource ignored */
        } finally {
            putMethod.releaseConnection();
        }
    }

    /**
     * Test DELETE command by removing page url file from page folder
     *
     * @throws IOException
     * @throws DavException
     */
    public void testDelGlobalRemovePageUrlAsFiles() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/Index.url";
        DeleteMethod deleteMethod = new DeleteMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(deleteMethod);

            assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
        } finally {
            deleteMethod.releaseConnection();
        }
    }

    /**
     * Test MOVE command by renaming page url file in page folder
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMoveGlobalRenamePageUrlAsFiles() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.url";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20OverviewS.url";
        MoveMethod moveMethod = new MoveMethod(targetUrl, destinationUrl, false);

        try {
            int statusCode = httpClient.executeMethod(moveMethod);
            assertEquals(HttpStatus.SC_FORBIDDEN, statusCode);
        } finally {
            moveMethod.releaseConnection();
        }
    }

    // Test disabled due to flakeyness in Bamboo build
    /**
     * Test PUT command by uploading page version file into page versions folder
     *
     * @throws IOException
     * @throws DavException
     */
//    public void testPutGlobalUploadPageVersionAsFiles() throws IOException, DavException
//    {
//    	String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/%40versions/Version%203.txt";
//    	PutMethod putMethod = new PutMethod(targetUrl);
//    	RequestEntity requestEntity = new StringRequestEntity("Upload attachment success");
//    	putMethod.setRequestEntity(requestEntity);
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(putMethod);
//
//    		assertEquals(HttpStatus.SC_CREATED, statusCode);
//    	}
//    	finally
//    	{
//    		putMethod.releaseConnection();
//    	}
//    }

    /**
     * Test DELETE command by removing page version file from page versions folder
     *
     * @throws IOException
     * @throws DavException
     */
    public void testDelGlobalRemovePageVersionAsFiles() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/%40versions/Version%201.txt";
        DeleteMethod deleteMethod = new DeleteMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(deleteMethod);

            assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
        } finally {
            deleteMethod.releaseConnection();
        }
    }

    // Test disabled due to flakeyness in Bamboo build
    /**
     * Test MOVE command by renaming page version file from page versions folder
     *
     * @throws IOException
     * @throws DavException
     */
//    public void testMoveGlobalRenamePageVersionAsFiles() throws IOException, DavException
//    {
//    	String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/%40versions/Version%201.txt";
//        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Index/%40versions/Version%203.txt";
//    	MoveMethod moveMethod = new MoveMethod(targetUrl, destinationUrl, false);
//
//    	try
//    	{
//    		int statusCode = httpClient.executeMethod(moveMethod);
//    		assertEquals(HttpStatus.SC_FORBIDDEN, statusCode);
//    	}
//    	finally
//    	{
//    		moveMethod.releaseConnection();
//    	}
//    }

    /**
     * Test PUT command by uploading 100 attachment to to a page in one go
     *
     * @throws IOException
     * @throws DavException
     */
    public void testPutGlobalUploadBulkAttachmentAsFile() throws IOException, DavException {
        String targetUrl;
        PutMethod putMethod;

        for (int i = 1; i <= 100; ++i) {
            targetUrl = getWebdavServletUrl() + "/Global/ds/Index/test" + i + ".txt";
            putMethod = new PutMethod(targetUrl);

            RequestEntity requestEntity = new StringRequestEntity("Upload attachment " + i + " success");
            putMethod.setRequestEntity(requestEntity);

            try {
                int statusCode = httpClient.executeMethod(putMethod);

                assertEquals(HttpStatus.SC_CREATED, statusCode);
            } finally {
                putMethod.releaseConnection();
            }
        }
    }

    // ## < TEST PERMISSIONS > ##

    /**
     * Test PUT command by updating page content permission
     *
     * @throws IOException
     * @throws DavException
     */
    public void testPutGlobalTestUpdatePagePermissionAsFile() throws IOException, DavException {
        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM)
                , new UsernamePasswordCredentials("sakura", "sakura")
        );

        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/Index.txt";
        PutMethod putMethod = new PutMethod(targetUrl);
        RequestEntity requestEntity = new StringRequestEntity("There is something now");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(HttpStatus.SC_FORBIDDEN, statusCode);
        } finally {
            putMethod.releaseConnection();
        }

        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM
                )
                , new UsernamePasswordCredentials("admin", "admin")
        );
    }

    // ------ < Personal > ----------------------------------------------------------------

    /**
     * Test PUT command by updating personal space description
     *
     * @throws IOException
     * @throws DavException
     */
    public void testPutPersonalUpdateSpaceDescriptionAsFile() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/admin.txt";
        PutMethod putMethod = new PutMethod(targetUrl);
        RequestEntity requestEntity = new StringRequestEntity("updated admin space description");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
        } finally {
            putMethod.releaseConnection();
        }

        GetMethod getMethod = new GetMethod(targetUrl);

        try {
            byte[] byteArray;
            String expectedBody;
            String actualBody;

            httpClient.executeMethod(getMethod);

            byteArray = getMethod.getResponseBody();

            assertNotNull(byteArray);

            expectedBody = "updated admin space description";

            actualBody = new String(byteArray, "UTF-8");

            assertEquals(expectedBody.length(), actualBody.length());
            assertTrue(expectedBody.contains(actualBody));
        } finally {
            /* Always always call releaseConnection() in finally. */
            getMethod.releaseConnection();
        }
    }

    /**
     * Test PUT command by uploading an attachment to page in Personal Space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testPutPersonalUploadAttachmentAsFile() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/test.txt";
        PutMethod putMethod = new PutMethod(targetUrl);
        RequestEntity requestEntity = new StringRequestEntity("Upload attachment success");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(HttpStatus.SC_CREATED, statusCode);
        } finally {
            putMethod.releaseConnection();
        }

        GetMethod getMethod = new GetMethod(targetUrl);

        try {
            byte[] byteArray;
            String expectedBody;
            String actualBody;

            httpClient.executeMethod(getMethod);

            byteArray = getMethod.getResponseBody();

            assertNotNull(byteArray);

            expectedBody = "Upload attachment success";

            actualBody = new String(byteArray, "UTF-8");

            assertEquals(expectedBody.length(), actualBody.length());
            assertTrue(expectedBody.contains(actualBody));
        } finally {
            /* Always always call releaseConnection() in finally. */
            getMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/~admin/Home/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/%40exports/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/Home.txt",
                            getWebdavServletUrl() + "/Personal/~admin/Home/Home.url",
                            getWebdavServletUrl() + "/Personal/~admin/Home/test.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test DELETE command by removing attachment from page in Personal Space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testDelPersonalRemoveAttachmentAsFile() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/test.txt";
        PutMethod putMethod = new PutMethod(targetUrl);
        RequestEntity requestEntity = new StringRequestEntity("Upload attachment success");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(HttpStatus.SC_CREATED, statusCode);
        } finally {
            putMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/~admin/Home/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/%40exports/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/Home.txt",
                            getWebdavServletUrl() + "/Personal/~admin/Home/Home.url",
                            getWebdavServletUrl() + "/Personal/~admin/Home/test.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/test.txt";
        DeleteMethod deleteMethod = new DeleteMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(deleteMethod);

            assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
        } finally {
            deleteMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(5, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/~admin/Home/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/%40exports/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/Home.txt",
                            getWebdavServletUrl() + "/Personal/~admin/Home/Home.url",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test PUT command by updating page content to Personal Space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testPutPersonalUpdatePageContentAsFile() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/Home.txt";
        PutMethod putMethod = new PutMethod(targetUrl);
        RequestEntity requestEntity = new StringRequestEntity("updated Home page successfully");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
        } finally {
            putMethod.releaseConnection();
        }

        GetMethod getMethod = new GetMethod(targetUrl);

        try {
            byte[] byteArray;
            String expectedBody;
            String actualBody;

            httpClient.executeMethod(getMethod);

            byteArray = getMethod.getResponseBody();

            assertNotNull(byteArray);

            expectedBody = "updated Home page successfully";

            actualBody = new String(byteArray, "UTF-8");

            assertEquals(expectedBody.length(), actualBody.length());
            assertTrue(expectedBody.contains(actualBody));
        } finally {
            /* Always always call releaseConnection() in finally. */
            getMethod.releaseConnection();
        }
    }


    /**
     * Test PUT command by drag & drop folder with subfolder from desktop into WebDAV Client Personal Space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testPutPersonalUploadPageWithSubPageAndTextFiles() throws IOException, DavException {
        for (int i = 1; i < 5; ++i) {
            if (i == 1 || i == 3) {
                String targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage";
                if (i == 3)
                    targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage/subpage";
                MkColMethod mkColMethod = new MkColMethod(targetUrl);

                try {
                    int statusCode = httpClient.executeMethod(mkColMethod);
                    assertEquals(HttpStatus.SC_CREATED, statusCode);
                } finally {
		    		/* Always always call releaseConnection() in finally. */
                    mkColMethod.releaseConnection();
                }
            } else if (i == 2 || i == 4) {
                String targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage/newpage.txt";
                if (i == 4)
                    targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage/subpage/subpage.txt";
                PutMethod putMethod = new PutMethod(targetUrl);
                RequestEntity requestEntity = new StringRequestEntity("This is newpage");
                if (i == 4)
                    requestEntity = new StringRequestEntity("This is subpage");
                putMethod.setRequestEntity(requestEntity);

                try {
                    int statusCode = httpClient.executeMethod(putMethod);

                    assertEquals(HttpStatus.SC_NO_CONTENT, statusCode);
                } finally {
                    putMethod.releaseConnection();
                }
            }
        }

        String targetUrl = getWebdavServletUrl() + "/Personal/~admin";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(5, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/~admin/",
                            getWebdavServletUrl() + "/Personal/~admin/%40news/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/",
                            getWebdavServletUrl() + "/Personal/~admin/newpage/",
                            getWebdavServletUrl() + "/Personal/~admin/admin.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/~admin/newpage/",
                            getWebdavServletUrl() + "/Personal/~admin/newpage/%40exports/",
                            getWebdavServletUrl() + "/Personal/~admin/newpage/%40versions/",
                            getWebdavServletUrl() + "/Personal/~admin/newpage/subpage/",
                            getWebdavServletUrl() + "/Personal/~admin/newpage/newpage.txt",
                            getWebdavServletUrl() + "/Personal/~admin/newpage/newpage.url",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage/subpage";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(5, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/~admin/newpage/subpage/",
                            getWebdavServletUrl() + "/Personal/~admin/newpage/subpage/%40exports/",
                            getWebdavServletUrl() + "/Personal/~admin/newpage/subpage/%40versions/",
                            getWebdavServletUrl() + "/Personal/~admin/newpage/subpage/subpage.txt",
                            getWebdavServletUrl() + "/Personal/~admin/newpage/subpage/subpage.url",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage/newpage.txt";
        GetMethod getMethod = new GetMethod(targetUrl);

        try {
            byte[] byteArray;
            String expectedBody;
            String actualBody;

            httpClient.executeMethod(getMethod);

            byteArray = getMethod.getResponseBody();

            assertNotNull(byteArray);

            expectedBody = "This is newpage";

            actualBody = new String(byteArray, "UTF-8");

            assertEquals(expectedBody.length(), actualBody.length());
            assertTrue(expectedBody.contains(actualBody));
        } finally {
            /* Always always call releaseConnection() in finally. */
            getMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage/subpage/subpage.txt";
        getMethod = new GetMethod(targetUrl);

        try {
            byte[] byteArray;
            String expectedBody;
            String actualBody;

            httpClient.executeMethod(getMethod);

            byteArray = getMethod.getResponseBody();

            assertNotNull(byteArray);

            expectedBody = "This is subpage";

            actualBody = new String(byteArray, "UTF-8");

            assertEquals(expectedBody.length(), actualBody.length());
            assertTrue(expectedBody.contains(actualBody));
        } finally {
            /* Always always call releaseConnection() in finally. */
            getMethod.releaseConnection();
        }
    }

    // ## < TEST PERMISSIONS > ##

    /**
     * Test PUT command by updating page content permission to personal space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testPutPersonalTestUpdatePagePermissionAsFile() throws IOException, DavException {
        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM)
                , new UsernamePasswordCredentials("sakura", "sakura")
        );

        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/Home.txt";
        PutMethod putMethod = new PutMethod(targetUrl);
        RequestEntity requestEntity = new StringRequestEntity("There is something now");
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(HttpStatus.SC_FORBIDDEN, statusCode);
        } finally {
            putMethod.releaseConnection();
        }

        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM
                )
                , new UsernamePasswordCredentials("admin", "admin")
        );
    }

    public void testDownloadPdf() throws IOException, DavException {
        String url = new StringBuilder(getWebdavServletUrl()).append("/Global/ds/Index/%40exports/Index.pdf").toString();
        GetMethod getMethod = new GetMethod(url);
        InputStream pdfIn = null;

        try {
            int statusCode = httpClient.executeMethod(getMethod);
            assertEquals(200, statusCode);


            pdfIn = getMethod.getResponseBodyAsStream();

            // Try to read it
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            IOUtils.copy(pdfIn, outputStream);
            assertTrue(outputStream.size() > 0);
        } finally {
            IOUtils.closeQuietly(pdfIn);
            getMethod.releaseConnection();
        }
    }
}
