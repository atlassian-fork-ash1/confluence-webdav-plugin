package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.spaces.Space;
import org.apache.commons.io.IOUtils;
import org.apache.jackrabbit.webdav.io.OutputContext;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class PageUrlResourceTestCase extends AbstractConfluenceResourceTestCase {

    private Space space;
    private Page page;
    private PageUrlResourceImpl pageUrlResource;

    @Before
    public void initialise() {
        space = new Space("ds");
        page = new Page();
        page.setTitle("test");
        page.setSpace(space);

        when(spaceManager.getSpace(space.getKey())).thenReturn(space);
        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/" + page.getTitle() + ".url");

        pageUrlResource = new PageUrlResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                settingsManager, webdavSettingsManager, pageManager,
                space.getKey(), page.getTitle());
    }

    @Test
    public void testDoesNotExistIfPageUrlDisabled() {
        when(permissionManager.hasPermission(user, Permission.VIEW, page)).thenReturn(true);

        assertFalse(pageUrlResource.exists());
        verify(webdavSettingsManager).isContentUrlResourceEnabled();
    }

    @Test
    public void testUrlContentEncodedWithSpecifiedCharset() throws IOException {
        String expectedOutput = "[InternetShortcut]\r\n" +
                "URL=http://localhost/display/ds/test\r\n";
        byte[] expectedOutputBytes = expectedOutput.getBytes("UTF-8");

        page.setLastModificationDate(new Date());

        Settings globalSettings = new Settings();

        globalSettings.setBaseUrl("http://localhost");
        globalSettings.setDefaultEncoding("UTF-8");

        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);
        when(settingsManager.getGlobalSettings()).thenReturn(globalSettings);

        OutputContext outputContext = mock(OutputContext.class);


        InputStream in = null;
        ByteArrayOutputStream out = null;

        try {
            in = pageUrlResource.getContent();
            out = new ByteArrayOutputStream();


            when(outputContext.hasStream()).thenReturn(true);
            when(outputContext.getOutputStream()).thenReturn(out);

            pageUrlResource.spool(outputContext);

            assertEquals(
                    expectedOutput,
                    new String(out.toByteArray(), "UTF-8")
            );

            verify(outputContext).setContentLength(expectedOutputBytes.length);
            verify(outputContext).setContentType(pageUrlResource.getContentTypeBase());
            verify(outputContext).setModificationTime(page.getLastModificationDate().getTime());
        } finally {
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(in);
        }
    }

    @Test
    public void testCreationTimeEqualsPageCreationTime() {
        Date now = new Date();

        page.setCreationDate(now);
        assertEquals(now.getTime(), pageUrlResource.getCreationtTime());
    }

    @Test
    public void testDisplayNameEqualsToBlogPostTitleSuffixedByDotTxt() {
        assertEquals(
                page.getTitle() + ".url",
                pageUrlResource.getDisplayName()
        );
    }

}
