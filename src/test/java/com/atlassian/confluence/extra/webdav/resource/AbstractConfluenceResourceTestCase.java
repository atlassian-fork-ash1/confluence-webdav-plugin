package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.extra.webdav.ConfluenceLocatorFactory;
import com.atlassian.confluence.extra.webdav.ConfluenceResourceFactory;
import com.atlassian.confluence.extra.webdav.DavResourceFactoryPluginManager;
import com.atlassian.confluence.extra.webdav.WebdavSettings;
import com.atlassian.confluence.extra.webdav.WebdavSettingsManager;
import com.atlassian.confluence.extra.webdav.job.ContentJobQueue;
import com.atlassian.confluence.importexport.ImportExportManager;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.servlet.download.SafeContentHeaderGuesser;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.user.ConfluenceUserPreferences;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.user.persistence.dao.ConfluenceUserDao;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.impl.DefaultUser;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.jackrabbit.webdav.DavLocatorFactory;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;
import org.junit.Before;
import org.junit.Rule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.mockito.Mockito.when;

public abstract class AbstractConfluenceResourceTestCase {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    protected DavLocatorFactory davLocatorFactory;
    protected DavResourceLocator davResourceLocator;
    protected DavResourceFactory davResourceFactory;

    protected ConfluenceUser user;
    protected ConfluenceDavSession davSession;

    @Mock
    protected LockManager lockManager;
    @Mock
    protected BootstrapManager bootstrapManager;
    @Mock
    protected UserAccessor userAccessor;
    @Mock
    protected FormatSettingsManager formatSettingsManager;
    @Mock
    protected ImportExportManager importExportManager;
    @Mock
    protected SettingsManager settingsManager;
    @Mock
    protected WebdavSettingsManager webdavSettingsManager;
    @Mock
    protected PermissionManager permissionManager;
    @Mock
    protected SpaceManager spaceManager;
    @Mock
    protected PageManager pageManager;
    @Mock
    protected XhtmlContent xhtmlContent;
    @Mock
    protected AttachmentManager attachmentManager;
    @Mock
    protected TransactionTemplate transactionTemplate;
    @Mock
    protected ContentJobQueue contentJobQueue;
    @Mock
    protected ContainerContext containerContext;
    @Mock
    protected ConfluenceUserPreferences confluenceUserPreferences;
    @Mock
    protected DavResourceFactoryPluginManager davResourceFactoryPluginManager;
    @Mock
    protected SafeContentHeaderGuesser attachmentSafeContentHeaderGuesser;
    @Mock
    protected ConfluenceUserDao confluenceUserDao;

    protected String prefix;
    protected String workspaceName;
    protected String workspacePath;
    protected WebdavSettings webdavSettings;
    protected MockHttpServletResponse mockHttpServletResponse;

    @Before
    public void setUp() {
        user = new ConfluenceUserImpl(new DefaultUser("admin"));
        davSession = new ConfluenceDavSession(user.getName());
        AuthenticatedUserThreadLocal.set(user);

        ContainerManager.getInstance().setContainerContext(containerContext);

        when(containerContext.getComponent("confluenceUserDao")).thenReturn(confluenceUserDao);
        when(confluenceUserDao.findByUsername("admin")).thenReturn(user);

        workspaceName = "default";
        prefix = "/plugins/servlet/confluence";
        workspacePath = "/" + workspaceName;

        webdavSettings = new WebdavSettings();

        davLocatorFactory = new ConfluenceLocatorFactory("/plugins/servlet/confluence");
        davResourceFactory = new ConfluenceResourceFactory(
                bootstrapManager, userAccessor, formatSettingsManager, importExportManager, settingsManager, webdavSettingsManager,
                permissionManager, spaceManager, pageManager, xhtmlContent, attachmentManager, attachmentSafeContentHeaderGuesser, contentJobQueue
        );

        mockHttpServletResponse = new MockHttpServletResponse();
        ServletActionContext.setResponse(mockHttpServletResponse);
    }

    static class MockHttpServletResponse implements HttpServletResponse {
        //stores headers set by the call to addHeader(), so they can be asserted with.
        final Map<String, List<String>> headers = new HashMap<>();

        @Override
        public void addCookie(Cookie cookie) {
        }

        @Override
        public boolean containsHeader(String s) {
            return headers.containsKey(s);
        }

        @Override
        public String encodeURL(String s) {
            return null;
        }

        @Override
        public String encodeRedirectURL(String s) {
            return null;
        }

        @Override
        public String encodeUrl(String s) {
            return null;
        }

        @Override
        public String encodeRedirectUrl(String s) {
            return null;
        }

        @Override
        public void sendError(int i, String s) {
        }

        @Override
        public void sendError(int i) {
        }

        @Override
        public void sendRedirect(String s) {
        }

        @Override
        public void setDateHeader(String s, long l) {
            setHeader(s, "" + l);
        }

        @Override
        public void addDateHeader(String s, long l) {
            addHeader(s, "" + l);
        }

        @Override
        public void setHeader(String s, String s2) {
            headers.put(s, Collections.singletonList(s2));
        }

        @Override
        public void addHeader(String s, String s2) {
            List<String> header = headers.computeIfAbsent(s, k -> new ArrayList<>());
            header.add(s2);
        }

        @Override
        public void setIntHeader(String s, int i) {
            setHeader(s, "" + i);
        }

        @Override
        public void addIntHeader(String s, int i) {
            addHeader(s, "" + i);
        }

        @Override
        public void setStatus(int i) {
        }

        @Override
        public void setStatus(int i, String s) {
        }

        @Override
        public int getStatus() {
            return 0;
        }

        @Override
        public String getHeader(String name) {
            List<String> strings = headers.get(name);
            if (strings == null) {
                return null;
            } else {
                return strings.get(0);
            }
        }

        @Override
        public Collection<String> getHeaders(String name) {
            return headers.get(name);
        }

        @Override
        public Collection<String> getHeaderNames() {
            return headers.keySet();
        }

        @Override
        public String getCharacterEncoding() {
            return null;
        }

        @Override
        public String getContentType() {
            return null;
        }

        @Override
        public ServletOutputStream getOutputStream() {
            return null;
        }

        @Override
        public PrintWriter getWriter() {
            return null;
        }

        @Override
        public void setCharacterEncoding(String s) {

        }

        @Override
        public void setContentLength(int i) {
        }

        @Override
        public void setContentType(String s) {
            setHeader("Content-Type", s);
        }

        @Override
        public void setBufferSize(int i) {
        }

        @Override
        public int getBufferSize() {
            return 0;
        }

        @Override
        public void flushBuffer() {
        }

        @Override
        public void resetBuffer() {
        }

        @Override
        public boolean isCommitted() {
            return false;
        }

        @Override
        public void reset() {
        }

        @Override
        public void setLocale(Locale locale) {
        }

        @Override
        public Locale getLocale() {
            return null;
        }

        @Override
        public void setContentLengthLong(long len) {
        }
    }
}
