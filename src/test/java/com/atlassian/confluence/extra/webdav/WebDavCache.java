package com.atlassian.confluence.extra.webdav;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheEntryListener;
import com.atlassian.cache.Supplier;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ParametersAreNonnullByDefault
public class WebDavCache<K, V> implements Cache<K, V> {

    private final String cacheName;
    private final Map<K, V> cache = new ConcurrentHashMap<>();

    public WebDavCache(String cacheName) {
        this.cacheName = cacheName;
    }

    @Nonnull
    @Override
    public String getName() {
        return cacheName;
    }

    @Override
    public boolean containsKey(K key) {
        return cache.containsKey(key);
    }

    @Nonnull
    @Override
    public Collection<K> getKeys() {
        return new ArrayList<K>(cache.keySet());
    }

    @Override
    public V get(K key) {
        return cache.get(key);
    }

    @Nonnull
    @Override
    public V get(K key, Supplier<? extends V> supplier) {
        if (!cache.containsKey(key))
            cache.put(key, supplier.get());
        return cache.get(key);
    }

    @Override
    public void put(K key, V value) {
        cache.put(key, value);
    }

    @Override
    public void remove(K key) {
        cache.remove(key);
    }

    @Override
    public void removeAll() {
        cache.clear();
    }

    @Override
    public V putIfAbsent(K key, V value) {
        if (!cache.containsKey(key))
            cache.put(key, value);
        return cache.get(key);
    }

    @Override
    public boolean replace(K key, V oldValue, V newValue) {
        if (oldValue.equals(cache.get(key))) {
            cache.put(key, newValue);
            return true;
        }
        return false;
    }

    @Override
    public void addListener(CacheEntryListener cacheEntryListener, boolean b) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void removeListener(CacheEntryListener cacheEntryListener) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(K key, V value) {
        if (value.equals(cache.get(key))) {
            cache.remove(key);
            return true;
        }
        return false;
    }

}
