package com.atlassian.confluence.extra.webdav.servlet.filter;

import com.atlassian.confluence.extra.webdav.util.WebdavConstants;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavMethods;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ReverseProxyCopyAndMoveDestinationFixTestCase extends TestCase {
    @Mock
    private HttpServletRequest servletRequest;

    @Mock
    private FilterConfig filterConfig;

    @Mock
    private FilterChain filterChain;

    private Filter filterFix;

    public void setUp() throws Exception {
        super.setUp();

        MockitoAnnotations.initMocks(this);


        filterFix = new ReverseProxyCopyAndMoveDestinationFix();
        filterFix.init(filterConfig);
    }

    @Override
    protected void tearDown() throws Exception {
        servletRequest = null;
        filterConfig = null;
        filterChain = null;
        super.tearDown();
    }

    public void testDestinationHeaderNotRewrittenServletRequestNotAnInstanceOfHttpServletRequest() throws ServletException, IOException {
        ServletRequest nonHttpServletRequest = mock(ServletRequest.class);

        filterFix.doFilter(nonHttpServletRequest, null, filterChain);
        verify(filterChain).doFilter(nonHttpServletRequest, null);
    }

    public void testDesintationHeaderRewrittenAccordingToHostRequestParameterWhenCopying() throws ServletException, IOException {
        final String rewrittenDestinationHeader = "http://10.60.1.187:8082/confluence/plugins/servlet/confluence/default/Global/ds/Confluence%20Overview/Index";

        when(servletRequest.getMethod()).thenReturn(DavMethods.METHOD_COPY);
        when(servletRequest.getHeader(WebdavConstants.HEADER_DESTINATION)).thenReturn("http://10.60.1.187/webdav/Global/ds/Confluence%20Overview/Index");
        when(servletRequest.getScheme()).thenReturn("http");
        when(servletRequest.getHeader(WebdavConstants.HEADER_HOST)).thenReturn("10.60.1.187:8082");
        when(servletRequest.getContextPath()).thenReturn("/confluence");

        filterFix.doFilter(servletRequest, null, filterChain);

        verify(filterChain).doFilter(
                argThat(
                        (ArgumentMatcher<HttpServletRequest>) request -> StringUtils.equals(rewrittenDestinationHeader, request.getHeader(WebdavConstants.HEADER_DESTINATION))
                ),
                any()
        );
    }

    public void testDesintationHeaderRewrittenAccordingToHostRequestParameterWhenMoving() throws ServletException, IOException {
        final String rewrittenDestinationHeader = "http://10.60.1.187:8082/confluence/plugins/servlet/confluence/default/Global/ds/Confluence%20Overview/Index";

        when(servletRequest.getMethod()).thenReturn(DavMethods.METHOD_MOVE);
        when(servletRequest.getHeader(WebdavConstants.HEADER_DESTINATION)).thenReturn("http://10.60.1.187/webdav/Global/ds/Confluence%20Overview/Index");
        when(servletRequest.getScheme()).thenReturn("http");
        when(servletRequest.getHeader(WebdavConstants.HEADER_HOST)).thenReturn("10.60.1.187:8082");
        when(servletRequest.getContextPath()).thenReturn("/confluence");

        filterFix.doFilter(servletRequest, null, filterChain);

        verify(filterChain).doFilter(
                argThat(
                        (ArgumentMatcher<HttpServletRequest>) request -> StringUtils.equals(rewrittenDestinationHeader, request.getHeader(WebdavConstants.HEADER_DESTINATION))
                ),
                any());
    }
}
