package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceIterator;
import org.apache.jackrabbit.webdav.io.InputContext;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SpaceResourceImplTestCase extends AbstractConfluenceResourceTestCase {

    @Mock
    private DavResource davResource;
    @Mock
    private InputContext inputContext;

    private Space space;
    private Page page;
    private SpaceResourceImpl spaceResourceImpl;
    private SpaceDescription spaceDescription;

    @Before
    public void initialise() {
        spaceDescription = new SpaceDescription();
        spaceDescription.setBodyAsString("");

        space = new Space("ds");
        space.setName("Demo");
        space.setDescription(spaceDescription);

        page = new Page();
        page.setSpace(space);
        page.setTitle("Test");

        when(spaceManager.getSpace(space.getKey())).thenReturn(space);

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath + "/Global/" + space.getKey()
        );

        spaceResourceImpl = new SpaceResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                permissionManager, spaceManager, pageManager, attachmentManager,
                space.getKey());
    }

    @Test
    public void testCreatePageWithoutPermissionDenied() {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle());

        try {
            spaceResourceImpl.addMember(davResource, inputContext);
            fail("DavException expected here.");
        } catch (DavException e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, e.getErrorCode());
            verify(pageManager, never()).saveContentEntity(
                    anyObject(),
                    anyObject()
            );
        }
    }

    @Test
    public void testCreatePageWithPermission() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle());
        when(permissionManager.hasCreatePermission(user, space, Page.class)).thenReturn(true);

        spaceResourceImpl.addMember(davResource, inputContext);

        verify(pageManager).saveContentEntity(page, null);
    }

    @Test
    public void testCreatePageWithNonUniqueTitleInSpace() {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle());
        when(permissionManager.hasCreatePermission(user, space, Page.class)).thenReturn(true);
        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);

        try {
            spaceResourceImpl.addMember(davResource, inputContext);
            fail("DavException expected here.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_BAD_REQUEST, de.getErrorCode());
            verify(pageManager, never()).saveContentEntity(
                    anyObject(),
                    anyObject()
            );
        }
    }

    @Test
    public void testCreateAttachmentInSpaceAttachesItAsSpaceDescriptionAttachment() throws DavException, IOException {
        final String attachmentName = "Test";

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + attachmentName);
        when(inputContext.hasStream()).thenReturn(true);
        when(permissionManager.hasCreatePermission(user, spaceDescription, Attachment.class)).thenReturn(true);

        InputStream attachmentData = new ByteArrayInputStream(new byte[0]);
        when(inputContext.getInputStream()).thenReturn(attachmentData);

        spaceResourceImpl.addMember(davResource, inputContext);

        verify(attachmentManager).saveAttachment(
                argThat(
                        attachment -> StringUtils.equals(attachment.getFileName(), attachmentName)
                                && attachment.getContainer() == spaceDescription
                ),
                any(),
                isA(InputStream.class));
    }

    @Test
    public void testAttachmentAddedAsMemberWithLocallyResolvedContentType() throws DavException, IOException {
        final String attachmentName = "README.txt";

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + attachmentName);
        when(inputContext.hasStream()).thenReturn(true);
        when(permissionManager.hasCreatePermission(user, spaceDescription, Attachment.class)).thenReturn(true);

        InputStream attachmentData = new ByteArrayInputStream(new byte[0]);
        when(inputContext.getInputStream()).thenReturn(attachmentData);

        spaceResourceImpl.addMember(davResource, inputContext);

        verify(attachmentManager).saveAttachment(
                argThat(
                        attachment -> StringUtils.equals(attachment.getFileName(), attachmentName)
                                && attachment.getContainer() == spaceDescription
                                && StringUtils.equals("text/plain", attachment.getContentType())
                ),
                any(),
                isA(InputStream.class));
    }

    @Test
    public void testUpdateSpaceDescriptionWithEditSpacePermission() throws DavException, IOException {

        final String attachmentName = space.getName() + ".txt";
        final String spaceDescriptionContent = "Updated space content";

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + attachmentName);
        when(inputContext.hasStream()).thenReturn(true);
        when(permissionManager.hasPermission(user, Permission.EDIT, space)).thenReturn(true);
        when(inputContext.getInputStream()).thenReturn(new ByteArrayInputStream(spaceDescriptionContent.getBytes(UTF_8)));

        spaceResourceImpl.addMember(davResource, inputContext);

        verify(spaceManager).saveSpace(
                argThat(
                        space -> StringUtils.equals(spaceDescriptionContent, space.getDescription().getBodyContent().getBody())
                )
        );
    }

    @Test
    public void testUpdateSpaceDescriptionWithoutEditSpacePermission() {

        final String attachmentName = space.getName() + ".txt";

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + attachmentName);

        try {
            spaceResourceImpl.addMember(davResource, inputContext);
            fail("DavException expected here.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
            verify(spaceManager, never()).saveSpace(any());
        }
    }

    @Test
    public void testRemoveSpaceDescriptionWithoutEditSpacePermission() {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + space.getName() + ".txt");

        try {
            spaceResourceImpl.removeMember(davResource);
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
            verify(attachmentManager, never()).removeAttachmentFromServer(any());
            verify(pageManager, never()).trashPage(any());
        }
    }

    @Test
    public void testRemoveSpaceDescriptionHidesIt() throws DavException {
        final DavResource otherResource = mock(DavResource.class);
        final DavResource spaceContentResource = mock(DavResource.class);

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + space.getName() + ".txt");
        when(permissionManager.hasPermission(user, Permission.EDIT, space)).thenReturn(true);

        assertFalse(davSession.getResourceStates().isSpaceDescriptionHidden(space));

        spaceResourceImpl = new SpaceResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                permissionManager, spaceManager, pageManager, attachmentManager,
                space.getKey()) {
            protected DavResource getSpaceContentResource() {
                return spaceContentResource;
            }

            protected DavResource getBlogPostsResource() {
                return otherResource;
            }

            protected List<DavResource> getPageResources() {
                return Collections.emptyList();
            }

            protected List<DavResource> getSpaceAttachmentResources() {
                return Collections.emptyList();
            }
        };

        spaceResourceImpl.removeMember(davResource);
        assertTrue(davSession.getResourceStates().isSpaceDescriptionHidden(space));

        for (DavResourceIterator davResourceIterator = spaceResourceImpl.getMembers(); davResourceIterator.hasNext(); )
            if (spaceContentResource == davResourceIterator.nextResource())
                fail("Space content not hidden.");
    }

    @Test
    public void testRemoveSpaceAttachment() throws DavException {
        final String tempFileName = "._" + space.getName() + ".txt";
        Attachment tempFile = new Attachment();

        tempFile.setContainer(spaceDescription);
        tempFile.setFileName(tempFileName);

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + tempFileName);
        when(attachmentManager.getAttachment(spaceDescription, tempFileName)).thenReturn(tempFile);

        spaceResourceImpl.removeMember(davResource);
        verify(attachmentManager).removeAttachmentFromServer(tempFile);
    }

    @Test
    public void testRemoveTopLevelPageWithoutPermission() {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle());
        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);

        try {
            spaceResourceImpl.removeMember(davResource);
            fail("DavException expected here.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
            verify(pageManager, never()).trashPage(any());
        }
    }

    @Test
    public void testRemoveTopLevelPageWithPermission() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle());
        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);
        when(permissionManager.hasPermission(user, Permission.REMOVE, page)).thenReturn(true);

        spaceResourceImpl.removeMember(davResource);
        verify(pageManager).trashPage(page);
    }

    @Test
    public void testCreatePageWithTextEditTempFolder() {
        davSession.setUserAgent("WebDavFS/*.*");

        Page page = new Page();
        page.setTitle("(A Document Being Saved By TextEdit)");

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle());
        when(permissionManager.hasCreatePermission(user, space, Page.class)).thenReturn(true);

        try {
            spaceResourceImpl.addMember(davResource, inputContext);
        } catch (DavException e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, e.getErrorCode());
            verify(pageManager, never()).saveContentEntity(any(), any());
        }
    }

    @Test
    public void testNewsAndTopLevelPagesListedAsChildResources() {
        Page topLevelPage = new Page();
        topLevelPage.setSpace(space);
        topLevelPage.setTitle("Top level page");

        when(permissionManager.getPermittedEntities(eq(user), eq(Permission.VIEW), (List) any())).thenReturn(singletonList(topLevelPage));
        when(pageManager.getPage(space.getKey(), topLevelPage.getTitle())).thenReturn(topLevelPage);

        List<DavResource> childResources = new ArrayList<>(spaceResourceImpl.getMemberResources());

        assertEquals(3, childResources.size());

        SpaceContentResourceImpl spaceContentResource = (SpaceContentResourceImpl) childResources.get(0);
        assertEquals(space, spaceContentResource.getSpace());

        BlogPostsResourceImpl blogPostsResource = (BlogPostsResourceImpl) childResources.get(1);
        assertEquals(space, blogPostsResource.getSpace());

        PageResourceImpl pageResource = (PageResourceImpl) childResources.get(2);
        assertEquals(topLevelPage, pageResource.getPage());
    }

    @Test
    public void testSpaceAttachmentsExposed() {

        Attachment tempFile = new Attachment();
        tempFile.setFileName("._temp.txt");
        tempFile.setContainer(spaceDescription);

        spaceDescription.addAttachment(tempFile);


        when(attachmentManager.getLatestVersionsOfAttachments(spaceDescription)).thenReturn(singletonList(tempFile));
        when(attachmentManager.getAttachment(spaceDescription, tempFile.getFileName())).thenReturn(tempFile);

        List<DavResource> childResources = new ArrayList<>(spaceResourceImpl.getMemberResources());

        assertEquals(3, childResources.size());

        SpaceContentResourceImpl spaceContentResource = (SpaceContentResourceImpl) childResources.get(0);
        assertEquals(space, spaceContentResource.getSpace());

        BlogPostsResourceImpl blogPostsResource = (BlogPostsResourceImpl) childResources.get(1);
        assertEquals(space, blogPostsResource.getSpace());

        SpaceAttachmentResourceImpl spaceAttachmentResource = (SpaceAttachmentResourceImpl) childResources.get(2);
        assertEquals(tempFile, spaceAttachmentResource.getAttachment());
    }
}
