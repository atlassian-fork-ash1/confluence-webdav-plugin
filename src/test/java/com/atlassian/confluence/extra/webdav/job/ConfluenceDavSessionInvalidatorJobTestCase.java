package com.atlassian.confluence.extra.webdav.job;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSessionStore;
import com.atlassian.scheduler.JobRunnerRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ConfluenceDavSessionInvalidatorJobTestCase {
    @Mock
    private ConfluenceDavSessionStore confluenceDavSessionStore;
    @Mock
    private JobRunnerRequest jobRunnerRequest;

    @InjectMocks
    private ConfluenceDavSessionInvalidatorJob confluenceDavSessionInvalidatorJob;

    @Test
    public void testJobAsksSessionStoreToInvalidateExpiredSessionOnExecute() {
        confluenceDavSessionInvalidatorJob.runJob(jobRunnerRequest);

        verify(confluenceDavSessionStore).invalidateExpiredSessions();
    }
}
