package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.core.TimeZone;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.spaces.Space;
import org.apache.jackrabbit.webdav.DavResource;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BlogPostsResourceImplTestCase extends AbstractConfluenceResourceTestCase {

    private Space space;
    private BlogPostsResourceImpl blogPostsResourceImpl;

    @Before
    public void initialise() {
        space = new Space("ds");

        when(spaceManager.getSpace(space.getKey())).thenReturn(space);

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath + "/Global/" + space.getKey() + "/" + BlogPostsResourceImpl.DISPLAY_NAME
        );

        blogPostsResourceImpl = new BlogPostsResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                permissionManager, spaceManager, pageManager,
                space.getKey());
    }

    @Test
    public void testExistsIfUserHasViewPermissionOnSpace() {
        when(permissionManager.hasPermission(user, Permission.VIEW, space)).thenReturn(true);
        assertTrue(blogPostsResourceImpl.exists());
    }

    @Test
    public void testDoesNotExistIfUserHasNoViewPermissionOnSpace() {
        assertFalse(blogPostsResourceImpl.exists());
        verify(permissionManager).hasPermission(user, Permission.VIEW, space);
    }

    @Test
    public void testBlogPostsListedByYearAsChildren() throws ParseException {
        BlogPost blogPost = new BlogPost();
        blogPost.setTitle("testBlog 1");
        blogPost.setSpace(space);
        blogPost.setCreationDate(new Date());

        BlogPost blogPost2 = new BlogPost();
        blogPost2.setTitle("testBlog 2");
        blogPost2.setSpace(space);
        blogPost2.setCreationDate(new SimpleDateFormat("yyyy/MM/dd").parse("2007/01/01"));

        BlogPost blogPost3 = new BlogPost();
        blogPost3.setTitle("testBlog 3");
        blogPost3.setSpace(space);
        blogPost3.setCreationDate(new SimpleDateFormat("yyyy/MM/dd").parse("2007/02/01"));

        when(spaceManager.getSpace(space.getKey())).thenReturn(space);
        when(permissionManager.getPermittedEntities(eq(user), anyObject(), (List) anyObject())).thenReturn(
                asList(blogPost, blogPost2, blogPost3)
        );
        when(userAccessor.getConfluenceUserPreferences(user)).thenReturn(confluenceUserPreferences);
        when(confluenceUserPreferences.getTimeZone()).thenReturn(TimeZone.getDefault());

        List<DavResource> members = new ArrayList<>(blogPostsResourceImpl.getMemberResources());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");

        assertEquals(2, members.size());

        members.sort((o1, o2) -> {
            // Recent blog posts (by the year) at the top)
            return -(((BlogPostsYearResourceImpl) o1).getYearPublished() - ((BlogPostsYearResourceImpl) o2).getYearPublished());
        });


        assertEquals(
                simpleDateFormat.format(blogPost.getCreationDate()),
                String.valueOf(((BlogPostsYearResourceImpl) members.get(0)).getYearPublished())
        );

        assertEquals(
                simpleDateFormat.format(blogPost2.getCreationDate()),
                String.valueOf(((BlogPostsYearResourceImpl) members.get(1)).getYearPublished())
        );

        verify(pageManager).getBlogPosts(space, true);
    }

    @Test
    public void testDisplayedName() {
        assertEquals("@news", blogPostsResourceImpl.getDisplayName());
    }

    @Test
    public void testCreationTimeSameAsContainingSpaceCreationTime() {
        space.setCreationDate(new Date());
        when(spaceManager.getSpace(space.getKey())).thenReturn(space);

        assertEquals(
                space.getCreationDate().getTime(),
                blogPostsResourceImpl.getCreationtTime()
        );
    }
}
