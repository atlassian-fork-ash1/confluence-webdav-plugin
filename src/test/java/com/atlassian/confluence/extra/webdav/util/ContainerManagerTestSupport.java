package com.atlassian.confluence.extra.webdav.util;

import com.atlassian.confluence.languages.DefaultLocaleManager;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.spring.container.SpringTestContainerContext;
import com.atlassian.user.User;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.StaticApplicationContext;

import java.util.Locale;

public class ContainerManagerTestSupport {
    private final StaticApplicationContext context;

    public ContainerManagerTestSupport() {
        context = new StaticApplicationContext();
    }

    public ContainerManagerTestSupport init() {
        context.refresh();
        ContainerManager.getInstance().setContainerContext(new SpringTestContainerContext(context));
        return this;
    }

    public static void reset() {
        ContainerManager.getInstance().setContainerContext(null);
        ContainerManager.resetInstance();
    }

    public ContainerManagerTestSupport registerDummyLocaleManager() {
        return registerComponent("localeManager", new DefaultLocaleManager() {
            @Override
            public Locale getLocale(User user) {
                return Locale.UK;
            }
        });
    }


    public ContainerManagerTestSupport registerComponent(String name, Object component) {
        getBeanFactory().registerSingleton(name, component);
        return this;
    }

    public ConfigurableListableBeanFactory getBeanFactory() {
        return context.getBeanFactory();
    }
}
