package com.atlassian.confluence.extra.webdav.servlet.filter;

import com.atlassian.confluence.extra.webdav.util.ContainerManagerTestSupport;
import com.atlassian.confluence.extra.webdav.util.WebdavConstants;
import com.atlassian.plugins.whitelist.OutboundWhitelist;
import org.apache.jackrabbit.webdav.DavMethods;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class WebdavRequestForwardFilterTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    private WebdavRequestForwardFilter requestForwardFilter;
    @Mock
    private HttpServletRequest httpServletRequest;
    @Mock
    private HttpServletResponse htppServletResponse;
    @Mock
    private ServletContext servletContext;
    @Mock
    private FilterChain filterChain;
    @Mock
    private OutboundWhitelist outboundWhitelist;
    @Mock
    private FilterConfig filterConfig;
    @Mock
    private RequestDispatcher requestDispatcher;

    @Before
    public void setup() {
        final ContainerManagerTestSupport containerManagerTestSupport = new ContainerManagerTestSupport();
        containerManagerTestSupport.registerDummyLocaleManager().init();
        containerManagerTestSupport.registerComponent("outboundWhitelist", outboundWhitelist);
    }

    @Test
    public void testForwardFilterDoesNotHandleOsxFinder() throws IOException, ServletException {
        final StringBuffer hasHandledOsx = new StringBuffer();

        when(httpServletRequest.getHeader(WebdavConstants.HEADER_USER_AGENT)).thenReturn("WebDAVFS/1.0");
        initializeRevereProxyFilter(hasHandledOsx);

        requestForwardFilter.doFilter((ServletRequest) httpServletRequest, null, filterChain);

        assertEquals(Boolean.FALSE.toString(), hasHandledOsx.toString());
    }

    @Test
    public void testForwardFilterDoesNotHandleNonEmptyContextPath() throws IOException, ServletException {
        final StringBuffer hasHandledRequest = new StringBuffer();

        when(httpServletRequest.getHeader(WebdavConstants.HEADER_USER_AGENT))
                .thenReturn("Microsoft-WebDAV-MiniRedir/1.0");
        when(httpServletRequest.getContextPath()).thenReturn("/confluence");

        initializeRevereProxyFilter(hasHandledRequest);

        requestForwardFilter.doFilter((ServletRequest) httpServletRequest, null, filterChain);

        assertEquals(Boolean.FALSE.toString(), hasHandledRequest.toString());
    }

    @Test
    public void testForwardFilterOnlyHandlesMicrosoftMiniRedirectorClientAndEmptyContextPath()
            throws IOException, ServletException {
        final StringBuffer hasHandledRequest = new StringBuffer();

        when(httpServletRequest.getHeader(WebdavConstants.HEADER_USER_AGENT))
                .thenReturn("Microsoft-WebDAV-MiniRedir/1.0");
        when(httpServletRequest.getContextPath()).thenReturn("");

        initializeRevereProxyFilter(hasHandledRequest);

        requestForwardFilter.doFilter((ServletRequest) httpServletRequest, null, filterChain);

        assertEquals(Boolean.TRUE.toString(), hasHandledRequest.toString());
    }

    @Test
    public void testRequestForwardingToWebDevServletPath() throws ServletException, IOException {
        setUpFilter("/webdav/Personal/");

        ArgumentCaptor<String> targetUriCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<HttpServletRequest> actualServletRequest =
                ArgumentCaptor.forClass(HttpServletRequest.class);
        ArgumentCaptor<HttpServletResponse> actualServletResponse =
                ArgumentCaptor.forClass(HttpServletResponse.class);

        when(servletContext.getRequestDispatcher(targetUriCaptor.capture())).thenReturn(requestDispatcher);

        requestForwardFilter.doFilter((ServletRequest) httpServletRequest, htppServletResponse, filterChain);

        verify(requestDispatcher).forward(actualServletRequest.capture(), actualServletResponse.capture());

        assertEquals("/plugins/servlet/confluence/default/Personal/", targetUriCaptor.getValue());
        assertEquals(httpServletRequest, actualServletRequest.getValue());
        assertEquals(htppServletResponse, actualServletResponse.getValue());
    }

    @Test
    public void testFirstMSAuthRequestResponse() throws ServletException, IOException {
        setUpFilter("/");

        requestForwardFilter.doFilter((ServletRequest) httpServletRequest, htppServletResponse, filterChain);

        verify(htppServletResponse).addHeader("MS-Author-Via", WebdavConstants.HEADER_DAV);
        verify(htppServletResponse).setStatus(HttpServletResponse.SC_OK);
    }

    private void initializeRevereProxyFilter(StringBuffer hasHandledRequest) throws IOException, ServletException {
        requestForwardFilter = new WebdavRequestForwardFilter() {
            public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                                 FilterChain filterChain) throws IOException, ServletException {
                hasHandledRequest.setLength(0);
                hasHandledRequest.append(Boolean.FALSE.toString());

                super.doFilter(servletRequest, servletResponse, filterChain);
            }

            public void doFilter(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                 FilterChain filterChain) throws IOException, ServletException {
                hasHandledRequest.setLength(0);
                hasHandledRequest.append(Boolean.TRUE.toString());
            }
        };
    }

    private void setUpFilter(String requestURI) throws ServletException {
        requestForwardFilter = new WebdavRequestForwardFilter();
        when(httpServletRequest.getRequestURI()).thenReturn(requestURI);
        when(httpServletRequest.getMethod()).thenReturn(DavMethods.METHOD_OPTIONS);
        when(filterConfig.getInitParameter("mount-point-prefix")).thenReturn("/webdav");
        when(filterConfig.getServletContext()).thenReturn(servletContext);
        requestForwardFilter.init(filterConfig);
    }

}
