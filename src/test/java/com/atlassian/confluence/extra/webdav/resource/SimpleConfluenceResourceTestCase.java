package com.atlassian.confluence.extra.webdav.resource;

import org.apache.jackrabbit.util.Text;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavLocatorFactory;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceIterator;
import org.apache.jackrabbit.webdav.DavResourceIteratorImpl;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.io.OutputContext;
import org.apache.jackrabbit.webdav.lock.SupportedLock;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SimpleConfluenceResourceTestCase extends AbstractConfluenceResourceTestCase {

    @Test
    public void testGetCollectionFromResourceLocatorCreatedWithEscapedPaths() throws DavException {
        DavLocatorFactory davLocatorFactory = mock(DavLocatorFactory.class);

        davResourceLocator = mock(DavResourceLocator.class);
        when(davResourceLocator.getResourcePath()).thenReturn(workspacePath + "/Global/ds/Test");
        when(davResourceLocator.getPrefix()).thenReturn(prefix);
        when(davResourceLocator.getFactory()).thenReturn(davLocatorFactory);

        DavResourceLocator parentResourceLocator = mock(DavResourceLocator.class);
        DavResource parentResource = mock(DavResource.class);

        davResourceFactory = mock(DavResourceFactory.class);
        when(davLocatorFactory.createResourceLocator(Text.escape(prefix), Text.escape(workspacePath + "/Global/ds"))).thenReturn(parentResourceLocator);
        when(davResourceFactory.createResource(parentResourceLocator, davSession)).thenReturn(parentResource);

        DavResource resource = new AbstractConfluenceResource(
                davResourceLocator, davResourceFactory, lockManager, davSession)

        {
            protected long getCreationtTime() {
                return 0;
            }

            protected SupportedLock getSupportedLock() {
                return null;
            }

            public boolean isCollection() {
                return false;
            }

            public String getDisplayName() {
                return "Fake Resource";
            }

            public void spool(OutputContext outputContext) {
                /* No op */
            }

            public DavResourceIterator getMembers() {
                return new DavResourceIteratorImpl(Collections.emptyList());
            }
        };

        assertSame(parentResource, resource.getCollection());
    }
}
