package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceType;
import com.atlassian.confluence.spaces.SpacesQuery;
import org.apache.jackrabbit.webdav.DavResource;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class PersonalSpacesResourceImplTestCase extends AbstractConfluenceResourceTestCase {

    private PersonalSpacesResourceImpl personalSpacesResourceImpl;
    private Space space;

    @Before
    public void initialise() {
        space = new Space("~" + user.getName());

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath,
                workspacePath + "/Global/" + space.getKey()
        );

        personalSpacesResourceImpl = new PersonalSpacesResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                spaceManager
        );
    }

    @Test
    public void testOnlyPersonalSpacesListedAsChildResources() {
        when(spaceManager.getAllSpaces(SpacesQuery.newQuery().forUser(user).withSpaceType(SpaceType.PERSONAL).build())).thenReturn(singletonList(space));
        when(spaceManager.getSpace(space.getKey())).thenReturn(space);

        List<DavResource> childResources = new ArrayList<>(personalSpacesResourceImpl.getMemberResources());

        assertEquals(1, childResources.size());
        assertTrue(childResources.get(0) instanceof SpaceResourceImpl);
    }

    @Test
    public void testCreationTimeIsZero() {
        assertEquals(0, personalSpacesResourceImpl.getCreationtTime());
    }

    @Test
    public void testDisplayName() {
        assertEquals("Personal", personalSpacesResourceImpl.getDisplayName());
    }
}
