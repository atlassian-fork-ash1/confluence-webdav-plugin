package com.atlassian.confluence.extra.webdav;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Tests {@link DefaultDavResourceFactoryPluginManager}.
 */
public class DefaultDavResourceFactoryPluginManagerTestCase {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private PluginEventManager pluginEventManager;
    @Mock
    private DavResourceFactory factory1;
    @Mock
    private DavResourceFactory factory2;
    @Mock
    private DavResourceFactoryModuleDescriptor descriptor1;
    @Mock
    private DavResourceFactoryModuleDescriptor descriptor2;
    private DefaultDavResourceFactoryPluginManager davResourceFactoryPluginManager;

    @Before
    public void setUp() {
        when(descriptor1.getWorkspaceName()).thenReturn("foo");
        when(descriptor1.getModule()).thenReturn(factory1);
        when(descriptor2.getWorkspaceName()).thenReturn("bar");
        when(descriptor2.getModule()).thenReturn(factory2);
        List<DavResourceFactoryModuleDescriptor> twoModuleDescriptors = Arrays.asList(descriptor1, descriptor2);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(DavResourceFactoryModuleDescriptor.class))
                .thenReturn(twoModuleDescriptors);
        this.davResourceFactoryPluginManager = new DefaultDavResourceFactoryPluginManager(pluginAccessor, pluginEventManager);
    }

    @Test
    public void testChoosesCorrectPlugin() {
        assertThat(davResourceFactoryPluginManager.getFactoryForWorkspace("foo"), is(factory1));
        assertThat(davResourceFactoryPluginManager.getFactoryForWorkspace("bar"), is(factory2));
        assertNull(davResourceFactoryPluginManager.getFactoryForWorkspace("baz"));
    }
}
