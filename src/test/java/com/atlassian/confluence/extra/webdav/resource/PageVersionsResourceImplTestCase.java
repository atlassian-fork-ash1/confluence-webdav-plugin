package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.core.VersionHistorySummary;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.spaces.Space;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PageVersionsResourceImplTestCase extends AbstractConfluenceResourceTestCase {

    @Mock
    private DavResource davResource;

    private Space space;
    private Page page;
    private PageVersionsResourceImpl pageVersionsResourceImpl;

    @Before
    public void initialise() {

        space = new Space("ds");

        page = new Page();
        page.setSpace(space);
        page.setTitle("Test");

        when(spaceManager.getSpace(space.getKey())).thenReturn(space);
        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/" + PageVersionsResourceImpl.DISPLAY_NAME
        );

        pageVersionsResourceImpl = new PageVersionsResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                webdavSettingsManager, pageManager,
                space.getKey(), page.getTitle());

        davResource = mock(DavResource.class);
    }

    @Test
    public void testVersionTextUnhiddenOnAdditionAsMember() throws DavException {
        String versionToUnhide = "Version 1.txt";

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@versions/" + versionToUnhide);

        davSession.getResourceStates().hideContentVersionText(page, versionToUnhide);
        assertTrue(davSession.getResourceStates().isContentVersionTextHidden(page, versionToUnhide));

        pageVersionsResourceImpl.addMember(davResource, null);

        assertFalse(davSession.getResourceStates().isContentVersionTextHidden(page, versionToUnhide));
    }

    @Test
    public void testReadmeUnhiddenOnAdditionAsMember() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@versions/" + GeneratedResourceReadMeResource.DISPLAY_NAME);

        davSession.getResourceStates().hideContentVersionsReadme(page);
        assertTrue(davSession.getResourceStates().isContentVersionsReadmeHidden(page));

        pageVersionsResourceImpl.addMember(davResource, null);

        assertFalse(davSession.getResourceStates().isContentVersionsReadmeHidden(page));
    }

    @Test
    public void testOnlyDeletedVersionHiddenOnRemove() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@versions/Version 1.txt");

        pageVersionsResourceImpl.removeMember(davResource);

        assertTrue(davSession.getResourceStates().isContentVersionTextHidden(page, "Version 1.txt"));
        assertFalse(davSession.getResourceStates().isContentVersionTextHidden(page, "Version 2.txt"));
    }

    @Test
    public void testReadmeHiddenOnDelete() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@versions/" + GeneratedResourceReadMeResource.DISPLAY_NAME);

        pageVersionsResourceImpl.removeMember(davResource);

        assertTrue(davSession.getResourceStates().isContentVersionsReadmeHidden(page));
    }

    @Test
    public void testDoesNotExistIfPageVersionsDisabled() {
        when(permissionManager.hasPermission(user, Permission.VIEW, space)).thenReturn(true);
        when(permissionManager.hasPermission(user, Permission.VIEW, page)).thenReturn(true);

        assertFalse(pageVersionsResourceImpl.exists());
        verify(webdavSettingsManager).isContentVersionsResourceEnabled();
    }

    @Test
    public void testVersionTextAndReadMeListedAsChildResources() {
        List<VersionHistorySummary> versionHistories = new ArrayList<>();
        versionHistories.add(new VersionHistorySummary(page));

        page.setCreationDate(new Date());
        page.setVersion(2);

        versionHistories.add(new VersionHistorySummary(page));

        when(pageManager.getVersionHistorySummaries(page)).thenReturn(versionHistories);

        List<DavResource> members = new ArrayList<>(pageVersionsResourceImpl.getMemberResources());

        assertEquals(3, members.size());

        PageVersionContentResourceImpl pageVersionContentResource = (PageVersionContentResourceImpl) members.get(0);

        assertEquals(1, pageVersionContentResource.getVersionNumber());
        assertEquals(page, pageVersionContentResource.getAbstractPage());

        pageVersionContentResource = (PageVersionContentResourceImpl) members.get(1);

        assertEquals(2, pageVersionContentResource.getVersionNumber());
        assertEquals(page, pageVersionContentResource.getAbstractPage());

        GeneratedResourceReadMeResource generatedResourceReadMeResource = (GeneratedResourceReadMeResource) members.get(2);
        assertEquals(page, ((PageVersionsResourceImpl) generatedResourceReadMeResource.getCollection()).getContentEntityObject());
    }

    @Test
    public void testCreationTimeEqualsPageCreationTime() {
        page.setCreationDate(new Date());

        assertEquals(page.getCreationDate().getTime(), pageVersionsResourceImpl.getCreationtTime());
    }

    @Test
    public void testDisplayName() {
        assertEquals("@versions", pageVersionsResourceImpl.getDisplayName());
    }
}
