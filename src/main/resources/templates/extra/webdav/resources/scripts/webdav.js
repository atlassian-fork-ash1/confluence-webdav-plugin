jQuery(function() {

    // AUI supports `.escape()` which works same as `window.escape()`.
    // But since historically escaping here was done using jquery text() transformation, 
    // the behaviour would be slightly different. I am keeping it for now, the code
    // heavily depends on jQuery anyway.
    var escapeHTML = function (s) {
        return jQuery("<a></a>").text("" + s).html();
    };

    var webdavConfig = {

        getI18nMessages : function() {
            var messages = { };

            jQuery("fieldset.plugin_webdav_config > input[name^='i18n-']").each(function() {
                messages[this.name] = this.value;
            });

            return messages;
        },

        getDenyRegexOptionsContainer : function() {
            return jQuery("select[name='denyRegexesContainer']");
        },

        getDenyRegexOptions : function() {
            var optionArray = new Array();

            this.getDenyRegexOptionsContainer().children("option").each(
                    function(index) {
                        optionArray[index] = jQuery(this);
                    }
            );

            return optionArray;
        },

        getDenyRegexes : function() {
            var optionValuesArray = new Array();

            jQuery.each(this.getDenyRegexOptions(), function(index) {
                optionValuesArray[index] = escapeHTML(this.attr("value"));
            });

            return optionValuesArray;
        },

        isDenyRegexUnique : function(regex) {
            var _regex = regex;
            var unique = true;
            var denyRegexesArray = this.getDenyRegexes();

            jQuery.each(denyRegexesArray, function() {
                if (_regex == this) {
                    unique = false;
                    return false; /* Stop iterating */
                }
            });

            return unique;
        },

        addDenyRegex : function(regex) {
            if (regex.length == 0) {
                alert(webdavConfig.getI18nMessages()["i18n-blankregex"]);
            } else {
                if (webdavConfig.isDenyRegexUnique(regex)) {
                    regex = escapeHTML(regex);
                    webdavConfig.getDenyRegexOptionsContainer().append("<option value='" + regex + "'>" + regex + "</option>");
                } else {
                    alert(webdavConfig.getI18nMessages()["i18n-duplicateregex"]);
                }

            }
        },

        init : function() {
            jQuery("input[name='addDeniedClientRegexButton']").click(function() {
                var regex = jQuery.trim(jQuery("input[name='deniedClientRegex']").attr("value"));
                webdavConfig.addDenyRegex(regex);
            });
            
            jQuery("input[name='deniedClientRegex']").keydown(function(event) {
                if (event.keyCode == 13) {
                    var regex = jQuery.trim(jQuery(this).attr("value"));
                    webdavConfig.addDenyRegex(regex);
                    event.preventDefault();
                }
            });

            jQuery("input[name='removeDeniedClientRegexButton']").click(function() {
                webdavConfig.getDenyRegexOptionsContainer().children("option:selected").each(function() {
                    jQuery(this).remove();
                });
            });

            jQuery("form[name='webdavSettings']").submit(function() {
                var regexes = webdavConfig.getDenyRegexes();
                var denyRegexParam = jQuery("select[name='denyRegexes']");

                jQuery.each(regexes, function() {
                    denyRegexParam.append("<option value='" + this + "' selected='selected'>" + this + "</option>");
                });

                return true;
            });
        }
    };

    webdavConfig.init();
});
