package com.atlassian.confluence.extra.webdav.listener;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.event.events.content.page.PageRestoreEvent;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSessionStore;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSessionTask;
import com.atlassian.event.Event;
import com.atlassian.event.EventListener;

/**
 * Listens for page restore events coming from Confluence to reset appropriate flags
 * in {@link com.atlassian.confluence.extra.webdav.ResourceStates}. This is useful when WebDAV resources
 * removed by clients are restored via Confluence. When content is restored, we want all of its states reset.
 * <p>
 * This class does that.
 *
 * @see com.atlassian.confluence.extra.webdav.ResourceStates
 */
public class PageRestoreListener implements EventListener {
    private static final Class[] HANDLED_EVENTS = new Class[]{
            PageRestoreEvent.class
    };

    private ConfluenceDavSessionStore confluenceDavSessionStore;

    public void setConfluenceDavSessionStore(ConfluenceDavSessionStore confluenceDavSessionStore) {
        this.confluenceDavSessionStore = confluenceDavSessionStore;
    }

    public void handleEvent(Event event) {
        confluenceDavSessionStore.executeTaskOnSessions(
                new ContentEntityAttributesResetDavSessionTask(((PageRestoreEvent) event).getContent())
        );
    }

    public Class[] getHandledEventClasses() {
        return HANDLED_EVENTS;
    }

    private static class ContentEntityAttributesResetDavSessionTask implements ConfluenceDavSessionTask {
        private final ContentEntityObject ceo;

        private ContentEntityAttributesResetDavSessionTask(ContentEntityObject ceo) {
            this.ceo = ceo;
        }

        public void execute(ConfluenceDavSession confluenceDavSession) {
            confluenceDavSession.getResourceStates().resetContentAttributes(ceo);
        }
    }

}
