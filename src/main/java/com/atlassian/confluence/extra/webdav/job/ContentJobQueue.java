package com.atlassian.confluence.extra.webdav.job;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a job that maintains a queue of child jobs (of type {@link ContentJob}) to be executed.
 *
 * @see com.atlassian.confluence.extra.webdav.job.ContentJob
 * @see #executeTasks()
 */
@Component
public class ContentJobQueue {
    /**
     * The object to synchronize on for operations on {@link #jobs}.
     */
    private final Object mutex = new Object();

    /**
     * The list of {@link com.atlassian.confluence.extra.webdav.job.ContentJob}
     */
    private final List<ContentJob> jobs = new ArrayList<ContentJob>();
    private final TransactionTemplate transactionTemplate;

    @Autowired
    public ContentJobQueue(@ComponentImport TransactionTemplate template) {
        this.transactionTemplate = template;
    }

    /**
     * Enques a {@link com.atlassian.confluence.extra.webdav.job.ContentJob} for execution.
     */
    public void enque(final ContentJob contentJob) {
        synchronized (mutex) {
            jobs.add(contentJob);
        }
    }

    /**
     * Removes all {@link com.atlassian.confluence.extra.webdav.job.ContentJob}s enqueued for
     * execution.
     *
     * @param contentJob All instances of {@link com.atlassian.confluence.extra.webdav.job.ContentJob} enqueued
     *                   that are equal to this and has not been executed will be removed from the execution queue.
     */
    public void remove(final ContentJob contentJob) {
        synchronized (mutex) {
            jobs.remove(contentJob);
        }
    }

    protected boolean isJobDueForExecution(ContentJob job) {
        return System.currentTimeMillis() - job.getCreationTime() >= job.getMinimumAgeForExecution();
    }

    protected ContentJob executeTask(ContentJob job) {
        return transactionTemplate.execute(new ContentJobQueueTransactionCallback(job));
    }

    /**
     * Executes all {@link com.atlassian.confluence.extra.webdav.job.ContentJob} in it if they
     * are due for execution.
     *
     * @see ContentJob#getMinimumAgeForExecution()
     */
    public void executeTasks() {
        final List<ContentJob> jobsCopy = new ArrayList<ContentJob>();
        final List<ContentJob> jobsExecuted = new ArrayList<ContentJob>();

        synchronized (mutex) {
            jobsCopy.addAll(jobs);
        }

        for (ContentJob job : jobsCopy) {
            if (isJobDueForExecution(job)) {
                ContentJob jobExecuted = executeTask(job);

                if (null != jobExecuted)
                    jobsExecuted.add(jobExecuted);
            }
        }

        synchronized (jobs) {
            jobs.removeAll(jobsExecuted);
        }
    }
}
