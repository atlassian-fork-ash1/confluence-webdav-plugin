package com.atlassian.confluence.extra.webdav.job;

import com.atlassian.sal.api.transaction.TransactionCallback;
import org.apache.log4j.Logger;

/**
 * Wraps the execution of the task represented by a single {@link com.atlassian.confluence.extra.webdav.job.ContentJob}
 * within a transaction.
 */
class ContentJobQueueTransactionCallback implements TransactionCallback<ContentJob> {
    private static final Logger LOGGER = Logger.getLogger(ContentJobQueue.class);

    private final ContentJob job;

    public ContentJobQueueTransactionCallback(ContentJob job) {
        this.job = job;
    }

    public ContentJob doInTransaction() {
        if (null != job) {
            try {
                job.execute();
            } catch (Exception e) {
                LOGGER.error("Error executing content job: " + job, e);
            }
        }

        return job;

    }
}
