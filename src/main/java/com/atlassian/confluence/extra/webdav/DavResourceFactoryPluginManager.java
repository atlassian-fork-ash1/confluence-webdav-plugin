package com.atlassian.confluence.extra.webdav;

import org.apache.jackrabbit.webdav.DavResourceFactory;

/**
 * Provides methods for getting the list of plugin-provided {@link DavResourceFactory} objects.
 */
public interface DavResourceFactoryPluginManager {
    /**
     * Returns the {@link DavResourceFactory} for the given workspace name, or {@code null} if there is no such
     * {@link DavResourceFactory}.
     *
     * @param workspaceName the name of the workspace, with no slashes before or after.
     * @return the appropriate {@link DavResourceFactory} or null.
     */
    DavResourceFactory getFactoryForWorkspace(String workspaceName);
}
