package com.atlassian.confluence.extra.webdav;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.spaces.Space;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

import static org.apache.commons.lang3.BooleanUtils.toBoolean;

/**
 * This class is used to track the states of WebDAV resources (what you see in the WebDAV client view). For now, it just
 * tracks whether a resource is hidden or not. So, why is this important?
 * <p>
 * A lot of WebDAV clients (including OS X Finder) likes to remove and then recreate a file as a &quot;replace&quot; operation.
 * If we did what those clients request, content attachment histories will be lost. Therefore, to work around this, we delay
 * the actual removal of a content/attachment when we receive the delete request
 * (see {@link com.atlassian.confluence.extra.webdav.job.impl.AttachmentRemovalJob}). Since we don't immediately delete
 * the resources clients ask us to, we need to have a way to tell clients they are gone... and that's what this class is for.
 * <p>
 * When a delete request is received, the relevant resource is hidden, so clients wouldn't get a surprise that a removed resource
 * is still there and proceeds to fail the operation in panic.
 * <p>
 * Things that can be hidden can also be unhidden.
 */
public class ResourceStates implements Serializable {

    private static final String WEBDAV_SPACE_PROP_DESC = "confluence.extra.webdav.description";
    private static final String WEBDAV_CONTENT_PROP_CONTENT = "confluence.extra.webdav.content";
    private static final String WEBDAV_CONTENT_PROP_CONTENT_MARKUP = "confluence.extra.webdav.content.wikimarkup";
    private static final String WEBDAV_CONTENT_PROP_CONTENT_URL = "confluence.extra.webdav.content.url";
    private static final String WEBDAV_CONTENT_PROP_CONTENT_VERSIONS = "confluence.extra.webdav.content.versions";
    private static final String WEBDAV_CONTENT_PROP_CONTENT_VERSIONS_README = "confluence.extra.webdav.content.versions.readme";
    private static final String WEBDAV_CONTENT_PROP_CONTENT_VERSION_TXT_PREFIX = "confluence.extra.webdav.content.versions.";
    private static final String WEBDAV_CONTENT_PROP_CONTENT_EXPORTS = "confluence.extra.webdav.content.exports";
    private static final String WEBDAV_CONTENT_PROP_CONTENT_EXPORTS_PDF = "confluence.extra.webdav.content.exports.pdf";
    private static final String WEBDAV_CONTENT_PROP_CONTENT_EXPORTS_WORD = "confluence.extra.webdav.content.exports.word";
    private static final String WEBDAV_CONTENT_PROP_CONTENT_EXPORTS_README = "confluence.extra.webdav.content.exports.readme";
    private static final String WEBDAV_CONTENT_PROP_ATTACHMENT_PREFIX = "confluence.extra.webdav.content.attachment.";

    private Map<AbstractAttributePlaceholder, String> contentAttributePlaceHolderToValueMap;

    public ResourceStates() {
        contentAttributePlaceHolderToValueMap = new HashMap<>();
    }

    private void setSpaceProperty(Space space, String property, String value) {
        if (null != space) {
            contentAttributePlaceHolderToValueMap.put(
                    new SpaceAttributePlaceHolder(space, property),
                    value
            );
        }
    }

    private String getSpaceProperty(Space space, String property) {
        return null != space
                ? contentAttributePlaceHolderToValueMap.get(new SpaceAttributePlaceHolder(space, property))
                : null;
    }

    private void setContentProperty(ContentEntityObject ceo, String property, String value) {
        if (null != ceo) {
            contentAttributePlaceHolderToValueMap.put(
                    new ContentAttributePlaceHolder(ceo, property),
                    value
            );
        }
    }

    private String getContentProperty(ContentEntityObject ceo, String property) {
        return null != ceo
                ? contentAttributePlaceHolderToValueMap.get(new ContentAttributePlaceHolder(ceo, property))
                : null;
    }

    /* Hiding utils for a space description */
    public void hideSpaceDescription(Space space) {
        if (null != space) {
            setSpaceProperty(space, WEBDAV_SPACE_PROP_DESC, Boolean.TRUE.toString());
        }
    }

    public void unhideSpaceDescription(Space space) {
        if (null != space) {
            setSpaceProperty(space, WEBDAV_SPACE_PROP_DESC, Boolean.FALSE.toString());
        }
    }

    public boolean isSpaceDescriptionHidden(Space space) {
        return toBoolean(getSpaceProperty(space, WEBDAV_SPACE_PROP_DESC));
    }

    /* Hiding utils for an attachment */
    public void hideAttachment(Attachment attachment) {
        if (null != attachment) {
            setContentProperty(attachment.getContainer(), WEBDAV_CONTENT_PROP_ATTACHMENT_PREFIX + attachment.getFileName(), Boolean.TRUE.toString());
        }
    }

    public void unhideAttachment(Attachment attachment) {
        if (null != attachment) {
            setContentProperty(attachment.getContainer(), WEBDAV_CONTENT_PROP_ATTACHMENT_PREFIX + attachment.getFileName(), Boolean.FALSE.toString());
        }
    }

    public boolean isAttachmentHidden(Attachment attachment) {
        return toBoolean(getContentProperty(attachment.getContainer(), WEBDAV_CONTENT_PROP_ATTACHMENT_PREFIX + attachment.getFileName()));
    }

    /* Hiding utils for the page */
    public void hideContent(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT, Boolean.TRUE.toString());
    }

    public void unhideContent(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT, Boolean.FALSE.toString());
    }

    public boolean isContentHidden(ContentEntityObject ceo) {
        return toBoolean(getContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT));
    }

    /* Hiding utils for the page content resource  */
    public void hideContentMarkup(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_MARKUP, Boolean.TRUE.toString());
    }

    public void unhideContentMarkup(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_MARKUP, Boolean.FALSE.toString());
    }

    public boolean isContentMarkupHidden(ContentEntityObject ceo) {
        return toBoolean(getContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_MARKUP));
    }

    /* Hiding utils the page .url resource */
    public void hideContentUrl(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_URL, Boolean.TRUE.toString());
    }

    public void unhideContentUrl(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_URL, Boolean.FALSE.toString());
    }

    public boolean isContentUrlHidden(ContentEntityObject ceo) {
        return toBoolean(getContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_URL));
    }

    /* Hiding utils for the @versions directory */
    public void hideContentVersions(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_VERSIONS, Boolean.TRUE.toString());
    }

    public void unhideContentVersions(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_VERSIONS, Boolean.FALSE.toString());
    }

    public boolean isContentVersionsHidden(ContentEntityObject ceo) {
        return toBoolean(getContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_VERSIONS));
    }

    /* Hiding utils for version files in the @versions directory */
    public void hideContentVersionText(ContentEntityObject ceo, String version) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_VERSION_TXT_PREFIX + version, Boolean.TRUE.toString());
    }

    public void unhideContentVersionText(ContentEntityObject ceo, String version) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_VERSION_TXT_PREFIX + version, Boolean.FALSE.toString());
    }

    public boolean isContentVersionTextHidden(ContentEntityObject ceo, String version) {
        return toBoolean(getContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_VERSION_TXT_PREFIX + version));
    }

    /* Hiding utils for the readme.txt in the @versions directory */
    public void hideContentVersionsReadme(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_VERSIONS_README, Boolean.TRUE.toString());
    }

    public void unhideContentVersionsReadme(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_VERSIONS_README, Boolean.FALSE.toString());
    }

    public boolean isContentVersionsReadmeHidden(ContentEntityObject ceo) {
        return toBoolean(getContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_VERSIONS_README));
    }

    /* Hiding utils for the @exports directory */
    public void hideContentExports(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_EXPORTS, Boolean.TRUE.toString());
    }

    public void unhideContentExports(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_EXPORTS, Boolean.FALSE.toString());
    }

    public boolean isContentExportsHidden(ContentEntityObject ceo) {
        return toBoolean(getContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_EXPORTS));
    }

    /* Hiding utils for PDF export in the @exports directory */
    public void hideContentPdfExport(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_EXPORTS_PDF, Boolean.TRUE.toString());
    }

    public void unhideContentPdfExport(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_EXPORTS_PDF, Boolean.FALSE.toString());
    }

    public boolean isContentPdfExportHidden(ContentEntityObject ceo) {
        return toBoolean(getContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_EXPORTS_PDF));
    }

    /* Hiding utils for MS Word exports in the @exports directory */
    public void hideContentWordExport(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_EXPORTS_WORD, Boolean.TRUE.toString());
    }

    public void unhideContentWordExport(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_EXPORTS_WORD, Boolean.FALSE.toString());
    }

    public boolean isContentWordExportHidden(ContentEntityObject ceo) {
        return toBoolean(getContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_EXPORTS_WORD));
    }

    /* Hiding utils the README.txt in @exports directory */
    public void hideContentExportsReadme(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_EXPORTS_README, Boolean.TRUE.toString());
    }

    public void unhideContentExportsReadme(ContentEntityObject ceo) {
        setContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_EXPORTS_README, Boolean.FALSE.toString());
    }

    public boolean isContentExportsReadmeHidden(ContentEntityObject ceo) {
        return toBoolean(getContentProperty(ceo, WEBDAV_CONTENT_PROP_CONTENT_EXPORTS_README));
    }

    /**
     * Resets all the hidden flags of the content and its:
     * <ul>
     * <li>attachments, including {@literal <page title>.txt} and {@literal <page title>.url}</li>
     * <li>versions directory and the files in it.</li>
     * <li>exports directory and the files in it.</li>
     * </ul>
     *
     * @param ceo The {@link com.atlassian.confluence.core.ContentEntityObject} to reset hidden flags of.
     */
    public void resetContentAttributes(ContentEntityObject ceo) {
        for (Iterator<AbstractAttributePlaceholder> i = contentAttributePlaceHolderToValueMap.keySet().iterator(); i.hasNext(); ) {
            AbstractAttributePlaceholder abstractAttributePlaceholder = i.next();
            if (abstractAttributePlaceholder instanceof ContentAttributePlaceHolder) {
                ContentAttributePlaceHolder contentAttributePlaceHolder = (ContentAttributePlaceHolder) abstractAttributePlaceholder;

                if (contentAttributePlaceHolder.getContentId() == ceo.getId()) {
                    i.remove();
                }
            }
        }
    }


    private static abstract class AbstractAttributePlaceholder implements Serializable {
        String attributeIdentifer;

        AbstractAttributePlaceholder(String attributeIdentifer) {
            this.attributeIdentifer = attributeIdentifer;
        }

        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof AbstractAttributePlaceholder)) return false;

            AbstractAttributePlaceholder that = (AbstractAttributePlaceholder) o;

            return Objects.equals(attributeIdentifer, that.attributeIdentifer);
        }

        public int hashCode() {
            return Objects.hash(attributeIdentifer);
        }
    }

    private static class ContentAttributePlaceHolder extends AbstractAttributePlaceholder {
        long contentId;

        ContentAttributePlaceHolder(ContentEntityObject ceo, String attributeIdentifer) {
            super(attributeIdentifer);
            this.contentId = ceo.getId();
        }

        long getContentId() {
            return contentId;
        }

        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            if (!super.equals(o)) return false;

            ContentAttributePlaceHolder that = (ContentAttributePlaceHolder) o;

            return contentId == that.contentId;
        }

        public int hashCode() {
            return Objects.hash(attributeIdentifer, contentId);
        }
    }

    private static class SpaceAttributePlaceHolder extends AbstractAttributePlaceholder {
        protected String spaceKey;

        SpaceAttributePlaceHolder(Space space, String attributeIdentifer) {
            super(attributeIdentifer);
            this.spaceKey = space.getKey();
        }

        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            if (!super.equals(o)) return false;

            SpaceAttributePlaceHolder that = (SpaceAttributePlaceHolder) o;

            return Objects.equals(spaceKey, that.spaceKey);
        }

        public int hashCode() {
            return Objects.hash(attributeIdentifer, spaceKey);
        }
    }

}
