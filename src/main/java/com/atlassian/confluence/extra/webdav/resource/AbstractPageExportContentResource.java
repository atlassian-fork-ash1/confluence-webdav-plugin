package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.io.IOUtils;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Abstract repsentation of a Confluence export (PDF or MS Word).
 *
 * @author weiching.cher
 */
public abstract class AbstractPageExportContentResource extends AbstractContentResource {
    private final PageManager pageManager;

    protected final String spaceKey;

    protected final String pageTitle;

    private Page page;

    public AbstractPageExportContentResource(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport PageManager pageManager,
            String spaceKey,
            String pageTitle) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession);
        this.pageManager = pageManager;
        this.spaceKey = spaceKey;
        this.pageTitle = pageTitle;
    }

    protected Page getPage() {
        if (null == page)
            page = pageManager.getPage(spaceKey, pageTitle);
        return page;
    }

    protected abstract InputStream getContentInternal() throws IOException;

    protected abstract String getExportSuffix();

    private File checkWriteToTempFile() throws IOException {
        Page thisPage = getPage();
        File exportedContent = new File(GeneralUtil.getConfluenceTempDirectory(), "webdav-" + thisPage.getIdAsString() + getExportSuffix());

        if (!exportedContent.exists() || exportedContent.lastModified() < thisPage.getLastModificationDate().getTime()) {
            InputStream in = null;
            OutputStream out = null;

            try {
                in = getContentInternal();
                out = new BufferedOutputStream(new FileOutputStream(exportedContent));

                IOUtils.copy(in, out);
            } finally {
                IOUtils.closeQuietly(out);
                IOUtils.closeQuietly(in);
            }
        }

        return exportedContent;
    }

    protected InputStream getContent() {
        try {
            return new BufferedInputStream(new FileInputStream(checkWriteToTempFile()));
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    protected long getContentLength() {
        try {
            return checkWriteToTempFile().length();
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    public long getModificationTime() {
        return getPage().getLastModificationDate().getTime();
    }

    protected long getCreationtTime() {
        return getPage().getCreationDate().getTime();
    }
}
