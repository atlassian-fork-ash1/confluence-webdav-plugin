package com.atlassian.confluence.extra.webdav.servlet;

import com.atlassian.confluence.extra.webdav.util.WebdavConstants;
import com.atlassian.security.xml.SecureXmlParserFactory;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavLocatorFactory;
import org.apache.jackrabbit.webdav.DavMethods;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavServletResponse;
import org.apache.jackrabbit.webdav.DavSessionProvider;
import org.apache.jackrabbit.webdav.WebdavRequest;
import org.apache.jackrabbit.webdav.WebdavRequestImpl;
import org.apache.jackrabbit.webdav.WebdavResponse;
import org.apache.jackrabbit.webdav.WebdavResponseImpl;
import org.apache.jackrabbit.webdav.server.AbstractWebdavServlet;
import org.apache.jackrabbit.webdav.xml.DomUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;

@Component("webDavServlet")
@ParametersAreNonnullByDefault
public class ConfluenceWebdavServlet extends AbstractWebdavServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfluenceWebdavServlet.class);

    private static final String DEFAULT_AUTH_HEADER = "Basic realm=\"Confluence WebDAV Server\"";

    private DavSessionProvider davSessionProvider;

    private DavResourceFactory resourceFactory;

    private DavLocatorFactory locatorFactory;

    @Autowired
    public ConfluenceWebdavServlet(DavSessionProvider davSessionProvider, @Qualifier("resourceFactory") DavResourceFactory resourceFactory, DavLocatorFactory locatorFactory) {
        this.davSessionProvider = checkNotNull(davSessionProvider);
        this.resourceFactory = checkNotNull(resourceFactory);
        this.locatorFactory = checkNotNull(locatorFactory);

        // for security reasons we must set correct settings for the factory that is used to parse xml requests
        // see CONFDEV-7519 for details
        DocumentBuilderFactory documentBuilderFactory = SecureXmlParserFactory.newDocumentBuilderFactory();
        documentBuilderFactory.setNamespaceAware(true);
        documentBuilderFactory.setIgnoringComments(true);
        documentBuilderFactory.setIgnoringElementContentWhitespace(true);
        documentBuilderFactory.setCoalescing(true);
        DomUtil.setBuilderFactory(documentBuilderFactory);
    }

    public DavSessionProvider getDavSessionProvider() {
        return davSessionProvider;
    }

    public void setDavSessionProvider(DavSessionProvider davSessionProvider) {
        this.davSessionProvider = checkNotNull(davSessionProvider);
    }

    public DavResourceFactory getResourceFactory() {
        return resourceFactory;
    }

    public void setResourceFactory(DavResourceFactory resourceFactory) {
        this.resourceFactory = checkNotNull(resourceFactory);
    }

    public DavLocatorFactory getLocatorFactory() {
        return locatorFactory;
    }

    public void setLocatorFactory(DavLocatorFactory locatorFactory) {
        this.locatorFactory = checkNotNull(locatorFactory);
    }

    protected void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String method = request.getMethod();
        String uri = request.getRequestURI();

        LOGGER.debug("{}, {} {}", request.getHeader(WebdavConstants.HEADER_USER_AGENT), method, uri);

        WebdavRequest webdavRequest = new WebdavRequestImpl(request, getLocatorFactory());

        int methodCode = DavMethods.getMethodCode(request.getMethod());

        boolean noCache = DavMethods.isDeltaVMethod(webdavRequest) && !(DavMethods.DAV_VERSION_CONTROL == methodCode || DavMethods.DAV_REPORT == methodCode);
        WebdavResponse webdavResponse = new WebdavResponseImpl(response, noCache);

        try {
            ServletActionContext.setRequest(request);
            ServletActionContext.setResponse(response);

            if (!getDavSessionProvider().attachSession(webdavRequest))
                throw new DavException(DavServletResponse.SC_UNAUTHORIZED, "Unable to authenticate.");

            DavResource resource = getResourceFactory().createResource(webdavRequest.getRequestLocator(), webdavRequest, webdavResponse);

            if (!isPreconditionValid(webdavRequest, resource)) {
                webdavResponse.sendError(DavServletResponse.SC_PRECONDITION_FAILED);
                return;
            }

            if (!execute(webdavRequest, webdavResponse, methodCode, resource))
                super.service(request, response);
        } catch (DavException e) {
            if (e.getErrorCode() == HttpServletResponse.SC_UNAUTHORIZED) {
                LOGGER.debug("{} {} unauthorized", method, uri);
                webdavResponse.setHeader("WWW-Authenticate", getAuthenticateHeaderValue());
                webdavResponse.sendError(e.getErrorCode(), e.getStatusPhrase());
            } else if (HttpServletResponse.SC_FORBIDDEN == e.getErrorCode()) {
                LOGGER.debug("{} {} denied: {}", method, uri, e.getMessage());
                webdavResponse.sendError(e);
            } else {
                LOGGER.error("Unexpected error", e);
                webdavResponse.sendError(e);
            }
        } finally {
            getDavSessionProvider().releaseSession(webdavRequest);
            ServletActionContext.setRequest(null);
            ServletActionContext.setResponse(null);
        }
    }

    public String getAuthenticateHeaderValue() {
        return DEFAULT_AUTH_HEADER;
    }

    protected boolean isPreconditionValid(WebdavRequest request, DavResource resource) {
        return !resource.exists() || request.matchesIfHeader(resource);
    }
}
