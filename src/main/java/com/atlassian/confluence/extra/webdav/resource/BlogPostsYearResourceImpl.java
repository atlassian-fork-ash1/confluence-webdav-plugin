package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents the blog posts year directory. For instance:
 * <tt>/Global/ds/&amp;#064;news/2008</tt>.
 */
public class BlogPostsYearResourceImpl extends BlogPostsResourceImpl {
    protected final int yearPublished;

    public BlogPostsYearResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport PermissionManager permissionManager,
            @ComponentImport SpaceManager spaceManager,
            @ComponentImport PageManager pageManager,
            String spaceKey,
            int yearPublished) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession, permissionManager, spaceManager, pageManager, spaceKey);
        this.yearPublished = yearPublished;
    }

    public int getYearPublished() {
        return yearPublished;
    }

    public String getDisplayName() {
        return String.valueOf(yearPublished);
    }

    public Collection<DavResource> getMemberResources() {
        try {
            DavResourceLocator locator = getLocator();
            String parentPath = getParentResourcePath();
            StringBuffer childResourcePathBuffer = new StringBuffer();
            Set<String> uniqueChildPaths = new HashSet<String>();
            List<DavResource> members = new ArrayList<DavResource>();
            @SuppressWarnings("unchecked")
            List<BlogPost> blogPosts = getPermissionManager().getPermittedEntities(
                    AuthenticatedUserThreadLocal.getUser(),
                    Permission.VIEW,
                    getPageManager().getBlogPosts(getSpace(), true)
            );
            String yearPublishedString = getDisplayName();

            for (BlogPost blogPost : blogPosts) {
                if (StringUtils.equals(yearPublishedString, blogPost.getPostingYear())) {
                    childResourcePathBuffer.setLength(0);
                    childResourcePathBuffer.append(parentPath)
                            .append('/').append(yearPublished)
                            .append('/').append(blogPost.getPostingMonthNumeric());

                    uniqueChildPaths.add(childResourcePathBuffer.toString());
                }
            }

            for (String uniqueChildPath : uniqueChildPaths) {
                DavResourceLocator blogPostMonthResourceLocator;

                blogPostMonthResourceLocator = locator.getFactory().createResourceLocator(
                        locator.getPrefix(), locator.getWorkspacePath(), uniqueChildPath, false);

                members.add(getFactory().createResource(blogPostMonthResourceLocator, getSession()));
            }

            return members;
        } catch (DavException de) {
            throw new RuntimeException(de);
        }
    }
}
