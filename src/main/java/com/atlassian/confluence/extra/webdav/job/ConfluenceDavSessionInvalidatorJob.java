package com.atlassian.confluence.extra.webdav.job;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSessionStore;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import static java.util.Objects.requireNonNull;

/**
 * Class uses to invalidate sessions tracked by
 * {@link com.atlassian.confluence.extra.webdav.ConfluenceDavSessionStore}.
 */
@Component
@ParametersAreNonnullByDefault
public class ConfluenceDavSessionInvalidatorJob implements JobRunner {
    private final ConfluenceDavSessionStore confluenceDavSessionStore;

    @Autowired
    public ConfluenceDavSessionInvalidatorJob(ConfluenceDavSessionStore confluenceDavSessionStore) {
        this.confluenceDavSessionStore = requireNonNull(confluenceDavSessionStore);
    }

    @Nullable
    @Override
    public JobRunnerResponse runJob(JobRunnerRequest request) {
        confluenceDavSessionStore.invalidateExpiredSessions();
        return null;
    }
}
