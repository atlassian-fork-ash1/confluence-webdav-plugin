package com.atlassian.confluence.extra.webdav.job;

import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import static java.util.Objects.requireNonNull;

@Component
@ParametersAreNonnullByDefault
public class ContentJobQueueExecutor implements JobRunner {
    private final ContentJobQueue contentJobQueue;

    @Autowired
    public ContentJobQueueExecutor(ContentJobQueue contentJobQueue) {
        this.contentJobQueue = requireNonNull(contentJobQueue);
    }

    @Nullable
    @Override
    public JobRunnerResponse runJob(JobRunnerRequest request) {
        contentJobQueue.executeTasks();
        return null;
    }
}
