package com.atlassian.confluence.extra.webdav;

import com.atlassian.confluence.extra.webdav.resource.NonExistentResource;
import com.atlassian.confluence.extra.webdav.resource.WorkspaceResourceImpl;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.DavServletRequest;
import org.apache.jackrabbit.webdav.DavServletResponse;
import org.apache.jackrabbit.webdav.DavSession;
import org.apache.jackrabbit.webdav.lock.LockManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * A {@link DavResourceFactory} that delegates to the various {@link DavResourceFactory} objects provided by the
 * {@link DavResourceFactoryPluginManager}
 */
@Component("resourceFactory")
public final class PluggableResourceFactory implements DavResourceFactory {
    private static final Logger log = LoggerFactory.getLogger(PluggableResourceFactory.class);
    private final String defaultWorkspaceName;
    private final DavResourceFactoryPluginManager davResourceFactoryPluginManager;

    @Autowired
    public PluggableResourceFactory(DavResourceFactoryPluginManager davResourceFactoryPluginManager) {
        this.defaultWorkspaceName = "default";
        this.davResourceFactoryPluginManager = davResourceFactoryPluginManager;
    }

    private DavSession getDavSession(final HttpServletRequest httpServletRequest) {
        return (DavSession) httpServletRequest.getSession().getAttribute(ConfluenceDavSession.class.getName());
    }

    public DavResource createResource(DavResourceLocator davResourceLocator, DavServletRequest davServletRequest,
                                      DavServletResponse davServletResponse) throws DavException {
        return createResource(davResourceLocator, getDavSession(davServletRequest));
    }

    public DavResource createResource(DavResourceLocator davResourceLocator, DavSession davSession) throws DavException {
        String resourcePath = davResourceLocator.getResourcePath();
        log.debug("Trying to locate DavResource: " + resourcePath);

        ConfluenceDavSession confluenceDavSession = (ConfluenceDavSession) davSession;
        LockManager lockManager = confluenceDavSession.getLockManager();

        // HACK from WBDV-215.  This currently results in "/default".
        if (StringUtils.isBlank(davResourceLocator.getWorkspacePath())) {
            return new WorkspaceResourceImpl(davResourceLocator, this, lockManager, confluenceDavSession, defaultWorkspaceName);
        }

        String[] resourcePathTokens = StringUtils.split(resourcePath, '/');

        DavResourceFactory factoryForWorkspace = davResourceFactoryPluginManager.getFactoryForWorkspace(resourcePathTokens[0]);
        if (factoryForWorkspace != null)
            return factoryForWorkspace.createResource(davResourceLocator, confluenceDavSession);

        return new NonExistentResource(davResourceLocator, this, lockManager, confluenceDavSession);
    }
}
