package com.atlassian.confluence.extra.webdav.util;

import org.apache.commons.lang.StringUtils;

import java.util.regex.Pattern;

public class UserAgentUtil {
    private static final Pattern MS_MINIREDIR_PATTERN = Pattern.compile("^Microsoft-WebDAV-MiniRedir/.*");
    private static final Pattern OSX_WEBDAVFS_PATTERN = Pattern.compile("^WebDAVFS/.*");

    public static boolean isMicrosoftMiniRedirector(final String userAgent) {
        return MS_MINIREDIR_PATTERN.matcher(StringUtils.defaultString(userAgent)).matches();
    }

    public static boolean isOsxFinder(final String userAgent) {
        return OSX_WEBDAVFS_PATTERN.matcher(StringUtils.defaultString(userAgent)).matches();
    }
}
