package com.atlassian.confluence.extra.webdav.servlet.filter;

import com.atlassian.confluence.extra.webdav.WebdavSettingsManager;
import com.atlassian.confluence.extra.webdav.util.WebdavConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavMethods;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ClientWriteDenyFilter extends AbstractHttpFilter {
    private final WebdavSettingsManager webdavSettingsManager;

    public ClientWriteDenyFilter(WebdavSettingsManager webdavSettingsManager) {
        this.webdavSettingsManager = webdavSettingsManager;
    }

    private boolean isWebdavClientDenied(String userAgent, String method) {
        return (method.equalsIgnoreCase(DavMethods.METHOD_PUT)
                || method.equalsIgnoreCase(DavMethods.METHOD_COPY)
                || method.equalsIgnoreCase(DavMethods.METHOD_DELETE)
                || method.equalsIgnoreCase(DavMethods.METHOD_MKCOL)
                || method.equalsIgnoreCase(DavMethods.METHOD_MOVE)
        ) && webdavSettingsManager.isClientInWriteBlacklist(userAgent);
    }

    public void doFilter(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (isWebdavClientDenied(
                StringUtils.defaultString(httpServletRequest.getHeader(WebdavConstants.HEADER_USER_AGENT)),
                httpServletRequest.getMethod())) {
            httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN); /* Send not permitted error seems better */
        } else {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        }
    }
}
