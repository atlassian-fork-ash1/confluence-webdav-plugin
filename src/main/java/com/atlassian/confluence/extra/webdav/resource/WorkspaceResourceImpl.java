package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.util.ArrayList;
import java.util.Collection;

/**
 * The parent of {@link com.atlassian.confluence.extra.webdav.resource.DashboardResourceImpl}.
 *
 * @author weiching.cher
 */
public class WorkspaceResourceImpl extends AbstractCollectionResource {
    private final String workspaceName;

    public WorkspaceResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            String workspaceName) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession);
        this.workspaceName = workspaceName;
    }

    protected long getCreationtTime() {
        return 0;
    }

    public boolean exists() {
        return true;
    }

    public String getDisplayName() {
        return workspaceName;
    }

    protected Collection<DavResource> getMemberResources() {
        try {
            DavResourceLocator davResourceLocator = getLocator();
            Collection<DavResource> memberResources = new ArrayList<DavResource>();

            memberResources.add(
                    getFactory().createResource(
                            davResourceLocator.getFactory().createResourceLocator(
                                    davResourceLocator.getPrefix(),
                                    new StringBuffer("/").append(workspaceName).toString(),
                                    "/",
                                    false),
                            getSession()
                    )
            );

            return memberResources;
        } catch (DavException de) {
            throw new RuntimeException(de);
        }
    }
}
