package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.extra.webdav.ResourceStates;
import com.atlassian.confluence.extra.webdav.job.ContentJobQueue;
import com.atlassian.confluence.extra.webdav.job.impl.AttachmentRemovalJob;
import com.atlassian.confluence.extra.webdav.util.ResourceHelper;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.user.User;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.DavSession;
import org.apache.jackrabbit.webdav.io.InputContext;
import org.apache.jackrabbit.webdav.lock.LockManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Represents a page.
 *
 * @author weiching.cher
 */
public class PageResourceImpl extends AbstractCollectionResource {
    private static final Logger LOGGER = Logger.getLogger(PageResourceImpl.class);

    private final PermissionManager permissionManager;

    private final SpaceManager spaceManager;

    private final PageManager pageManager;

    private final AttachmentManager attachmentManager;

    private final ContentJobQueue contentJobQueue;

    private final String spaceKey;

    private final String pageTitle;

    private Page page;

    public PageResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport PermissionManager permissionManager,
            @ComponentImport SpaceManager spaceManager,
            @ComponentImport PageManager pageManager,
            @ComponentImport AttachmentManager attachmentManager,
            ContentJobQueue contentJobQueue,
            String spaceKey,
            String pageTitle) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession);
        this.permissionManager = permissionManager;
        this.spaceManager = spaceManager;
        this.pageManager = pageManager;
        this.attachmentManager = attachmentManager;
        this.contentJobQueue = contentJobQueue;
        this.spaceKey = spaceKey;
        this.pageTitle = pageTitle;
    }

    public static boolean isPageTitleValid(String pageTitle) {
        return AbstractPage.isValidPageTitle(pageTitle);
    }

    public Page getPage() {
        if (null == page)
            page = pageManager.getPage(spaceKey, pageTitle);

        return page;
    }

    protected long getCreationtTime() {
        return getPage().getCreationDate().getTime();
    }

    public boolean exists() {
        Page thisPage = getPage();

        LOGGER.debug("String describing page \"" + pageTitle + "\" in space " + spaceKey + ": " + page);

        if (null != thisPage) {
            User currentUser = AuthenticatedUserThreadLocal.getUser();
            boolean hasViewPermissionOnpage = permissionManager.hasPermission(
                    currentUser,
                    Permission.VIEW,
                    thisPage);
            boolean isHidden = ((ConfluenceDavSession) getSession()).getResourceStates().isContentHidden(thisPage);

            LOGGER.debug("Current user: " + currentUser.getName());
            LOGGER.debug("Does current user have view privilege on page? " + hasViewPermissionOnpage);
            LOGGER.debug("Does the WebDAV plugin consider the page hiddden: " + isHidden);

            return hasViewPermissionOnpage && !isHidden;
        }

        return false;
    }

    public String getDisplayName() {
        return pageTitle;
    }

    private void createPage(String newPageTitle) throws DavException {
        Page thisPage = getPage();
        Page newPage = new Page();

        newPage.setSpace(thisPage.getSpace());

        /* Check for invalid page title */
        if (isPageTitleValid(newPageTitle)) {
            /* Check for (A Document Being Saved By Text Edit) created by TextEdit and stop the page creation */
            ConfluenceDavSession confluenceDavSession = (ConfluenceDavSession) getSession();

            if (isTextEditCreatingTempFolder(newPageTitle, confluenceDavSession))
                throw new DavException(HttpServletResponse.SC_FORBIDDEN, "This plugin does not allow creation of page with the title \"" + TEXTEDIT_TEMP_FOLDER_NAME + "\". See http://developer.atlassian.com/jira/browse/WBDV-143 for more information.");

            newPage.setTitle(newPageTitle);

            /* Check for duplicate page creation. */
            if (null == pageManager.getPage(newPage.getSpaceKey(), newPage.getTitle())) {
                newPage.setParentPage(thisPage);
                newPage.setCreatorName(AuthenticatedUserThreadLocal.getUser().getName());

                thisPage.addChild(newPage);

                pageManager.saveContentEntity(newPage, null);
            } else {
                throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Page creation denied. Page " + newPageTitle + " is not unique in space " + newPage.getSpaceKey());
            }
        } else {
            throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Page creation denied. New page name has invalid characters in the title: " + newPageTitle);
        }
    }

    private boolean isPageContentAttachment(Page aPage, String resourceName) {
        return StringUtils.equals(resourceName, aPage.getTitle() + PageContentResourceImpl.DISPLAY_NAME_SUFFIX);
    }

    private String getInputContentAsString(InputContext inputContext) throws IOException {
        if (inputContext.hasStream()) {
            Writer wikiMarkupBuffer = new StringWriter();
            Reader pageContentReader = new InputStreamReader(new BufferedInputStream(inputContext.getInputStream()), "UTF-8");

            try {
                IOUtils.copy(pageContentReader, wikiMarkupBuffer);
                return wikiMarkupBuffer.toString();
            } finally {
                IOUtils.closeQuietly(pageContentReader);
                IOUtils.closeQuietly(wikiMarkupBuffer);
            }
        } else {
            return StringUtils.EMPTY;
        }
    }

    private void updatePageContent(Page pageToUpdate, InputContext inputContext) throws IOException, CloneNotSupportedException {
        String xhtml = getInputContentAsString(inputContext);
        ContentEntityObject previousVersionOfPage = (ContentEntityObject) pageToUpdate.clone();
        User user = AuthenticatedUserThreadLocal.getUser();


        pageToUpdate.setBodyAsString(xhtml);
        pageToUpdate.setLastModificationDate(new Date());
        pageToUpdate.setLastModifierName(user.getName());

        pageManager.saveContentEntity(pageToUpdate, previousVersionOfPage, null);
    }


    private void cancelPendingAttachmentRemovalJobs(Page thisPage, String resourceName) {
        AttachmentRemovalJob attachmentRemovalJob =
                new AttachmentRemovalJob(
                        pageManager, attachmentManager,
                        thisPage.getId(),
                        resourceName
                );

        contentJobQueue.remove(attachmentRemovalJob);
    }

    private boolean isAttachmentRepresentingTempVersionOfPageContent(String resourceName) {
        return StringUtils.equals(
                "._" + getPage().getTitle() + PageContentResourceImpl.DISPLAY_NAME_SUFFIX,
                resourceName
        );
    }

    public void addMember(DavResource davResource, InputContext inputContext) throws DavException {
        User user = AuthenticatedUserThreadLocal.getUser();
        Page thisPage = getPage();
        String[] davResourcePathComponents = StringUtils.split(davResource.getResourcePath(), '/');
        String resourceName = davResourcePathComponents[davResourcePathComponents.length - 1];

        if (!inputContext.hasStream()) {
            /* InputContext.hasStream() returns false. This _is_ a folder creation. Page creation handling required. */
            if (permissionManager.hasCreatePermission(user, thisPage.getSpace(), Page.class)) {
                ResourceStates resourceStates = ((ConfluenceDavSession) getSession()).getResourceStates();

                if (StringUtils.equals(PageExportsResourceImpl.DISPLAY_NAME, resourceName)) {
                    resourceStates.unhideContentExports(thisPage);
                } else if (StringUtils.equals(PageVersionsResourceImpl.DISPLAY_NAME, resourceName)) {
                    resourceStates.unhideContentVersions(thisPage);
                } else {
                    createPage(resourceName);
                }
            } else {
                throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Permission denied for creating a child page " + resourceName + " on " + thisPage);
            }
        } else {
            /* InputContext.hasStream() returns true. This _is not_ a folder creation. Attachment handling required. */
            boolean isPageContentAttachment = isPageContentAttachment(thisPage, resourceName);
            boolean isPermitted = isPageContentAttachment
                    ? permissionManager.hasPermission(user, Permission.EDIT, thisPage)
                    : permissionManager.hasCreatePermission(user, thisPage, Attachment.class);

            if (!isPermitted)
                throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Permission denied for creating or updating attachment " + resourceName + " on " + thisPage);

            try {
                ResourceStates resourceStates = ((ConfluenceDavSession) getSession()).getResourceStates();

                if (isPageContentAttachment) {
                    resourceStates.unhideContentMarkup(thisPage);
                    updatePageContent(thisPage, inputContext);
                }
                /* Ignore .url creation and .DS_Store (OSX Finder temp file) */
                else if (StringUtils.equals(resourceName, thisPage.getTitle() + ".url")) {
                    resourceStates.unhideContentUrl(thisPage);
                } else if (StringUtils.equals(resourceName, ".DS_Store")) {
                    /* Ignore .DS_Store creation */
                } else {
                    /* Check that attachment name does not contain invalid characters */
                    if (AbstractAttachmentResource.isValidAttachmentName(resourceName)
                            || isAttachmentRepresentingTempVersionOfPageContent(resourceName)) {
                        /* Cancel all attachment removal task of this file */
                        cancelPendingAttachmentRemovalJobs(thisPage, resourceName);
                        ResourceHelper.addOrUpdateAttachment(attachmentManager, thisPage, resourceName, inputContext);

                        /* Then unmark it, so that it is returned in getMember() */
                        resourceStates.unhideAttachment(attachmentManager.getAttachment(thisPage, resourceName));
                    } else {
                        throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Attachment creation denied. File name contains invalid characters ['&', '+', '?', '|', '=']: " + resourceName);
                    }
                }
            } catch (DavException de) {
                throw de; /* Don't distort the status code of the exception */
            } catch (Exception e) {
                String errorMessage = "Unable to add/update attachment " + StringUtils.join(davResourcePathComponents, "/") + " on page " + thisPage;
                LOGGER.error(errorMessage, e);
                throw new DavException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
            }
        }
    }

    public void removeMember(DavResource davResource) throws DavException {
        String[] displayName = StringUtils.split(davResource.getResourcePath(), '/');
        String resourceName = displayName[displayName.length - 1];
        Page thisPage = getPage();
        ResourceStates resourceStates = ((ConfluenceDavSession) getSession()).getResourceStates();

        if (StringUtils.equals(resourceName, thisPage.getTitle() + PageContentResourceImpl.DISPLAY_NAME_SUFFIX)) {
            resourceStates.hideContentMarkup(thisPage);
        } else if (StringUtils.equals(resourceName, thisPage.getTitle() + PageUrlResourceImpl.DISPLAY_NAME_SUFFIX)) {
            resourceStates.hideContentUrl(thisPage);
        } else if (StringUtils.equals(resourceName, PageVersionsResourceImpl.DISPLAY_NAME)) {
            resourceStates.hideContentVersions(thisPage);
        } else if (StringUtils.equals(resourceName, PageExportsResourceImpl.DISPLAY_NAME)) {
            resourceStates.hideContentExports(thisPage);
        } else {
            List<Page> childPages = new ArrayList<Page>(thisPage.getChildren());
            @SuppressWarnings("unchecked")
            List<Attachment> attachments = new ArrayList<Attachment>(thisPage.getLatestVersionsOfAttachments());
            User user = AuthenticatedUserThreadLocal.getUser();

            for (Page childPage : childPages) {
                if (StringUtils.equals(resourceName, childPage.getTitle())) {
                    if (permissionManager.hasPermission(user, Permission.REMOVE, childPage)) {
                        pageManager.trashPage(childPage);
                        resourceStates.hideContent(childPage);
                    } else {
                        throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Permission denied to remove " + childPage);
                    }
                }
            }

            for (Attachment attachment : attachments) {
                if (StringUtils.equals(resourceName, attachment.getFileName())) {
                    if (permissionManager.hasPermission(user, Permission.REMOVE, attachment)) {
                        AttachmentRemovalJob attachmentRemovalJob =
                                new AttachmentRemovalJob(
                                        pageManager, attachmentManager, attachment.getContainer().getId(), attachment.getFileName()
                                );
                        /* Make the attachment invisible since we are deferring the deletion, but need to tell the client it is removed. */
                        resourceStates.hideAttachment(attachment);

                        contentJobQueue.enque(attachmentRemovalJob);
                    } else {
                        throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Permission denied to remove " + attachment);
                    }
                }
            }
        }
    }

    private boolean isDestinationPathValid(String[] destinationResourcePathComponents) {
        /* Only valid if in a space _and not any of_ @news or @versions or @exports */
        return destinationResourcePathComponents.length > 3
                && !ArrayUtils.contains(destinationResourcePathComponents, BlogPostsResourceImpl.DISPLAY_NAME)
                && !ArrayUtils.contains(destinationResourcePathComponents, PageVersionsResourceImpl.DISPLAY_NAME)
                && !ArrayUtils.contains(destinationResourcePathComponents, PageExportsResourceImpl.DISPLAY_NAME)
                && AbstractPage.isValidPageTitle(destinationResourcePathComponents[destinationResourcePathComponents.length - 1]);
    }

    public void move(DavResource davResource) throws DavException {
        Page thisPage = getPage();
        String[] destinationResourcePathComponents = StringUtils.split(davResource.getResourcePath(), '/');
        User user = AuthenticatedUserThreadLocal.getUser();

        if (!isDestinationPathValid(destinationResourcePathComponents))
            throw new DavException(
                    HttpServletResponse.SC_FORBIDDEN,
                    "Cannot move " + getResourcePath() + " to " + StringUtils.join(destinationResourcePathComponents, '/'));

        String destinationSpaceKey = destinationResourcePathComponents[2];
        String destinationPageTitle = destinationResourcePathComponents[destinationResourcePathComponents.length - 1];

        if (!permissionManager.hasPermission(user, Permission.EDIT, thisPage))
            throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Permission denied to move " + thisPage);

        if (!StringUtils.equals(thisPage.getTitle(), destinationPageTitle)) {
            /* Page rename */
            if (null != pageManager.getPage(destinationSpaceKey, destinationPageTitle))
                throw new DavException(HttpServletResponse.SC_FORBIDDEN, StringUtils.join(destinationResourcePathComponents, '/') + " points to an existing page.");

            pageManager.renamePage(thisPage, destinationPageTitle);
        } else {
            /* Page move */
            String destinationPageParentTitle = destinationResourcePathComponents.length > 4
                    ? destinationResourcePathComponents[destinationResourcePathComponents.length - 2]
                    : null;
            Page destinationPageParent = null == destinationPageParentTitle
                    ? null
                    : pageManager.getPage(destinationSpaceKey, destinationPageParentTitle);
            Space destinationSpace = spaceManager.getSpace(destinationSpaceKey);

            pageManager.movePageAsChild(thisPage, destinationPageParent);
        }
    }

    /**
     * Generates a unique page title in the given space based and base page title.
     *
     * @param spaceKey The key of the space to get a unique title of.
     * @param title    The base title (which will be prepended with &quot;Copy of&quot;) to get unique page titles.
     * @return The unique page title in the space.
     */
    protected String generateUniquePageTitle(String spaceKey, String title) {
        String newTitle = title;

        if (pageManager.getPage(spaceKey, newTitle) != null) {
            newTitle = "Copy of " + title;

            while (pageManager.getPage(spaceKey, newTitle) != null)
                newTitle = "Copy of " + newTitle;
        }

        return newTitle;
    }

    public void copy(DavResource davResource, boolean shallow)
            throws DavException {
        String[] displayName = StringUtils.split(davResource.getResourcePath(), '/');
        User user = AuthenticatedUserThreadLocal.getUser();

        if (permissionManager.hasCreatePermission(AuthenticatedUserThreadLocal.getUser(),
                spaceManager.getSpace(spaceKey),
                Page.class)) {
            Space targetSpace = spaceManager.getSpace(displayName[2]);
            Page parentPage;

            if (displayName.length <= 4) {
                parentPage = null;
            } else {
                parentPage = pageManager.getPage(targetSpace.getKey(), displayName[displayName.length - 2]);
            }

            String uniqueName = displayName[displayName.length - 1];

            if (getPage().getTitle().equals(displayName[displayName.length - 1])) {
                uniqueName = generateUniquePageTitle(getPage().getSpaceKey(), getPage().getTitle());
            }

            Page newPage = new Page();
            newPage.setTitle(uniqueName);
            newPage.setSpace(targetSpace);
            newPage.setBodyContent(getPage().getBodyContent());
            newPage.setParentPage(parentPage);
            newPage.setCreatorName(user.getName());

            for (Attachment attachment : getPage().getAttachments()) {
                Attachment newAttachment = new Attachment();

                newPage.addAttachment(newAttachment);

                newAttachment.setFileName(attachment.getFileName());
                newAttachment.setContentType(attachment.getContentType());
                newAttachment.setFileSize(attachment.getFileSize());
                newAttachment.setCreatorName(user.getName());

                try {
                    InputStream in = attachmentManager.getAttachmentData(attachment);
                    attachmentManager.saveAttachment(newAttachment, null, in);
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (parentPage != null) {
                parentPage.addChild(newPage);
            }

            if (getPage().hasChildren()) {
                /* Copy all childpages */
                List<Page> childPages = getPage().getChildren();
                addChild(childPages, newPage);
            }

            pageManager.saveContentEntity(newPage, null);
        } else {
            throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Forbidden to copy " + getHref());
        }
    }

    protected void addChild(List<Page> childPages, Page parent) {
        User user = AuthenticatedUserThreadLocal.getUser();

        for (Page childPage : childPages) {
            Page newChildPage = new Page();
            newChildPage.setTitle(generateUniquePageTitle(childPage.getSpaceKey(), childPage.getTitle()));
            newChildPage.setSpace(childPage.getSpace());
            newChildPage.setBodyContent(childPage.getBodyContent());
            newChildPage.setParentPage(parent);
            newChildPage.setCreatorName(user.getName());

            for (Attachment attachment : childPage.getAttachments()) {

                Attachment newAttachment = new Attachment();

                newChildPage.addAttachment(newAttachment);

                newAttachment.setFileName(attachment.getFileName());
                newAttachment.setContentType(attachment.getContentType());
                newAttachment.setFileSize(attachment.getFileSize());
                newAttachment.setCreatorName(AuthenticatedUserThreadLocal.getUser().getName());

                try {
                    InputStream in = attachment.getContentsAsStream();
                    attachmentManager.saveAttachment(newAttachment, null, in);
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (parent != null) {
                parent.addChild(newChildPage);
            }

            if (childPage.hasChildren()) {
                List<Page> childPageChildren = childPage.getChildren();
                addChild(childPageChildren, newChildPage);
            }
            pageManager.saveContentEntity(newChildPage, null);
        }
    }

    private DavResourceLocator getPageContentResourceLocator() {
        StringBuffer contentPathBuffer = new StringBuffer(getParentResourcePath());
        DavResourceLocator locator = getLocator();

        contentPathBuffer
                .append('/').append(pageTitle)
                .append('/').append(pageTitle).append(PageContentResourceImpl.DISPLAY_NAME_SUFFIX);

        return locator.getFactory().createResourceLocator(
                locator.getPrefix(),
                locator.getWorkspacePath(),
                contentPathBuffer.toString(), false);
    }

    protected DavResource getPageContentResource()
            throws DavException {
        return getFactory().createResource(getPageContentResourceLocator(), getSession());
    }

    private DavResourceLocator getPageUrlResourceLocator() {
        StringBuffer contentPathBuffer = new StringBuffer(getParentResourcePath());
        DavResourceLocator locator = getLocator();

        contentPathBuffer
                .append('/').append(pageTitle)
                .append('/').append(pageTitle).append(PageUrlResourceImpl.DISPLAY_NAME_SUFFIX);

        return locator.getFactory().createResourceLocator(
                locator.getPrefix(),
                locator.getWorkspacePath(),
                contentPathBuffer.toString(), false);
    }

    protected DavResource getPageUrlResource()
            throws DavException {
        return getFactory().createResource(getPageUrlResourceLocator(), getSession());
    }

    private DavResourceLocator[] getPageAttachmentResourceLocators() {
        DavResourceLocator locator = getLocator();
        List<Attachment> attachments = attachmentManager.getLatestVersionsOfAttachments(getPage());
        StringBuffer contentPathBuffer = new StringBuffer();
        List<DavResourceLocator> pageAttachmentResourceLocators = new ArrayList<DavResourceLocator>();
        String parentResourcePath = getParentResourcePath();

        for (Attachment attachment : attachments) {
            DavResourceLocator pageAttachmentResourceLocator;

            contentPathBuffer.setLength(0);
            contentPathBuffer.append(parentResourcePath)
                    .append('/').append(pageTitle)
                    .append('/').append(attachment.getFileName());

            pageAttachmentResourceLocator = locator.getFactory().createResourceLocator(
                    locator.getPrefix(),
                    locator.getWorkspacePath(),
                    contentPathBuffer.toString(),
                    false
            );

            pageAttachmentResourceLocators.add(pageAttachmentResourceLocator);
        }

        return pageAttachmentResourceLocators.toArray(new DavResourceLocator[pageAttachmentResourceLocators.size()]);
    }

    protected List<DavResource> getPageAttachmentResources() throws DavException {
        DavResourceLocator[] pageAttachmentResourceLocators = getPageAttachmentResourceLocators();
        List<DavResource> attachmentResources = new ArrayList<DavResource>(pageAttachmentResourceLocators.length);
        DavResourceFactory davResourceFactory = getFactory();
        DavSession session = getSession();

        for (DavResourceLocator pageAttachmentResourceLocator : pageAttachmentResourceLocators)
            attachmentResources.add(davResourceFactory.createResource(pageAttachmentResourceLocator, session));

        return attachmentResources;
    }

    private DavResourceLocator getPageExportResourceLocator() {
        StringBuffer contentPathBuffer = new StringBuffer(getParentResourcePath());
        DavResourceLocator locator = getLocator();

        contentPathBuffer
                .append('/').append(pageTitle)
                .append('/').append(AbstractExportsResource.DISPLAY_NAME);

        return locator.getFactory().createResourceLocator(
                locator.getPrefix(),
                locator.getWorkspacePath(),
                contentPathBuffer.toString(), false);
    }

    protected DavResource getPageExportsResource()
            throws DavException {
        return getFactory().createResource(getPageExportResourceLocator(), getSession());
    }

    private DavResourceLocator getPageVersionResourceLocator() {
        StringBuffer contentPathBuffer = new StringBuffer(getParentResourcePath());
        DavResourceLocator locator = getLocator();

        contentPathBuffer
                .append('/').append(pageTitle)
                .append('/').append(AbstractVersionsResource.DISPLAY_NAME);

        return locator.getFactory().createResourceLocator(
                locator.getPrefix(),
                locator.getWorkspacePath(),
                contentPathBuffer.toString(), false);
    }

    protected DavResource getPageVersionsResource()
            throws DavException {
        return getFactory().createResource(getPageVersionResourceLocator(), getSession());
    }

    private DavResourceLocator[] getChildPageResourceLocators() {
        DavResourceLocator locator = getLocator();
        Page page = getPage();
        List<Page> childrenPages = permissionManager.getPermittedEntities(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, page.getChildren());
        StringBuffer contentPathBuffer = new StringBuffer();
        List<DavResourceLocator> pageResourceLocators = new ArrayList<DavResourceLocator>();
        String parentResourcePath = getParentResourcePath();

        LOGGER.debug("Found " + (null == childrenPages ? 0 : childrenPages.size()) + " child pages of " + page + ".");

        for (Page childPage : childrenPages) {
            DavResourceLocator pageResourceLocator;

            contentPathBuffer.setLength(0);
            contentPathBuffer.append(parentResourcePath)
                    .append('/').append(page.getTitle())
                    .append('/').append(childPage.getTitle());

            LOGGER.debug("Found child page of \"" + page + "\", \"" + childPage + "\"");

            pageResourceLocator = locator.getFactory().createResourceLocator(
                    locator.getPrefix(),
                    locator.getWorkspacePath(),
                    contentPathBuffer.toString(),
                    false
            );

            LOGGER.debug("Created a " + pageResourceLocator.getClass() + " with the following details:\"\n"
                    + "\nPrefix: " + locator.getPrefix()
                    + "\nWorkspace path: " + locator.getWorkspacePath()
                    + "\nResource path: " + contentPathBuffer.toString());

            pageResourceLocators.add(pageResourceLocator);
        }

        return pageResourceLocators.toArray(new DavResourceLocator[pageResourceLocators.size()]);
    }

    protected List<DavResource> getChildPageResources() throws DavException {
        DavResourceLocator[] pageResourceLocators = getChildPageResourceLocators();
        List<DavResource> childPageResources = new ArrayList<DavResource>(pageResourceLocators.length);
        DavResourceFactory davResourceFactory = getFactory();
        DavSession session = getSession();

        for (DavResourceLocator pageResourceLocator : pageResourceLocators) {
            DavResource davResource = davResourceFactory.createResource(pageResourceLocator, session);

            LOGGER.debug("Does child page resource of \n" + getPage() + "\", \"" + davResource.getDisplayName() + "\" exist? " + davResource.exists());
            childPageResources.add(davResource);
        }

        return childPageResources;
    }

    protected Collection<DavResource> getMemberResources() {
        try {
            Collection<DavResource> memberResources = new ArrayList<DavResource>();

            memberResources.add(getPageContentResource());
            memberResources.add(getPageUrlResource());
            memberResources.addAll(getPageAttachmentResources());

            memberResources.add(getPageExportsResource());
            memberResources.add(getPageVersionsResource());

            memberResources.addAll(getChildPageResources());

            return memberResources;
        } catch (DavException de) {
            throw new RuntimeException(de);
        }
    }
}
