package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

/**
 * Abstract representation of the <tt>&amp;#064;exports</tt> directory.
 *
 * @author weiching.cher
 */
public abstract class AbstractExportsResource extends AbstractCollectionResource {
    public static final String DISPLAY_NAME = "@exports";

    public AbstractExportsResource(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession);
    }

    public String getDisplayName() {
        return DISPLAY_NAME;
    }

    public abstract ContentEntityObject getContentEntityObject();
}
