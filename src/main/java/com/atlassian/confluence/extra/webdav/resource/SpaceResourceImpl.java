package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.extra.webdav.util.ResourceHelper;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.user.User;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.io.InputContext;
import org.apache.jackrabbit.webdav.lock.LockManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Represents spaces, personal and global.
 *
 * @author weiching.cher
 */
public class SpaceResourceImpl extends AbstractCollectionResource {
    private static final Logger LOGGER = Logger.getLogger(SpaceResourceImpl.class);

    private final PermissionManager permissionManager;

    private final SpaceManager spaceManager;

    private final PageManager pageManager;

    private final AttachmentManager attachmentManager;

    private final String spaceKey;

    private Space space;

    public SpaceResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport PermissionManager permissionManager,
            @ComponentImport SpaceManager spaceManager,
            @ComponentImport PageManager pageManager,
            @ComponentImport AttachmentManager attachmentManager,
            String spaceKey) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession);
        this.permissionManager = permissionManager;
        this.spaceManager = spaceManager;
        this.pageManager = pageManager;
        this.attachmentManager = attachmentManager;
        this.spaceKey = spaceKey;
    }

    private Space getSpace() {
        if (null == space)
            space = spaceManager.getSpace(spaceKey);
        return space;
    }

    protected long getCreationtTime() {
        return getSpace().getCreationDate().getTime();
    }

    public boolean exists() {
        return super.exists()
                && null != getSpace()
                && permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, getSpace());
    }

    public String getDisplayName() {
        return spaceKey;
    }

    private String getInputContentAsString(InputContext inputContext) throws IOException {
        if (inputContext.hasStream()) {
            Writer wikiMarkupBuffer = new StringWriter();
            Reader pageContentReader = new InputStreamReader(new BufferedInputStream(inputContext.getInputStream()), "UTF-8");

            try {
                IOUtils.copy(pageContentReader, wikiMarkupBuffer);
                return wikiMarkupBuffer.toString();
            } finally {
                IOUtils.closeQuietly(pageContentReader);
                IOUtils.closeQuietly(wikiMarkupBuffer);
            }
        } else {
            return StringUtils.EMPTY;
        }
    }

    private boolean isResourceSpaceDescription(Space space, String resourceName) {
        return StringUtils.equals(resourceName, space.getName() + SpaceContentResourceImpl.DISPLAY_NAME_SUFFIX);
    }

    private void createPage(String newPageTitle)
            throws DavException {
        Space thisSpace = getSpace();
        Page newPage = new Page();

        newPage.setSpace(thisSpace);

        /* Check for invalid page title */
        if (PageResourceImpl.isPageTitleValid(newPageTitle)) {
            /* Check for (A Document Being Saved By Text Edit) created by TextEdit and stop the page creation */
            ConfluenceDavSession confluenceDavSession = (ConfluenceDavSession) getSession();

            if (isTextEditCreatingTempFolder(newPageTitle, confluenceDavSession))
                throw new DavException(HttpServletResponse.SC_FORBIDDEN, "This plugin does not allow creation of page with the title \"" + TEXTEDIT_TEMP_FOLDER_NAME + "\". See http://developer.atlassian.com/jira/browse/WBDV-143 for more information.");

            newPage.setTitle(newPageTitle);
            /* Check for duplicate page creation. */
            if (null == pageManager.getPage(newPage.getSpaceKey(), newPage.getTitle())) {
                newPage.setCreatorName(AuthenticatedUserThreadLocal.getUser().getName());
                pageManager.saveContentEntity(newPage, null);
            } else {
                throw new DavException(HttpServletResponse.SC_BAD_REQUEST, "Page creation denied. Page " + newPageTitle + " is not unique in space " + newPage.getSpaceKey());
            }
        } else {
            throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Page creation denied. New page name has invalid characters in the title: " + newPageTitle);
        }
    }

    public void addMember(DavResource davResource, InputContext inputContext) throws DavException {
        User user = AuthenticatedUserThreadLocal.getUser();
        Space thisSpace = getSpace();
        String[] davResourcePathComponents = StringUtils.split(davResource.getResourcePath(), '/');
        String resourceName = davResourcePathComponents[davResourcePathComponents.length - 1];

        if (!inputContext.hasStream()) {
            /* InputContext.hasStream() returns false. This _is_ a folder creation. Page creation handling required. */
            if (permissionManager.hasCreatePermission(user, thisSpace, Page.class)) {
                createPage(resourceName);
            } else {
                throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Permission denied for creating a top level page " + resourceName + " in " + thisSpace);
            }
        } else {
            /* InputContext.hasStream() returns true. This _is not_ a folder creation. Attachment handling required. */
            ContentEntityObject spaceDescription = thisSpace.getDescription();
            boolean isSpaceDescription = isResourceSpaceDescription(thisSpace, resourceName);
            boolean isPermitted = isSpaceDescription
                    ? permissionManager.hasPermission(user, Permission.EDIT, thisSpace)
                    : permissionManager.hasCreatePermission(user, spaceDescription, Attachment.class);

            if (!isPermitted)
                throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Permission denied for creating or updating attachment " + resourceName + " on " + spaceDescription);

            try {
                if (isResourceSpaceDescription(thisSpace, resourceName)) {
                    spaceDescription.setBodyAsString(getInputContentAsString(inputContext));

                    spaceManager.saveSpace(thisSpace);

                    ((ConfluenceDavSession) getSession()).getResourceStates().unhideSpaceDescription(thisSpace);
                } else {
                    ResourceHelper.addOrUpdateAttachment(attachmentManager, spaceDescription, resourceName, inputContext);
                }
            } catch (Exception e) {
                String errorMessage = "Unable to add/update attachment " + StringUtils.join(davResourcePathComponents, "/") + " on space " + thisSpace;
                LOGGER.error(errorMessage, e);
                throw new DavException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
            }
        }
    }

    public void removeMember(DavResource davResource) throws DavException {
        String[] pathComponents = StringUtils.split(davResource.getResourcePath(), '/');
        String resourceName = pathComponents[pathComponents.length - 1];
        Space thisSpace = getSpace();
        User user = AuthenticatedUserThreadLocal.getUser();

        if (isResourceSpaceDescription(thisSpace, resourceName)) {
            if (permissionManager.hasPermission(user, Permission.EDIT, thisSpace))
                ((ConfluenceDavSession) getSession()).getResourceStates().hideSpaceDescription(thisSpace);
            else
                throw new DavException(HttpServletResponse.SC_FORBIDDEN, "No permission to edit " + thisSpace);
        } else {
            Page pageToRemove = pageManager.getPage(thisSpace.getKey(), resourceName);

            if (null == pageToRemove) {
                ContentEntityObject spaceDesc = getSpace().getDescription();
                Attachment attachmentToRemove = attachmentManager.getAttachment(spaceDesc, resourceName);

                if (null != attachmentToRemove) {
                    attachmentManager.removeAttachmentFromServer(attachmentToRemove);
                }
            } else {
                if (permissionManager.hasPermission(
                        AuthenticatedUserThreadLocal.getUser(),
                        Permission.REMOVE,
                        pageToRemove)) {
                    pageManager.trashPage(pageToRemove);
                } else {
                    throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Forbidden to delete " + pageToRemove);
                }
            }

        }
    }

    protected Collection<DavResource> getMemberResources() {
        try {
            Collection<DavResource> memberResources = new ArrayList<DavResource>();

            memberResources.add(getSpaceContentResource());
            memberResources.add(getBlogPostsResource());
            memberResources.addAll(getPageResources());
            memberResources.addAll(getSpaceAttachmentResources());

            return memberResources;
        } catch (DavException de) {
            throw new RuntimeException(de);
        }
    }

    protected DavResource getSpaceContentResource()
            throws DavException {
        return getFactory().createResource(getSpaceContentResourceLocator(), getSession());
    }

    private DavResourceLocator getSpaceContentResourceLocator() {
        DavResourceLocator locator = getLocator();
        Space space = getSpace();
        StringBuffer contentPathBuffer = new StringBuffer(getParentResourcePath());

        contentPathBuffer
                .append('/').append(space.getKey())
                .append('/').append(space.getName()).append(SpaceContentResourceImpl.DISPLAY_NAME_SUFFIX);

        return locator.getFactory().createResourceLocator(
                locator.getPrefix(),
                locator.getWorkspacePath(),
                contentPathBuffer.toString(), false);
    }

    protected DavResource getBlogPostsResource()
            throws DavException {
        return getFactory().createResource(getBlogPostsResourceLocator(), getSession());
    }

    private DavResourceLocator getBlogPostsResourceLocator() {
        DavResourceLocator locator = getLocator();
        StringBuffer contentPathBuffer = new StringBuffer(getParentResourcePath());

        contentPathBuffer
                .append('/').append(spaceKey)
                .append('/').append(BlogPostsResourceImpl.DISPLAY_NAME);

        return locator.getFactory().createResourceLocator(
                locator.getPrefix(),
                locator.getWorkspacePath(),
                contentPathBuffer.toString(), false);
    }

    protected List<DavResource> getPageResources() throws DavException {
        DavResourceLocator[] pageResourceLocators = getPageResourceLocators();
        List<DavResource> pageResources = new ArrayList<DavResource>(pageResourceLocators.length);

        for (DavResourceLocator pageResourceLocator : pageResourceLocators)
            pageResources.add(getFactory().createResource(pageResourceLocator, getSession()));

        return pageResources;
    }

    private DavResourceLocator[] getPageResourceLocators() {
        DavResourceLocator locator = getLocator();
        Space space = getSpace();
        @SuppressWarnings("unchecked")
        List<Page> topLevelPages = permissionManager.getPermittedEntities(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, pageManager.getTopLevelPages(space));
        StringBuffer contentPathBuffer = new StringBuffer();
        List<DavResourceLocator> pageResourceLocators = new ArrayList<DavResourceLocator>();
        String parentResourcePath = getParentResourcePath();

        for (Page topLevelPage : topLevelPages) {
            DavResourceLocator pageResourceLocator;

            contentPathBuffer.setLength(0);
            contentPathBuffer.append(parentResourcePath)
                    .append('/').append(spaceKey)
                    .append('/').append(topLevelPage.getTitle());

            pageResourceLocator = locator.getFactory().createResourceLocator(
                    locator.getPrefix(),
                    locator.getWorkspacePath(),
                    contentPathBuffer.toString(),
                    false
            );

            pageResourceLocators.add(pageResourceLocator);
        }

        return pageResourceLocators.toArray(new DavResourceLocator[pageResourceLocators.size()]);
    }

    protected List<DavResource> getSpaceAttachmentResources() throws DavException {
        DavResourceLocator[] spaceAttachmentResourceLocators = getSpaceAttachmentResourceImpl();
        List<DavResource> spaceAttachmentResources = new ArrayList<DavResource>(spaceAttachmentResourceLocators.length);

        for (DavResourceLocator spaceAttachmentResourceLocator : spaceAttachmentResourceLocators)
            spaceAttachmentResources.add(getFactory().createResource(spaceAttachmentResourceLocator, getSession()));

        return spaceAttachmentResources;
    }

    private DavResourceLocator[] getSpaceAttachmentResourceImpl() {
        DavResourceLocator locator = getLocator();
        Space thisSpace = getSpace();
        ContentEntityObject spaceDesc = thisSpace.getDescription();
        @SuppressWarnings("unchecked")
        List<Attachment> attachments = attachmentManager.getLatestVersionsOfAttachments(spaceDesc);
        StringBuffer contentPathBuffer = new StringBuffer();
        List<DavResourceLocator> resourceLocators = new ArrayList<DavResourceLocator>();
        String parentResourcePath = getParentResourcePath();

        for (Attachment attachment : attachments) {
            DavResourceLocator resourceLocator;

            contentPathBuffer.setLength(0);
            contentPathBuffer.append(parentResourcePath)
                    .append('/').append(spaceKey)
                    .append('/').append(attachment.getFileName());

            resourceLocator = locator.getFactory().createResourceLocator(
                    locator.getPrefix(),
                    locator.getWorkspacePath(),
                    contentPathBuffer.toString(),
                    false
            );

            resourceLocators.add(resourceLocator);
        }

        return resourceLocators.toArray(new DavResourceLocator[resourceLocators.size()]);
    }
}
