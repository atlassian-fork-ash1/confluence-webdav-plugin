package com.atlassian.confluence.extra.webdav;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.tracker.DefaultPluginModuleTracker;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Provides methods for getting the list of plugin-provided {@link DavResourceFactory} objects using
 * wired components and a {@link DefaultPluginModuleTracker}
 */
@Component
public class DefaultDavResourceFactoryPluginManager implements DavResourceFactoryPluginManager {
    private DefaultPluginModuleTracker<DavResourceFactory, DavResourceFactoryModuleDescriptor> customDavResourceFactoryTracker;

    @Autowired
    public DefaultDavResourceFactoryPluginManager(@ComponentImport PluginAccessor pluginAccessor, @ComponentImport PluginEventManager pluginEventManager) {
        this.customDavResourceFactoryTracker = new DefaultPluginModuleTracker<DavResourceFactory, DavResourceFactoryModuleDescriptor>
                (pluginAccessor, pluginEventManager, DavResourceFactoryModuleDescriptor.class);
    }

    /**
     * {@inheritDoc}
     */
    public DavResourceFactory getFactoryForWorkspace(String workspaceName) {
        for (DavResourceFactoryModuleDescriptor davResourceFactoryModuleDescriptor : customDavResourceFactoryTracker.getModuleDescriptors()) {
            if (StringUtils.equals(workspaceName, davResourceFactoryModuleDescriptor.getWorkspaceName()))
                return davResourceFactoryModuleDescriptor.getModule();
        }
        return null;
    }
}
