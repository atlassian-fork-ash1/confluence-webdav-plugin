package com.atlassian.confluence.extra.webdav;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.regex.Pattern;

import static com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext.GLOBAL_CONTEXT;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Persists and retrieves {@link WebdavSettings} to/via Bandana.
 *
 * @since 4.3
 */
@Component
@ExportAsService(WebdavSettingsManager.class)
public class BandanaWebdavSettingsManager implements WebdavSettingsManager {
    /* We don't want to overwrite existing Webdav 1.0 settings. That's why there is a -2.0 in the key. */
    private static final String SETTINGS_KEY = "com.atlassian.confluence.extra.webdav-2.0.settings";

    private final BandanaManager bandanaManager;
    private final XStream xStream;

    @Autowired
    public BandanaWebdavSettingsManager(@ComponentImport BandanaManager bandanaManager) {
        this.bandanaManager = bandanaManager;
        xStream = new XStream(new PureJavaReflectionProvider());
        xStream.setClassLoader(getClass().getClassLoader());
    }

    @Override
    public void save(final WebdavSettings webdavSettings) {
        bandanaManager.setValue(GLOBAL_CONTEXT, SETTINGS_KEY, webdavSettings);
    }

    @Override
    public WebdavSettings getWebdavSettings() {
        WebdavSettings webdavSettings = null;
        Object setting = bandanaManager.getValue(GLOBAL_CONTEXT, SETTINGS_KEY);
        if (setting instanceof WebdavSettings) {
            webdavSettings = (WebdavSettings) setting;
        } else if (setting instanceof String) {
            String webdavSettingsXml = (String) setting;
            webdavSettings = isNotBlank(webdavSettingsXml) ? (WebdavSettings) xStream.fromXML(webdavSettingsXml) : new WebdavSettings();
        }
        return webdavSettings != null ? webdavSettings : new WebdavSettings();
    }

    @Override
    public boolean isClientInWriteBlacklist(String userAgent) {
        if (null == userAgent)
            return false;

        for (String regex : getWriteBlacklistClients()) {
            if (Pattern.compile(regex).matcher(userAgent).find())
                return true;
        }

        return false;
    }

    @Override
    public Set<String> getWriteBlacklistClients() {
        return getWebdavSettings().getExcludedClientUserAgentRegexes();
    }

    @Override
    public boolean isContentExportsResourceEnabled() {
        return getWebdavSettings().isContentExportsResourceEnabled();
    }

    @Override
    public boolean isContentVersionsResourceEnabled() {
        return getWebdavSettings().isContentVersionsResourceEnabled();
    }

    @Override
    public boolean isContentUrlResourceEnabled() {
        return getWebdavSettings().isContentUrlResourceEnabled();
    }

    @Override
    public boolean isStrictPageResourcePathCheckingDisabled() {
        return getWebdavSettings().isStrictPageResourcePathCheckingDisabled();
    }
}
