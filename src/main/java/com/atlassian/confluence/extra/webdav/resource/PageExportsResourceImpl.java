package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.extra.webdav.ResourceStates;
import com.atlassian.confluence.extra.webdav.WebdavSettingsManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.io.InputContext;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Represents a page's <tt>&amp;#064;exports</tt> directory.
 *
 * @author weiching.cher
 */
public class PageExportsResourceImpl extends AbstractExportsResource {
    private final WebdavSettingsManager webdavSettingsManager;

    private final PageManager pageManager;

    private final String spaceKey;

    private final String pageTitle;

    private Page page;

    public PageExportsResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            WebdavSettingsManager webdavSettingsManager,
            @ComponentImport PageManager pageManager,
            String spaceKey,
            String pageTitle) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession);
        this.webdavSettingsManager = webdavSettingsManager;
        this.pageManager = pageManager;
        this.spaceKey = spaceKey;
        this.pageTitle = pageTitle;
    }

    public ContentEntityObject getContentEntityObject() {
        if (null == page)
            page = pageManager.getPage(spaceKey, pageTitle);
        return page;
    }

    public boolean exists() {
        return super.exists()
                && !((ConfluenceDavSession) getSession()).getResourceStates().isContentExportsHidden(getContentEntityObject())
                && webdavSettingsManager.isContentExportsResourceEnabled();
    }

    public void addMember(DavResource davResource, InputContext inputContext) throws DavException {
        String[] pathComponents = StringUtils.split(davResource.getResourcePath(), '/');
        String resourceName = pathComponents[pathComponents.length - 1];
        ResourceStates resourceStates = ((ConfluenceDavSession) getSession()).getResourceStates();
        ContentEntityObject contentEntityObject = getContentEntityObject();

        if (StringUtils.equals(pageTitle + PagePdfExportContentResourceImpl.DISPLAY_NAME_SUFFIX, resourceName))
            resourceStates.unhideContentPdfExport(contentEntityObject);

        if (StringUtils.equals(pageTitle + PageWordExportContentResourceImpl.DISPLAY_NAME_SUFFIX, resourceName))
            resourceStates.unhideContentWordExport(contentEntityObject);

        if (StringUtils.equals(GeneratedResourceReadMeResource.DISPLAY_NAME, resourceName))
            resourceStates.unhideContentExportsReadme(contentEntityObject);
    }

    public void removeMember(DavResource davResource) throws DavException {
        String[] pathComponents = StringUtils.split(davResource.getResourcePath(), '/');
        String resourceName = pathComponents[pathComponents.length - 1];
        ResourceStates resourceStates = ((ConfluenceDavSession) getSession()).getResourceStates();
        ContentEntityObject contentEntityObject = getContentEntityObject();

        if (StringUtils.equals(pageTitle + PagePdfExportContentResourceImpl.DISPLAY_NAME_SUFFIX, resourceName))
            resourceStates.hideContentPdfExport(contentEntityObject);

        if (StringUtils.equals(pageTitle + PageWordExportContentResourceImpl.DISPLAY_NAME_SUFFIX, resourceName))
            resourceStates.hideContentWordExport(contentEntityObject);

        if (StringUtils.equals(GeneratedResourceReadMeResource.DISPLAY_NAME, resourceName))
            resourceStates.hideContentExportsReadme(contentEntityObject);
    }

    protected long getCreationtTime() {
        return getContentEntityObject().getCreationDate().getTime();
    }

    public long getModificationTime() {
        return getContentEntityObject().getLastModificationDate().getTime();
    }

    private DavResourceLocator getPageWordExportContentResourceLocator() {
        DavResourceLocator locator = getLocator();
        StringBuffer contentPathBuffer = new StringBuffer(getParentResourcePath());

        contentPathBuffer
                .append('/').append(AbstractExportsResource.DISPLAY_NAME)
                .append('/').append(pageTitle).append(PageWordExportContentResourceImpl.DISPLAY_NAME_SUFFIX);

        return locator.getFactory().createResourceLocator(
                locator.getPrefix(),
                locator.getWorkspacePath(),
                contentPathBuffer.toString(), false);
    }

    private DavResourceLocator getPagePdfExportContentResourceLocator() {
        DavResourceLocator locator = getLocator();
        StringBuffer contentPathBuffer = new StringBuffer(getParentResourcePath());

        contentPathBuffer
                .append('/').append(AbstractExportsResource.DISPLAY_NAME)
                .append('/').append(pageTitle).append(PagePdfExportContentResourceImpl.DISPLAY_NAME_SUFFIX);

        return locator.getFactory().createResourceLocator(
                locator.getPrefix(),
                locator.getWorkspacePath(),
                contentPathBuffer.toString(), false);
    }

    private DavResourceLocator getPageExportsReadmeResourceLocator() {
        DavResourceLocator locator = getLocator();
        StringBuffer contentPathBuffer = new StringBuffer(getParentResourcePath());

        contentPathBuffer
                .append('/').append(AbstractExportsResource.DISPLAY_NAME)
                .append('/').append(GeneratedResourceReadMeResource.DISPLAY_NAME);

        return locator.getFactory().createResourceLocator(
                locator.getPrefix(),
                locator.getWorkspacePath(),
                contentPathBuffer.toString(), false);
    }

    protected Collection<DavResource> getMemberResources() {
        try {
            Collection<DavResource> memberResources = new ArrayList<DavResource>();
            DavResourceFactory davResourceFactory = getFactory();

            memberResources.add(davResourceFactory.createResource(getPagePdfExportContentResourceLocator(), getSession()));
            memberResources.add(davResourceFactory.createResource(getPageWordExportContentResourceLocator(), getSession()));
            memberResources.add(davResourceFactory.createResource(getPageExportsReadmeResourceLocator(), getSession()));

            return memberResources;
        } catch (DavException de) {
            throw new RuntimeException(de);
        }
    }
}
